/*
	Conway's Game of Life simulator

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <conwaygol/all.hpp>

#include <wx/wx.h>
#include <wx/splitter.h>
#include <wx/glcanvas.h>
#include <wx/textctrl.h>
#include <wx/filedlg.h>
#include <wx/checkbox.h>
#include <wx/filename.h>
#include <wx/cmdline.h>
#include <wx/choice.h>
#include <wx/accel.h>
#include <wx/artprov.h>
#include <wx/bookctrl.h>
#include <wx/apptrait.h>
#include <wx/stdpaths.h>
#include <wx/gauge.h>
#include <wx/aboutdlg.h>

#include <memory>
#include <sstream>
#include <chrono>
#include <fstream>
#include <thread>
#include <random>
#include <future>
#include <list>
#include <charconv>
#include <optional>
#include <atomic>
#include <random>

#include <experimental/propagate_const>

#include <boost/algorithm/string/predicate.hpp>

#include <patlib/scoped.hpp>
#include <patlib/mutex.hpp>
#include <patlib/pointer.hpp>

namespace ConwayGoL_GUI
{
	using namespace std::chrono_literals;
	namespace sa = shared_access;
	namespace pl = patlib;

	using conwaygol::Point2D;
	using conwaygol::Point2Ds;
	using conwaygol::Point2Dr;

	[[nodiscard]] inline auto atomicClock() noexcept
		{ return pl::atomic_fence_return([]{ return std::chrono::high_resolution_clock::now(); }); }

	enum class SimulatorTypes
	{
		BruteForce = 0,
		Tiled,
		BruteForceLabyrinth1,
		BruteForceLabyrinth2
	};

	struct SimulatorDesc
	{
		SimulatorTypes type;
		std::wstring name;
	};

	const std::map<std::string, SimulatorTypes> simTypeStrMap =
	{
		{"bruteforce",               SimulatorTypes::BruteForce},
		{"tiled",                    SimulatorTypes::Tiled},
		{"bruteforce_labyrinth1",    SimulatorTypes::BruteForceLabyrinth1},
		{"bruteforce_labyrinth2",    SimulatorTypes::BruteForceLabyrinth2}
	};

	const std::map<SimulatorTypes, SimulatorDesc> simTypes =
	{
		{SimulatorTypes::BruteForce,               {SimulatorTypes::BruteForce,               L"Classic - Brute force"}},
		{SimulatorTypes::Tiled,                    {SimulatorTypes::Tiled,                    L"Classic - Tiled"}},
		{SimulatorTypes::BruteForceLabyrinth1,     {SimulatorTypes::BruteForceLabyrinth1,     L"Labyrinth 1 - Brute force"}},
		{SimulatorTypes::BruteForceLabyrinth2,     {SimulatorTypes::BruteForceLabyrinth1,     L"Labyrinth 2 - Brute force"}}
	};

	template <typename T_, typename... Args_>
	void newThis(T_* &ptr, Args_&&... args)
		{ ptr = new T_(PL_FWD(args)...); }

	[[nodiscard]] float getCellSize(float zoomLevel) noexcept
	{
		if(zoomLevel < 0)
			return 1 / std::floor(-zoomLevel + 1);

		return std::floor(zoomLevel + 1);
	}

	template <typename UniverseViewType_>
		requires (UniverseViewType_::is_const_view)
	class GLDisplay : public wxGLCanvas
	{
	public:

		GLDisplay(wxWindow *parent) :
			wxGLCanvas(parent, wxID_ANY, nullptr, wxDefaultPosition, wxDefaultSize, 0, L"GLCanvas", wxNullPalette)
		{
			Bind(wxEVT_PAINT, &GLDisplay::onPaint, this);

			pl::make_unique(mGLContext, this);
		}

		void setUniverse(const UniverseViewType_ s) noexcept { mUniverse = s; }
		[[nodiscard]] auto getUniverse() const noexcept { return mUniverse; }

		void setZoomLevel(float s) noexcept { mZoomLevel = s; }
		[[nodiscard]] auto getZoomLevel() const noexcept { return mZoomLevel; }

		void setViewPos(Point2Dr s) noexcept { mViewPos = s; }
		[[nodiscard]] auto getViewPos() const noexcept { return mViewPos; }

		void setBrushPos(Point2Ds s) noexcept { mBrushPos = s; }
		[[nodiscard]] auto getBrushPos() const noexcept { return mBrushPos; }

		void setBrushDim(Point2D s) noexcept { mBrushDim = s; }
		[[nodiscard]] auto getBrushDim() const noexcept { return mBrushDim; }

		void setSelectionPos(Point2D s) noexcept { mSelPos = s; }
		[[nodiscard]] auto getSelectionPos() const noexcept { return mSelPos; }

		void setSelectionDim(Point2D s) noexcept { mSelDims = s; }
		[[nodiscard]] auto getSelectionDim() const noexcept { return mSelDims; }

		void setZoomType(conwaygol::SnapshotType s) noexcept { mZoomType = s; }
		[[nodiscard]] auto getZoomType() const noexcept { return mZoomType; }

		void snapshot()
		{
			const auto viewSize = GetSize();
			const auto cellSize = getCellSize(mZoomLevel);

			mUniverse | sa::mode::shared |
				[&](const auto &uni)
				{
					mUniDims = uni.getDimensions();

					// Snapshot visible portion of the universe.
					mViewPosSnap = mViewPos;
					mGridStart = {
						int(mViewPosSnap[0] * cellSize),
						int(mViewPosSnap[1] * cellSize)
					};
					mNbViewed = {
						(int)std::ceil(viewSize.GetWidth() / cellSize) + 1,
						(int)std::ceil(viewSize.GetHeight() / cellSize) + 1
					};
					mCellStart = {
						std::max<int>(0, (int)mViewPosSnap[0]),
						std::max<int>(0, (int)mViewPosSnap[1])
					};
					mDrawStart = {
						int((mCellStart[0] - mViewPosSnap[0]) * cellSize),
						int((mCellStart[1] - mViewPosSnap[1]) * cellSize)
					};

					if(mViewPosSnap[0] < 0)
						mNbViewed[0] = std::ceil(mNbViewed[0] + mViewPosSnap[0]);

					if(mViewPosSnap[1] < 0)
						mNbViewed[1] = std::ceil(mNbViewed[1] + mViewPosSnap[1]);

					mNbViewed = {
						std::max<int>(0, std::min<int>({mNbViewed[0], (int)mUniDims[0] - mCellStart[0], (int)mUniDims[0]})),
						std::max<int>(0, std::min<int>({mNbViewed[1], (int)mUniDims[1] - mCellStart[1], (int)mUniDims[1]}))
					};

					if(mZoomLevel < 0)
						mSnapshot.setDimensions({Point2D::value_type(mNbViewed[0] * cellSize), Point2D::value_type(mNbViewed[1] * cellSize)});
					else
						mSnapshot.setDimensions({(Point2D::value_type)mNbViewed[0], (Point2D::value_type)mNbViewed[1]});

					// Reduce lock contention by making a snapshot of visible cells
					uni.snapshot({(Point2D::value_type)mCellStart[0], (Point2D::value_type)mCellStart[1]}, mSnapshot, mZoomLevel < 0 ? -mZoomLevel + 1 : 1, mZoomType);
				};
		}

	private:

		std::unique_ptr<wxGLContext> mGLContext;
		UniverseViewType_ mUniverse;

		float mZoomLevel = 0;
		conwaygol::SnapshotType mZoomType = conwaygol::SnapshotType::ApproximateRandom;
		Point2Dr mViewPos{}, mViewPosSnap{};
		Point2Ds mBrushPos{-1, -1};
		Point2D mBrushDim{1, 1};

		conwaygol::Structure<std::uint8_t> mSnapshot;
		Point2Ds mNbViewed{}, mCellStart{}, mDrawStart{}, mGridStart{};
		Point2D mUniDims{};

		Point2D mSelPos{}, mSelDims{};

		void onPaint(wxPaintEvent &evt)
		{
			evt.Skip();

			wxPaintDC dc(this);
			PrepareDC(dc);

			if(!mUniverse)
				return;

			SetCurrent(*mGLContext);

			const auto viewSize = GetSize();
			const auto cellSize = getCellSize(mZoomLevel);
			const auto cellSizeInt = std::max<int>(cellSize, 1);

			glClearColor(0, 0, 0, 0);
			glClear(GL_COLOR_BUFFER_BIT);

			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			glOrtho(0, viewSize.GetWidth(), viewSize.GetHeight(), 0, 0, 100);

			glMatrixMode(GL_MODELVIEW);

			glViewport(0, 0, viewSize.GetWidth(), viewSize.GetHeight());

				// Draw grid lines
				if(cellSizeInt > 2)
				{
					glColor3f(0.1, 0.1, 0.1);

					const auto
						stx = mDrawStart[0] - 1,
						sty = mDrawStart[1] - 1,
						lx = stx + (int)mNbViewed[0] * cellSizeInt + 1,
						ly = sty + (int)mNbViewed[1] * cellSizeInt + 1;

					glBegin(GL_LINES);
						for(int y=1; y<mNbViewed[1]; ++y)
						{
							const auto p = sty + y * cellSizeInt;
							glVertex2i(stx, p);
							glVertex2i(lx, p);
						}

						for(int x=1; x<mNbViewed[0]; ++x)
						{
							const auto p = stx + x * cellSizeInt;
							glVertex2i(p, sty);
							glVertex2i(p, ly);
						}
					glEnd();
				}


				// Draw cells
				const auto cellDrawSize = cellSizeInt > 2 ? std::max<int>(1, cellSizeInt - 1) : cellSizeInt;
				const auto snapshotDims = mSnapshot.getDimensions();

				glBegin(cellSizeInt == 1 ? GL_POINTS : GL_QUADS);
				{
					auto it = mSnapshot.cbegin();
					for(int y=0; y<(int)snapshotDims[1]; ++y)
					{
						for(int x=0; x<(int)snapshotDims[0]; ++x)
						{
							if(const auto cur = *it++; cur)
							{
								glColor3f((float)cur / std::numeric_limits<typename std::decay_t<decltype(mSnapshot)>::value_type>::max(), 0, 0);
								Point2Ds sp {mDrawStart[0] + x * cellSizeInt, mDrawStart[1] + y * cellSizeInt};

								glVertex2i(sp[0], sp[1]);

								if(cellSizeInt > 1)
								{
									glVertex2i(sp[0], sp[1] + cellDrawSize);
									glVertex2i(sp[0] + cellDrawSize, sp[1] + cellDrawSize);
									glVertex2i(sp[0] + cellDrawSize, sp[1]);
								}
							}
						}
					}
				}
				glEnd();


				// Draw universe boundaries
				{
					const int
						stx = -mViewPosSnap[0] * cellSize - 1,
						sty = -mViewPosSnap[1] * cellSize - 1,
						lx = stx + (int)mUniDims[0] * cellSize,
						ly = sty + (int)mUniDims[1] * cellSize;

					glColor3f(0, 1, 0);

					glBegin(GL_LINE_LOOP);
						glVertex2i(stx, sty);
						glVertex2i(stx, ly + 1);
						glVertex2i(lx, ly);
						glVertex2i(lx, sty);
					glEnd();
				}


				// Draw selection if present
				if(mSelDims[0] && mSelDims[1])
				{
					const int
						stx = (-mViewPosSnap[0] + mSelPos[0]) * cellSize - 1,
						sty = (-mViewPosSnap[1] + mSelPos[1]) * cellSize - 1,
						lx = stx + (int)mSelDims[0] * cellSize,
						ly = sty + (int)mSelDims[1] * cellSize;

					glColor3f(0, 0.5, 0.5);

					glBegin(GL_LINE_LOOP);
						glVertex2i(stx, sty);
						glVertex2i(stx, ly + 1);
						glVertex2i(lx, ly);
						glVertex2i(lx, sty);
					glEnd();
				}


				// Draw brush
				if(mBrushPos[0] >= 0 && mBrushPos[1] >= 0)
				{
					const int
						stx = (-mViewPosSnap[0] + mBrushPos[0]) * cellSize - 1,
						sty = (-mViewPosSnap[1] + mBrushPos[1]) * cellSize - 1,
						lx = stx + (int)mBrushDim[0] * cellSize,
						ly = sty + (int)mBrushDim[1] * cellSize;

					glColor3f(0, 0, 1);

					glBegin(GL_LINE_LOOP);
						glVertex2i(stx, sty);
						glVertex2i(stx, ly + 1);
						glVertex2i(lx, ly);
						glVertex2i(lx, sty);
					glEnd();
				}

			glFlush();

			SwapBuffers();
		}

	};


	class OptionsPanel : public wxScrolledWindow
	{
	public:

		struct FormData
		{
			Point2D size{};
			int zoom = 0;
			conwaygol::SnapshotType zoomType = conwaygol::SnapshotType::AccurateAverage;
			std::chrono::milliseconds speed{};
			float refreshRate = 0;
			bool wrap = false, moveCurrent = false;
			SimulatorTypes simType = SimulatorTypes::BruteForce;
		};

		std::function<void(const FormData &)> mEvtFormHandler;
		std::function<void(int)> mEvtZoomChange;
		std::function<void(std::chrono::milliseconds)> mEvtSpeedChange;
		std::function<void(std::size_t)> mEvtFastForward;
		std::function<void()> mEvtRandomFill, mEvtClear;
		std::function<void(bool)> mEvtWrap;
		std::function<void(float)> mEvtRefreshRate;
		std::function<void(conwaygol::SnapshotType)> mEvtZoomType;
		std::function<void(SimulatorTypes)> mEvtSimType;

		OptionsPanel(wxWindow *parent) :
			wxScrolledWindow(parent)
		{
			auto noteBook = new wxNotebook(this, wxID_ANY);
			newThis(wndSimOptions, noteBook, wxID_ANY);
			newThis(wndFastForward, noteBook, wxID_ANY);

			auto sizer = new wxBoxSizer(wxVERTICAL);
			SetSizer(sizer);

			sizer->Add(noteBook, wxSizerFlags(1).Expand());

			noteBook->Freeze();
			noteBook->AddPage(wndSimOptions, L"Simulation", true);
			noteBook->AddPage(wndFastForward, L"Fast forward");

			// Simulation tab
			{
				auto wnd = wndSimOptions;

				wnd->ShowScrollbars(wxSHOW_SB_DEFAULT, wxSHOW_SB_DEFAULT);
				wnd->SetScrollRate(10, 10);

				auto sizer = new wxBoxSizer(wxVERTICAL);
				wnd->SetSizer(sizer);

				{
					auto ctrlSizer = new wxBoxSizer(wxHORIZONTAL);

					auto lbl = new wxStaticText(wnd, wxID_ANY, L"Type: ");
					ctrlSizer->Add(lbl, wxSizerFlags().Align(wxALIGN_CENTER_VERTICAL));

					newThis(ctrlSimType, wnd, wxID_ANY);

					for(const auto &st : simTypes)
						ctrlSimType->Append(st.second.name);

					ctrlSizer->Add(ctrlSimType, wxSizerFlags().Align(wxALIGN_CENTER_VERTICAL));

					sizer->Add(ctrlSizer, wxSizerFlags().Expand().Border());

				}

				{
					auto ctrlSizer = new wxBoxSizer(wxHORIZONTAL);

					auto lbl = new wxStaticText(wnd, wxID_ANY, L"Width: ");
					ctrlSizer->Add(lbl, wxSizerFlags().Align(wxALIGN_CENTER_VERTICAL));

					newThis(ctrlWidth, wnd, wxID_ANY);
					auto size = ctrlWidth->GetSize();
					size.x = 100;
					ctrlWidth->SetSize(size);
					ctrlSizer->Add(ctrlWidth, wxSizerFlags().Align(wxALIGN_CENTER_VERTICAL));

					sizer->Add(ctrlSizer, wxSizerFlags().Expand().Border());
				}

				{
					auto ctrlSizer = new wxBoxSizer(wxHORIZONTAL);

					auto lbl = new wxStaticText(wnd, wxID_ANY, L"Height: ");
					ctrlSizer->Add(lbl, wxSizerFlags().Align(wxALIGN_CENTER_VERTICAL));

					newThis(ctrlHeight, wnd, wxID_ANY);
					auto size = ctrlHeight->GetSize();
					size.x = 100;
					ctrlHeight->SetSize(size);
					ctrlSizer->Add(ctrlHeight, wxSizerFlags().Align(wxALIGN_CENTER_VERTICAL));

					sizer->Add(ctrlSizer, wxSizerFlags().Expand().Border());
				}

				newThis(ctrlNew, wnd, wxID_ANY, L"New universe");
				sizer->Add(ctrlNew, wxSizerFlags().Expand().Border());

				newThis(ctrlResize, wnd, wxID_ANY, L"Resize current universe");
				sizer->Add(ctrlResize, wxSizerFlags().Expand().Border());

				newThis(ctrlClear, wnd, wxID_ANY, L"Clear");
				sizer->Add(ctrlClear, wxSizerFlags().Expand().Border());

				newThis(ctrlRandomFill, wnd, wxID_ANY, L"Random fill");
				sizer->Add(ctrlRandomFill, wxSizerFlags().Expand().Border());

				newThis(ctrlWrap, wnd, wxID_ANY, L"Wrap around");
				sizer->Add(ctrlWrap, wxSizerFlags().Expand().Border());

				{
					auto lbl = new wxStaticText(wnd, wxID_ANY, L"Zoom: ");
					sizer->Add(lbl, wxSizerFlags().Expand().Border());

					newThis(ctrlZoom, wnd, wxID_ANY, 0, -20, 20, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL | wxSL_LABELS);

					sizer->Add(ctrlZoom, wxSizerFlags().Expand().Border());
				}

				{
					auto ctrlSizer = new wxBoxSizer(wxHORIZONTAL);

					auto lbl = new wxStaticText(wnd, wxID_ANY, L"Zoom type: ");
					ctrlSizer->Add(lbl, wxSizerFlags().Align(wxALIGN_CENTER_VERTICAL));

					newThis(ctrlZoomType, wnd, wxID_ANY);

					ctrlZoomType->Append(L"Accurate averaging");
					ctrlZoomType->Append(L"Approximate");
					ctrlZoomType->Append(L"Approximate random");

					ctrlSizer->Add(ctrlZoomType, wxSizerFlags().Align(wxALIGN_CENTER_VERTICAL));

					sizer->Add(ctrlSizer, wxSizerFlags().Expand().Border());
				}

				{
					auto lbl = new wxStaticText(wnd, wxID_ANY, L"Delay between generations (milliseconds): ");
					sizer->Add(lbl, wxSizerFlags().Expand().Border());

					newThis(ctrlSpeed, wnd, wxID_ANY, 0, 0, 1000, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL | wxSL_LABELS);

					sizer->Add(ctrlSpeed, wxSizerFlags().Expand().Border());
				}

				{
					auto ctrlSizer = new wxBoxSizer(wxHORIZONTAL);

					auto lbl = new wxStaticText(wnd, wxID_ANY, L"Display refresh rate (FPS): ");
					ctrlSizer->Add(lbl, wxSizerFlags().Align(wxALIGN_CENTER_VERTICAL));

					newThis(ctrlRefreshRate, wnd, wxID_ANY);
					auto size = ctrlRefreshRate->GetSize();
					size.x = 100;
					ctrlRefreshRate->SetSize(size);
					ctrlSizer->Add(ctrlRefreshRate, wxSizerFlags().Align(wxALIGN_CENTER_VERTICAL));

					sizer->Add(ctrlSizer, wxSizerFlags().Expand().Border());
				}

				ctrlSimType->Bind(wxEVT_CHOICE,

					[this](wxCommandEvent &)
					{
						if(mEvtSimType)
							mEvtSimType( (SimulatorTypes)ctrlSimType->GetSelection() );
					},

					ctrlSimType->GetId()

				);

				ctrlNew->Bind(wxEVT_COMMAND_BUTTON_CLICKED,

					[this](wxCommandEvent &)
					{
						if(mEvtFormHandler)
						{
							if(auto data = getData(); data)
							{
								data->moveCurrent = false;
								mEvtFormHandler(*data);
							}
						}
					},

					ctrlNew->GetId()

				);

				ctrlResize->Bind(wxEVT_COMMAND_BUTTON_CLICKED,

					[this](wxCommandEvent &)
					{
						if(mEvtFormHandler)
						{
							if(auto data = getData(); data)
							{
								data->moveCurrent = true;
								mEvtFormHandler(*data);
							}
						}
					},

					ctrlResize->GetId()

				);

				ctrlClear->Bind(wxEVT_COMMAND_BUTTON_CLICKED,

					[this](wxCommandEvent &)
					{
						if(mEvtClear)
							mEvtClear();
					},

					ctrlClear->GetId()

				);

				ctrlZoom->Bind(wxEVT_SLIDER,

					[this](wxCommandEvent &)
					{
						if(mEvtZoomChange)
							mEvtZoomChange(ctrlZoom->GetValue());
					},

					ctrlZoom->GetId()

				);

				ctrlSpeed->Bind(wxEVT_SLIDER,

					[this](wxCommandEvent &)
					{
						if(mEvtSpeedChange)
							mEvtSpeedChange(std::chrono::milliseconds{ctrlSpeed->GetValue()});
					},

					ctrlSpeed->GetId()

				);

				ctrlWrap->Bind(wxEVT_CHECKBOX,

					[this](wxCommandEvent &)
					{
						if(mEvtWrap)
							mEvtWrap(ctrlWrap->GetValue());
					},

					ctrlWrap->GetId()

				);

				ctrlRandomFill->Bind(wxEVT_COMMAND_BUTTON_CLICKED,

					[this](wxCommandEvent &)
					{
						if(mEvtRandomFill)
							mEvtRandomFill();
					},

					ctrlRandomFill->GetId()

				);

				ctrlRefreshRate->Bind(wxEVT_TEXT,

					[this](wxCommandEvent &)
					{
						if(mEvtRefreshRate)
						{
							try
							{
								mEvtRefreshRate( std::stof(ctrlRefreshRate->GetValue().ToStdString()) );
							}
							catch(...) {}
						}
					},

					ctrlRefreshRate->GetId()

				);

				ctrlZoomType->Bind(wxEVT_CHOICE,

					[this](wxCommandEvent &)
					{
						if(mEvtZoomType)
							mEvtZoomType( (conwaygol::SnapshotType)ctrlZoomType->GetSelection() );
					},

					ctrlZoomType->GetId()

				);

			}

			// Fast forward tab
			{
				auto wnd = wndFastForward;

				wnd->ShowScrollbars(wxSHOW_SB_DEFAULT, wxSHOW_SB_DEFAULT);
				wnd->SetScrollRate(10, 10);

				auto sizer = new wxBoxSizer(wxVERTICAL);
				wnd->SetSizer(sizer);

				{
					auto ctrlSizer = new wxBoxSizer(wxHORIZONTAL);

					auto lbl = new wxStaticText(wnd, wxID_ANY, L"Steps: ");
					ctrlSizer->Add(lbl, wxSizerFlags().Align(wxALIGN_CENTER_VERTICAL));

					newThis(ctrlFastForward, wnd, wxID_ANY);
					auto size = ctrlFastForward->GetSize();
					size.x = 100;
					ctrlFastForward->SetSize(size);
					ctrlSizer->Add(ctrlFastForward, wxSizerFlags().Align(wxALIGN_CENTER_VERTICAL));

					newThis(ctrlBtnFastForward, wnd, wxID_ANY, L"Go");
					ctrlSizer->Add(ctrlBtnFastForward, wxSizerFlags().Align(wxALIGN_CENTER_VERTICAL));

					sizer->Add(ctrlSizer, wxSizerFlags().Expand().Border());
				}

				newThis(ctrlFastForwardTimer, wnd, wxID_ANY, L"");
				sizer->Add(ctrlFastForwardTimer, wxSizerFlags().Expand().Border());

				newThis(ctrlFastForwardStepsPS, wnd, wxID_ANY, L"");
				sizer->Add(ctrlFastForwardStepsPS, wxSizerFlags().Expand().Border());

				newThis(crtlFastForwardGauge, wnd, wxID_ANY, 100);
				sizer->Add(crtlFastForwardGauge, wxSizerFlags().Expand().Border());

				setFastForwardTimer({}, 0);

				ctrlBtnFastForward->Bind(wxEVT_COMMAND_BUTTON_CLICKED,

					[this](wxCommandEvent &)
					{
						if(mEvtFastForward)
						{
							std::size_t steps = 0;

							try
							{
								steps = std::stoi(ctrlFastForward->GetValue().ToStdString());
							}
							catch(const std::invalid_argument &e)
							{
								wxMessageBox(L"Enter a valid number of steps to simulate.", L"Invalid parameter", wxOK | wxCENTRE | wxICON_ERROR, this);
							}
							catch(const std::out_of_range &e)
							{
								wxMessageBox(L"Enter a valid number of steps to simulate.", L"Invalid parameter", wxOK | wxCENTRE | wxICON_ERROR, this);
							}

							if(steps)
								mEvtFastForward( std::max<int>(0, steps) );
						}
					},

					ctrlBtnFastForward->GetId()

				);
			}

			noteBook->Thaw();
		}

		void setFastForwardTimer(std::chrono::duration<double> dur, std::size_t nbTicks)
		{
			ctrlFastForwardTimer->SetLabel(L"Fast forward timer: " + (dur.count() ? std::to_wstring(dur.count()) + L" sec" : L""));
			ctrlFastForwardStepsPS->SetLabel(L"Steps per second: " + (dur.count() ? std::to_wstring(nbTicks / dur.count()) : L""));
		}

		void setData(const FormData &data)
		{
			ctrlWidth->ChangeValue(std::to_string(data.size[0]));
			ctrlHeight->ChangeValue(std::to_string(data.size[1]));

			ctrlZoom->SetValue(data.zoom);
			ctrlZoomType->SetSelection((int)data.zoomType);
			ctrlSpeed->SetValue(std::max<int>(0, data.speed.count()));
			ctrlWrap->SetValue(data.wrap);
			ctrlRefreshRate->ChangeValue(std::to_string(data.refreshRate));
			ctrlSimType->SetSelection((int)data.simType);
		}

		[[nodiscard]] std::optional<FormData> getData()
		{
			FormData data;

			const auto validationError = [&](auto&& message)
				{
					wxMessageBox(PL_FWD(message), L"Validation error", wxOK | wxCENTRE | wxICON_ERROR, this);
				};

			try
			{
				data.size = {
					(Point2D::value_type)std::max<int>(0, std::stoi(ctrlWidth->GetValue().ToStdString())),
					(Point2D::value_type)std::max<int>(0, std::stoi(ctrlHeight->GetValue().ToStdString()))
				};
			}
			catch(const std::invalid_argument &e)
			{
				validationError(L"Enter valid dimensions for the universe.");
				return std::nullopt;

			}
			catch(const std::out_of_range &e)
			{
				validationError(L"Enter valid dimensions for the universe.");
				return std::nullopt;
			}

			if(!data.size[0])
			{
				validationError(L"Width must be > 0.");
				return std::nullopt;
			}

			if(!data.size[1])
			{
				validationError(L"Height must be > 0.");
				return std::nullopt;
			}

			data.zoom = ctrlZoom->GetValue();
			data.zoomType = (conwaygol::SnapshotType)ctrlZoomType->GetSelection();
			data.speed = std::chrono::milliseconds{ctrlSpeed->GetValue()};
			data.wrap = ctrlWrap->GetValue();
			data.refreshRate = std::stof(ctrlRefreshRate->GetValue().ToStdString());
			data.simType = (SimulatorTypes)ctrlSimType->GetSelection();

			return data;
		}

		void setFastForwardGauge(int pos)
		{
			crtlFastForwardGauge->SetValue(pos);
		}

		void setEnabledEdit(bool enable)
		{
			ctrlBtnFastForward->Enable(enable);
			wndSimOptions->Enable(enable);
		}

	private:
		wxScrolledWindow *wndSimOptions, *wndFastForward;
		wxTextCtrl *ctrlWidth, *ctrlHeight, *ctrlFastForward, *ctrlRefreshRate;
		wxButton *ctrlNew, *ctrlResize, *ctrlClear, *ctrlBtnFastForward, *ctrlRandomFill;
		wxSlider *ctrlZoom, *ctrlSpeed;
		wxStaticText *ctrlFastForwardTimer, *ctrlFastForwardStepsPS;
		wxCheckBox *ctrlWrap;
		wxChoice *ctrlZoomType, *ctrlSimType;
		wxGauge *crtlFastForwardGauge;

	};

	class WxApplication : public wxApp
	{
	public:

		GLDisplay<conwaygol::SharedUniverseConstViewType> *mGLDispl;
		OptionsPanel *mOptionsPanel;
		wxFrame *mMainWnd;
		wxToolBar *mToolBar;

		static constexpr int
			ToolIdPlayPause = 1,
			ToolIdSave = 2,
			ToolIdLoad = 3,
			ToolIdQuit = 4,
			ToolIdInsert = 5,
			ToolIdClearInsert = 6,
			ToolIdAbout = 7;

		std::vector<wxAcceleratorEntry> mAccelerators;

		std::string mLastPath, mLastStructPath;

		// Simulation thread
		enum class SimulationStatus
		{
			Active, Paused, Stop
		};

		std::atomic<std::chrono::milliseconds> mSimSpeed{std::chrono::milliseconds{1000 / 10}};
		float mMaxRefreshRate = 0;
		std::atomic<SimulationStatus> mSimStatus{SimulationStatus::Paused};
		std::condition_variable mSimCV;
		mutable std::mutex mSimCVmut;
		std::thread mSimulationThread;
		wxTimer *mDrawTimer;
		std::atomic<std::size_t> mSimFrameNo{0};
		std::size_t mLastFrameDrawn = 0;

		// The simulator
		using SimulatorSharedAccessType = sa::shared_upgrade<std::experimental::propagate_const<std::unique_ptr<conwaygol::ISimulator>>>;
		SimulatorSharedAccessType mSimulator;

		class SimulatorProperties
		{
		private:
			bool mWrapAround = false;

		public:

			SimulatorSharedAccessType &mSimulator;
			SimulatorTypes mSimulationType = SimulatorTypes::Tiled;

			SimulatorProperties(SimulatorSharedAccessType &sim) :
				mSimulator{sim} {}

			[[nodiscard]] bool getWrap() const { return mWrapAround; }
			void setWrap(bool v)
			{
				mWrapAround = v;
				(**mSimulator.access(sa::mode::excl)).setWrap(v);
			}

		};

		sa::shared_upgrade<SimulatorProperties> mSimulatorProps{sa::in_place_list, mSimulator};

		conwaygol::SharedUniverseViewType mUniverse;
		conwaygol::SharedUniverseConstViewType mConstUniverse;

		// Structure data
		//conwaygol::Structure<std::uint8_t> mStructure;
		conwaygol::RleStructure mStructure;

		// Mouse events
		bool mMouseLeftAction = false, mMouseRightAction = false, mWasRunning = false, mSelectionStarted = false;
		std::atomic<bool> mUniEditEnabled{true};
		wxPoint mMousePos;
		Point2Dr mFStart;

		// Tasks
		struct Task
		{
			std::future<void> future;
		};
		sa::shared<std::list<Task>, std::mutex> mTasks, mNewTasks;
		wxTimer *mTaskTimer, *mProgressTimer;
		std::atomic<int> mProgress{};

		[[nodiscard]] auto getMinViewPos() const noexcept
		{
			const auto size = mGLDispl->getZoomLevel() >= 0 ? -10 / (mGLDispl->getZoomLevel() + 1) : 10 * (mGLDispl->getZoomLevel() - 1);
			return Point2Dr{size, size};
		}

		[[nodiscard]] auto createSimulator(SimulatorTypes type) const
		{
			std::unique_ptr<conwaygol::ISimulator> sim;

			switch(type)
			{
			case SimulatorTypes::Tiled:
				sim = std::make_unique<conwaygol::VirtualSimulator<conwaygol::SimulatorTiled<>>>();
				break;
			case SimulatorTypes::BruteForceLabyrinth1:
				sim = std::make_unique<conwaygol::VirtualSimulator<conwaygol::Simulator<conwaygol::rules::Labyrinth1>>>();
				break;
			case SimulatorTypes::BruteForceLabyrinth2:
				sim = std::make_unique<conwaygol::VirtualSimulator<conwaygol::Simulator<conwaygol::rules::Labyrinth2>>>();
				break;
			default:
				sim = std::make_unique<conwaygol::VirtualSimulator<conwaygol::Simulator<>>>();
			}

			return sim;
		}

		void setSimulationType(SimulatorTypes type, SimulatorProperties &simProps)
		{
			simProps.mSimulationType = type;

			mSimulator | sa::mode::excl |
				[&](auto &sim)
				{
					sim = createSimulator(type);
					sim->setWrap(simProps.getWrap());

					mUniverse = sim->getUniverse();
					mConstUniverse = std::as_const(sim)->getUniverse();
				};

			mGLDispl->setUniverse(mConstUniverse);
		}

		void doExit()
		{
			stopSimulation();

			while(!checkTasks()) { std::this_thread::sleep_for(10ms); }

			if(mSimulationThread.joinable())
				mSimulationThread.join();
		}

		void enableEdit(bool enabled)
		{
			mUniEditEnabled = enabled;

			mToolBar->Enable(enabled);
			mOptionsPanel->setEnabledEdit(enabled);
		}

		void setRefreshRate(float rate)
		{
			mMaxRefreshRate = std::clamp<float>(rate, 0, 1000);
		}

		void stopSnapshots()
		{
			mDrawTimer->Stop();
		}

		void startSnapshots()
		{
			mGLDispl->snapshot();
			mDrawTimer->Start( mMaxRefreshRate <= 0 ? 1 : int(1000.0f / mMaxRefreshRate) );
		}

		void snapshotAndResetTimer()
		{
			stopSnapshots();
			startSnapshots();
		}

		void setZoomLevel(float level)
		{
			mGLDispl->setZoomLevel(level);
		}

		void adjustZoom(float newZoom, float posX, float posY)
		{
			mConstUniverse | sa::mode::shared |
				[&](const auto &uni)
				{
					const auto cellSize = getCellSize(mGLDispl->getZoomLevel());
					const auto viewPos = mGLDispl->getViewPos();
					const auto zoomScale = cellSize / getCellSize(newZoom);
					const auto uniDims = uni.getDimensions();
					const auto minViewPos = getMinViewPos();

					setZoomLevel(newZoom);
					mGLDispl->setViewPos({
						std::clamp<float>( viewPos[0] + (posX / cellSize) - zoomScale * (posX / cellSize), minViewPos[0], uniDims[0] - 1 ),
						std::clamp<float>( viewPos[1] + (posY / cellSize) - zoomScale * (posY / cellSize), minViewPos[1], uniDims[1] - 1 )
					});
				};
		}

		void pauseSimulation()
		{
			mSimStatus = SimulationStatus::Paused;
			mToolBar->SetToolNormalBitmap(ToolIdPlayPause, wxArtProvider::GetBitmap("media-playback-start", wxART_MENU));
		}

		void startSimulation()
		{
			mToolBar->SetToolNormalBitmap(ToolIdPlayPause, wxArtProvider::GetBitmap("media-playback-pause", wxART_MENU));

			{
				std::unique_lock waitLock{mSimCVmut};
				mSimStatus = SimulationStatus::Active;
			}

			mSimCV.notify_all();
		}

		void stopSimulation()
		{
			{
				std::unique_lock waitLock{mSimCVmut};
				mSimStatus = SimulationStatus::Stop;
			}

			mSimCV.notify_all();
		}

		void updateOptionsPanel()
		{
			OptionsPanel::FormData data;

			mSimulatorProps | sa::mode::shared |
				[&](auto &simProps)
				{
					data.wrap = simProps.getWrap();
					data.simType = simProps.mSimulationType;
				};

			if(mConstUniverse)
				mConstUniverse | sa::mode::shared |
					[&](const auto &uni)
					{
						data.size = uni.getDimensions();
						data.zoom = (int)mGLDispl->getZoomLevel();
						data.zoomType = mGLDispl->getZoomType();
						data.speed = mSimSpeed;
						data.refreshRate = mMaxRefreshRate;
					};

			mOptionsPanel->setData(data);
		}

		[[nodiscard]] auto getCellPos(const wxMouseEvent &evt) const
		{
			const auto mp = evt.GetPosition();
			const auto uniDims = mConstUniverse.access(sa::mode::shared)->getDimensions();
			const auto viewPos = mGLDispl->getViewPos();
			const auto cellSize = getCellSize(mGLDispl->getZoomLevel());

			return Point2D{
				(Point2D::value_type)std::clamp<int>(mp.x / cellSize + viewPos[0], 0, uniDims[0] - 1),
				(Point2D::value_type)std::clamp<int>(mp.y / cellSize + viewPos[1], 0, uniDims[1] - 1)
			};
		}

		void adjustBrush(const wxMouseEvent &evt)
		{
			const auto pos = getCellPos(evt);
			mGLDispl->setBrushPos({(int)pos[0], (int)pos[1]});
		}

		void placeCell(const wxMouseEvent &evt, bool alive)
		{
			const auto cellPos = getCellPos(evt);

			mUniverse | sa::mode::excl |
				[&](auto &uni)
				{
					const auto uniDims = uni.getDimensions();
					const auto brushDim = mGLDispl->getBrushDim();

					if(mStructure.mData.empty())
					{
						for(std::uint32_t y=0; y<brushDim[1]; ++y)
							for(std::uint32_t x=0; x<brushDim[0]; ++x)
							{
								if(const Point2D p{cellPos[0] + x, cellPos[1] + y};
										p[0] < uniDims[0] && p[1] < uniDims[1])
									uni.set(p, alive);
							}
					}
					else
					{
						conwaygol::insertStructRLE(mStructure.mData.cbegin(), mStructure.mData.cend(), uni, cellPos, brushDim);
					}

				};
		}

		// Load a structure or universe from a file. Return true on success.
		template <typename StructType_>
		bool loadStruct(StructType_ &struc, const std::string &filename)
		{
			std::ifstream file(filename, std::ios_base::binary | std::ios_base::in);

			if(!file)
			{
				displayError("Failed to open file for reading.");
				return false;
			}

			std::cout << "Loading universe/structure from file \"" << filename << "\".\n";

			if(boost::iends_with(filename, ".rle"))
			{
				auto strucWriterSelect =
					[&]
					{
						if constexpr(std::is_same_v<std::decay_t<StructType_>, conwaygol::RleStructure>)
							return conwaygol::RLEWriter{struc};
						else
							return conwaygol::StructureWriter{struc};
					};

				conwaygol::readStructRLE(file, strucWriterSelect());

				if constexpr(std::is_same_v<std::decay_t<StructType_>, conwaygol::RleStructure>)
					struc.mData.shrink_to_fit();
			}
			else
				file >> struc;

			return true;
		}

		template <typename StructType_>
		bool saveStruct(const StructType_ &struc, const std::string &filename)
		{
			std::ofstream file(filename, std::ios_base::binary | std::ios_base::out);

			if(!file)
			{
				displayError("Failed to open file for writing.");
				return false;
			}

			std::cout << "Saving universe/structure to file \"" << filename << "\".\n";

			if(boost::iends_with(filename, ".rle"))
				conwaygol::writeStructRLE(struc, file);
			else
				file << struc;

			return true;
		}

		void openStructure()
		{
			wxFileDialog fileDialog(
				mMainWnd, L"Load structure data", "", "",
				L"All files|*|Game of Life file (*.cgol)|*.cgol|Run Length Encoded file (*.rle)|*.rle",
				wxFD_OPEN
			);

			fileDialog.SetPath(mLastStructPath);

			if(fileDialog.ShowModal() != wxID_CANCEL)
			{
				const auto pathStr = fileDialog.GetPath().ToStdString();

				try
				{
					decltype(mStructure) tempStruct;

					if(loadStruct(tempStruct, pathStr))
					{
						// Only modify current structure on success.
						mStructure = std::move(tempStruct);

						if(mStructure.mData.empty())
						{
							displayError("This structure file is empty.");
							mGLDispl->setBrushDim({1, 1});
							mToolBar->EnableTool(ToolIdClearInsert, false);
						}
						else
						{
							mGLDispl->setBrushDim(mStructure.getDimensions());
							mToolBar->EnableTool(ToolIdClearInsert, true);
						}

						mLastStructPath = pathStr;
					}
				}
				catch(...)
				{
					handleException<true>("Failed to read file.", std::current_exception());
				}

				updateOptionsPanel();

				mGLDispl->Refresh(false);
			}
		}

		void openUniverse()
		{
			wxFileDialog fileDialog(
				mMainWnd, L"Load universe data", "", "",
				L"All files|*|Run Length Encoded file (*.rle)|*.rle|Game of Life file (*.cgol)|*.cgol",
				wxFD_OPEN
			);

			fileDialog.SetPath(mLastPath);

			if(fileDialog.ShowModal() != wxID_CANCEL)
			{
				const auto pathStr = fileDialog.GetPath().ToStdString();

				pauseSimulation();

				// Disable snapshoting while we hold exclusive access to avoid GUI freezing.
				stopSnapshots();
				pl::scoped_guard sg{[&]{ startSnapshots(); }};

				try
				{
					mUniverse | sa::mode::excl |
					mSimulator | sa::mode::shared |
						[&](auto &uni, const auto &sim)
						{
							auto tempUni = sim->createUniverse();

							if(loadStruct(*tempUni, pathStr))
							{
								// Only modify current universe on success.
								uni = std::move(*tempUni);

								mGLDispl->setViewPos(getMinViewPos());

								mLastPath = pathStr;
							}
						};
				}
				catch(...)
				{
					handleException<true>("Failed to read file.", std::current_exception());
				}

				updateOptionsPanel();

				mGLDispl->Refresh(false);
			}
		}

		void saveUniverse()
		try
		{
			stopSnapshots();
			pl::scoped_guard sg{[&]{ startSnapshots(); }};

			mConstUniverse | sa::mode::shared |
				[&](const auto &uni)
				{
					wxFileDialog fileDialog(
						mMainWnd, L"Save universe data", "", "",
						L"Run Length Encoded file (*.rle)|*.rle|Game of Life file (*.cgol)|*.cgol|All files|*",
						wxFD_SAVE|wxFD_OVERWRITE_PROMPT
					);

					fileDialog.SetPath(mLastPath);

					if(fileDialog.ShowModal() != wxID_CANCEL)
					{
						wxFileName path(fileDialog.GetPath());
						auto pathStr = path.GetFullPath().ToStdString();

						if(fileDialog.GetFilterIndex() == 0 && path.GetExt() != "rle")
							pathStr += ".rle";
						else if(fileDialog.GetFilterIndex() == 1 && path.GetExt() != "cgol")
							pathStr += ".cgol";

						saveStruct(uni, pathStr);

						mLastPath = pathStr;
					}
				};
		}
		catch(...)
		{
			handleException<true>("Failed to write to file.", std::current_exception());
		}

		std::string simTypeChoices;
		virtual void OnInitCmdLine(wxCmdLineParser &parser) override
		{
			wxApp::OnInitCmdLine(parser);

			parser.EnableLongOptions();

			simTypeChoices = "Type of simulator (";

			bool first = true;
			for(const auto &st : simTypeStrMap)
			{
				if(!first)
					simTypeChoices += ", ";

				simTypeChoices += st.first;

				first = false;
			}

			simTypeChoices += ")";

			static const wxCmdLineEntryDesc cmdLineDesc[] = {
				{ wxCMD_LINE_PARAM, NULL, NULL, "path to universe file", wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL },
				{ wxCMD_LINE_OPTION, "t", "type", simTypeChoices.c_str(), wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL },
				{ wxCMD_LINE_SWITCH, "w", "wrap", "Universe wraps around", wxCMD_LINE_VAL_NONE, wxCMD_LINE_PARAM_OPTIONAL },
				{ wxCMD_LINE_NONE, nullptr, nullptr, nullptr, wxCMD_LINE_VAL_NONE, 0 }
			};

			parser.SetDesc(cmdLineDesc);
		}

		virtual bool OnCmdLineParsed(wxCmdLineParser &parser) override
		{
			if(!wxApp::OnCmdLineParsed(parser))
				return false;

			bool loadSuccess = false;

			{
				wxString str;

				parser.Found("type", &str);

				if(!str.empty())
				{
					if(auto it = simTypeStrMap.find(str.ToStdString()); it != simTypeStrMap.end())
						setSimulationType(it->second, *mSimulatorProps.access(sa::mode::excl));
					else
					{
						displayError<false>(std::string{"Invalid simulation type "} + str.ToStdString() + ".");
						return false;
					}
				}
				else
					setSimulationType(SimulatorTypes::BruteForce, *mSimulatorProps.access(sa::mode::excl));
			}

			if(parser.GetParamCount())
				loadSuccess = loadStruct(*mUniverse.access(sa::mode::excl), parser.GetParam(0).ToStdString());

			if(!loadSuccess)
				mUniverse.access(sa::mode::excl)->setDimensions({640, 480});

			if(parser.FoundSwitch("wrap"))
				mSimulatorProps.access(sa::mode::excl)->setWrap(true);

			return true;
		}

		void displayErrorImpl(std::string message)
		{
			std::cerr << "Error: " << message << '\n';
			wxMessageBox(message, "Error", wxOK | wxCENTER | wxICON_ERROR, mMainWnd);
		}

		template <bool DeferMessage_ = true>
		void displayError(std::string message, const std::exception &e)
		{
			if constexpr(DeferMessage_)
				CallAfter([this, mes = message + '\n' + e.what()]{ displayErrorImpl(mes); });
			else
				displayErrorImpl(message + '\n' + e.what());
		}

		template <bool DeferMessage_ = true>
		void displayError(std::string message)
		{
			if constexpr(DeferMessage_)
				CallAfter([this, mes = std::move(message)]{ displayErrorImpl(mes); });
			else
				displayErrorImpl(std::move(message));
		}

		template <bool DeferMessage_ = true>
		void handleException(std::string message, std::exception_ptr eptr)
		try { std::rethrow_exception(eptr); }
		catch(const std::exception &e) { displayError<DeferMessage_>(std::move(message), e); }
		catch(...) { displayError<DeferMessage_>(std::move(message)); }

		template <typename FC_>
		void pushExclusiveTask(FC_&& fc)
		{
			pauseSimulation();
			stopSnapshots();
			enableEdit(false);

			pushTask([fc = PL_FWD(fc), this]{
				pl::scoped_guard g{[&]{
						CallAfter([this]{
							enableEdit(true);
							startSnapshots();
							mGLDispl->Refresh(false);
						});
					}};

				fc();
			});
		}

		template <typename FC_>
		void pushTask(FC_&& fc)
		{
			mNewTasks | sa::mode::excl |
				[&](auto &list)
				{
					list.push_back({std::async(PL_FWD(fc))});
				};
		}

		bool checkTasks()
		{
			mNewTasks | sa::mode::excl |
			mTasks    | sa::mode::excl |
				[&](auto &newList, auto &list)
				{
					list.insert(list.end(), std::make_move_iterator(newList.begin()), std::make_move_iterator(newList.end()));
					newList.clear();
				};

			bool isEmpty = true;
			mTasks | sa::mode::excl |
				[&](auto &list)
				{
					for(auto it=list.begin(); it!=list.end(); )
					{
						if(it->future.wait_for(0s) != std::future_status::timeout)
						{
							if(it->future.valid())
								it->future.get();
							it = list.erase(it);
						}
						else
							++it;
					}

					isEmpty = list.empty();
				};

			return isEmpty;
		}

		#ifdef WIN32
			class WinIconProvider : public wxArtProvider
			{
			public:
				WinIconProvider(const wxString &iconPath) :
					mIconPath{iconPath} {}

			protected:

				wxString mIconPath;

				wxString getIconCategory(const wxArtID &id) const
				{
					if(id == wxART_FRAME_ICON)
						return "apps";
					else if(id == wxART_MENU)
						return "actions";

					return {};
				}

				virtual wxBitmap CreateBitmap(const wxArtID &id, const wxArtClient &client, const wxSize &size) override
				{
					wxBitmap bmp;

					wxString catStr = getIconCategory(client);

					if(catStr.empty())
						return {};

					std::string wStr;

					if(size.GetWidth() > 0)
					{
						wStr.resize(10);
						if(auto res = std::to_chars(wStr.data(), wStr.data() + wStr.size(), size.GetWidth()); res.ec == std::errc::value_too_large)
							return bmp;
						else
							wStr.resize(res.ptr - wStr.data());
					}
					else
						wStr = "32";

					std::string hStr;

					if(size.GetHeight() > 0)
					{
						hStr.resize(10);
						if(auto res = std::to_chars(hStr.data(), hStr.data() + hStr.size(), size.GetHeight()); res.ec == std::errc::value_too_large)
							return bmp;
						else
							hStr.resize(res.ptr - hStr.data());
					}
					else
						hStr = "32";

					wxString path = mIconPath + '/' + wStr + 'x' + hStr + "/" + catStr + "/";
					wxString file = path + id + ".png";
					//if(wxFileName::FileExists(file))
						bmp.LoadFile(file, wxBITMAP_TYPE_ANY);
					//else
						//bmp.LoadFile(mIconPath + '/' + wStr + 'x' + hStr + "/" + catStr + "/" + id + ".symbolic.png", wxBITMAP_TYPE_ANY);

					return bmp;
				}

				virtual wxIconBundle CreateIconBundle(const wxArtID &id, const wxArtClient &client) override
				{
					wxIconBundle icons;

					wxString catStr = getIconCategory(client);

					if(catStr.empty())
						return {};

					wxBitmap bmp;

					{
						wxIcon icon;
						bmp.LoadFile(mIconPath + "/16x16/" + catStr + "/" + id + ".png", wxBITMAP_TYPE_ANY);
						icon.CopyFromBitmap(bmp);
						icons.AddIcon(icon);
					}

					{
						wxIcon icon;
						bmp.LoadFile(mIconPath + "/32x32/" + catStr + "/" + id + ".png", wxBITMAP_TYPE_ANY);
						icon.CopyFromBitmap(bmp);
						icons.AddIcon(icon);
					}

					return icons;
				}
			};
		#endif

		virtual bool OnInit() override
		{
			#ifdef _OPENMP
				std::cout << "Maximum number of threads: " << omp_get_max_threads() << '\n';
			#else
				std::cout << "OpenMP support disabled.\n";
			#endif

			#ifdef PL_LIB_NUMA
				if(numa_available() > -1)
					std::cout << "Number of numa nodes: " << pl::numa::node_count() << '\n';
				else
					std::cout << "Numa information not available.\n";
			#else
				std::cout << "NUMA support disabled.\n";
			#endif

			#ifdef PL_HUGEPAGES
				std::cout << "Huge pages support enabled.\n";
			#else
				std::cout << "Huge pages support disabled.\n";
			#endif

			SetAppName("Conway's Game of Lifuuu");
			SetAppDisplayName("Conway's Game of Lifuuu");

			wxInitAllImageHandlers();

			newThis(mTaskTimer, this);

			Bind(wxEVT_TIMER,

				[this](wxTimerEvent &)
				{
					checkTasks();
				},

				mTaskTimer->GetId()
			);

			mTaskTimer->Start(100);

			newThis(mProgressTimer, this);

			Bind(wxEVT_TIMER,

				[this](wxTimerEvent &)
				{
					mOptionsPanel->setFastForwardGauge(mProgress.load());
				},

				mProgressTimer->GetId()
			);

			newThis(mMainWnd,
				nullptr, wxID_ANY, L"Conway's Game of Lifuuu", wxDefaultPosition, wxSize(800, 600),
				wxMAXIMIZE|wxCAPTION|wxRESIZE_BORDER|wxMAXIMIZE_BOX|wxMINIMIZE_BOX|wxCLOSE_BOX|wxSYSTEM_MENU
			);

			SetTopWindow(mMainWnd);

			auto splitter = new wxSplitterWindow(mMainWnd);
			auto rightWindow = new wxWindow(splitter, wxID_ANY);
			newThis(mGLDispl, rightWindow);

			if(!wxApp::OnInit())
				return false;

			newThis(mOptionsPanel, splitter);
			newThis(mToolBar, rightWindow, wxID_ANY);

			#ifdef WIN32
				wxArtProvider::Push(new WinIconProvider(GetTraits()->GetStandardPaths().GetResourcesDir() + "/../icons"));
			#endif

			mMainWnd->SetIcons(wxArtProvider::GetIconBundle("org.norpat.ConwayGoL", wxART_FRAME_ICON));

			mToolBar->AddTool(
				ToolIdLoad, L"Open",
				wxArtProvider::GetBitmap("document-open", wxART_MENU),
				{},
				wxITEM_NORMAL, L"Open universe."
			);
			mToolBar->AddTool(
				ToolIdSave, L"Save",
				wxArtProvider::GetBitmap("document-save-as", wxART_MENU),
				{},
				wxITEM_NORMAL, L"Save universe (ctrl + s)."
			);
			mToolBar->AddSeparator();
			mToolBar->AddTool(
				ToolIdPlayPause, L"Start/pause",
				wxArtProvider::GetBitmap("media-playback-start", wxART_MENU),
				{},
				wxITEM_NORMAL, L"Start and pause simulation (F1)."
			);
			mToolBar->AddSeparator();
			mToolBar->AddTool(
				ToolIdInsert, L"Insert",
				wxArtProvider::GetBitmap("insert-object", wxART_MENU),
				{},
				wxITEM_NORMAL, L"Insert a structure from file."
			);
			mToolBar->AddTool(
				ToolIdClearInsert, L"Clear",
				wxArtProvider::GetBitmap("edit-clear", wxART_MENU),
				{},
				wxITEM_NORMAL, L"Unload structure."
			);
			mToolBar->AddStretchableSpace();
			mToolBar->AddTool(
				ToolIdAbout, L"About",
				wxArtProvider::GetBitmap(wxART_INFORMATION),
				{},
				wxITEM_NORMAL, L"About..."
			);
			mToolBar->AddTool(
				ToolIdQuit, L"Quit",
				wxArtProvider::GetBitmap(wxART_QUIT),
				{},
				wxITEM_NORMAL, L"Quit. (ctrl + q)"
			);

			mToolBar->EnableTool(ToolIdClearInsert, false);

			mToolBar->Realize();

			mToolBar->Bind(wxEVT_MENU,

				[this](wxCommandEvent &)
				{
					if(mSimStatus == SimulationStatus::Active)
						pauseSimulation();
					else
						startSimulation();
				},

				ToolIdPlayPause
			);

			mToolBar->Bind(wxEVT_MENU,

				[this](wxCommandEvent &)
				{
					openUniverse();
				},

				ToolIdLoad
			);

			mToolBar->Bind(wxEVT_MENU,

				[this](wxCommandEvent &)
				{
					saveUniverse();
				},

				ToolIdSave
			);

			mToolBar->Bind(wxEVT_MENU,

				[&](wxCommandEvent &)
				{
					wxAboutDialogInfo info;
					info.AddDeveloper(L"Patrick Northon");
					info.SetCopyright(L"(C) 2020 Patrick Northon");
					info.SetLicence(L"GPL v3 or later");
					info.SetWebSite(L"https://gitlab.com/patlefort/conwaygol");

					info.SetDescription(
						L"This is a simple Conway's game of life simulator written in c++ originally made for a contest in "
						L"Together C & C++ discord channel. Only a brute force algorithm is supported but it is highly optimized.");

					wxIcon icon;
					icon.CopyFromBitmap(wxArtProvider::GetBitmap("org.norpat.ConwayGoL", wxART_FRAME_ICON, {32, 32}));

					info.SetIcon(icon);

					wxAboutBox(info);
				},

				ToolIdAbout
			);

			mToolBar->Bind(wxEVT_MENU,

				[this](wxCommandEvent &)
				{
					mMainWnd->Close();
				},

				ToolIdQuit
			);

			mToolBar->Bind(wxEVT_MENU,

				[this](wxCommandEvent &)
				{
					openStructure();
				},

				ToolIdInsert
			);

			mToolBar->Bind(wxEVT_MENU,

				[this](wxCommandEvent &)
				{
					mStructure.mData.clear();
					mStructure.mData.shrink_to_fit();

					mGLDispl->setBrushDim({1, 1});

					mGLDispl->Refresh(false);
					mToolBar->EnableTool(ToolIdClearInsert, false);
				},

				ToolIdClearInsert
			);

			auto rightSizer = new wxBoxSizer(wxVERTICAL);
			rightWindow->SetSizer(rightSizer);

			rightSizer->Add(mToolBar, wxSizerFlags().Border(wxDOWN, 3).Expand());
			rightSizer->Add(mGLDispl, wxSizerFlags(1).Expand());

			splitter->SplitVertically(mOptionsPanel, rightWindow, 300);

			newThis(mDrawTimer, this);

			Bind(wxEVT_TIMER,

				[this](wxTimerEvent &)
				{
					const std::size_t frameNo = mSimFrameNo;

					if((mGLDispl->getZoomLevel() < 0 && mGLDispl->getZoomType() == conwaygol::SnapshotType::ApproximateRandom) || frameNo != mLastFrameDrawn)
					{
						CallAfter([&]{
							mGLDispl->snapshot();
							mGLDispl->Refresh(false);
							mLastFrameDrawn = mSimFrameNo;
						});
					}

				},

				mDrawTimer->GetId()
			);

			setZoomLevel(2);
			mGLDispl->setViewPos(getMinViewPos());
			setRefreshRate(60);

			enum class AccelEvt : int
			{
				Pause = 1,
				Save,
				Advance1,
				Quit
			};

			mAccelerators.insert(
				mAccelerators.cbegin(),
				{
					{wxACCEL_NORMAL, WXK_F1, (int)AccelEvt::Pause},
					{wxACCEL_CTRL, (int)'s', (int)AccelEvt::Save},
					{wxACCEL_NORMAL, WXK_F2, (int)AccelEvt::Advance1},
					{wxACCEL_CTRL, (int)'q', (int)AccelEvt::Quit}
				}
			);

			#if wxCHECK_VERSION(3, 1, 0)
				mAccelerators.emplace_back(wxACCEL_NORMAL, WXK_MEDIA_PLAY_PAUSE, (int)AccelEvt::Pause);
			#endif

			wxAcceleratorTable accelTable{(int)mAccelerators.size(), mAccelerators.data()};
			mMainWnd->SetAcceleratorTable(accelTable);

			mMainWnd->Bind(wxEVT_MENU,

				[this](wxCommandEvent &)
				{
					if(mSimStatus == SimulationStatus::Active)
						pauseSimulation();
					else
						startSimulation();
				},

				(int)AccelEvt::Pause
			);

			mMainWnd->Bind(wxEVT_MENU,

				[this](wxCommandEvent &)
				{
					mMainWnd->Close();
				},

				(int)AccelEvt::Quit
			);

			mMainWnd->Bind(wxEVT_MENU,

				[this](wxCommandEvent &)
				{
					saveUniverse();
				},

				(int)AccelEvt::Save
			);

			mMainWnd->Bind(wxEVT_MENU,

				[this](wxCommandEvent &)
				{
					pauseSimulation();
					stopSnapshots();
					(**mSimulator.access(sa::mode::excl)).advance();
					startSnapshots();
					mGLDispl->Refresh(false);
				},

				(int)AccelEvt::Advance1
			);

			mOptionsPanel->mEvtSimType =
				[this](SimulatorTypes type)
				{
					stopSnapshots();
					pl::scoped_guard sg{[&]{ startSnapshots(); }};

					pauseSimulation();

					auto simProps = mSimulatorProps.access(sa::mode::excl);

					std::unique_ptr<conwaygol::IUniverse> tempUni;
					const auto oldType = simProps->mSimulationType;
					bool success = false;

					mUniverse | sa::mode::excl |
					mSimulator | sa::mode::shared |
						[&](auto &uni, const auto &sim)
						{
							tempUni = sim->createUniverse();
							*tempUni = std::move(uni);
						};

					setSimulationType(type, *simProps);

					mUniverse | sa::mode::excl |
						[&](auto &uni)
						{
							try
							{
								uni = std::move(*tempUni);
								success = true;
							}
							catch(...)
							{
								handleException<true>("Failed to create universe.", std::current_exception());
							}
						};

					if(!success)
					{
						if(oldType != simProps->mSimulationType)
							setSimulationType(oldType, *simProps);

						*mUniverse.access(sa::mode::excl) = std::move(*tempUni);
					}

					snapshotAndResetTimer();
					mGLDispl->Refresh(false);
				};

			mOptionsPanel->mEvtFormHandler =
				[this](const OptionsPanel::FormData &data)
				{
					stopSnapshots();
					pl::scoped_guard sg{[&]{ startSnapshots(); }};

					pauseSimulation();

					auto simProps = mSimulatorProps.access(sa::mode::excl);

					std::unique_ptr<conwaygol::IUniverse> tempUni;
					Point2D oldSize;
					const auto oldType = simProps->mSimulationType;

					mUniverse | sa::mode::excl |
					mSimulator | sa::mode::shared |
						[&](auto &uni, const auto &sim)
						{
							oldSize = uni.getDimensions();

							if(data.moveCurrent)
							{
								tempUni = sim->createUniverse();
								*tempUni = std::move(uni);
							}
						};

					if(data.simType != simProps->mSimulationType)
						setSimulationType(data.simType, *simProps);

					simProps->setWrap(data.wrap);

					bool success = false;

					mUniverse | sa::mode::excl |
						[&](auto &uni)
						{
							setZoomLevel(data.zoom);
							mGLDispl->setViewPos(getMinViewPos());

							try
							{
								uni.setDimensions(data.size);
								if(data.moveCurrent)
									uni.insert(*tempUni);

								mSimSpeed = data.speed;
								success = true;
							}
							catch(...)
							{
								handleException<true>("Failed to resize universe.", std::current_exception());
							}
						};

					if(!success)
					{
						if(oldType != simProps->mSimulationType)
							setSimulationType(oldType, *simProps);

						mUniverse | sa::mode::excl |
							[&](auto &uni)
							{
								if(data.moveCurrent)
									uni = std::move(*tempUni);
								else
									uni.setDimensions(oldSize);
							};
					}

					//startSimulation();
					snapshotAndResetTimer();
					mGLDispl->Refresh(false);
				};

			mOptionsPanel->mEvtRefreshRate =
				[this](float rate)
				{
					setRefreshRate(rate);
					stopSnapshots();
					startSnapshots();
				};

			mOptionsPanel->mEvtClear =
				[this]
				{
					pauseSimulation();

					mUniverse.access(sa::mode::excl)->zero();

					snapshotAndResetTimer();
					mGLDispl->Refresh(false);
				};

			mOptionsPanel->mEvtWrap =
				[this](bool wrap)
				{
					pauseSimulation();
					mSimulatorProps.access(sa::mode::excl)->setWrap(wrap);
				};

			mOptionsPanel->mEvtZoomChange =
				[this](int zoom)
				{
					adjustZoom(zoom, 0, 0);

					snapshotAndResetTimer();
					mGLDispl->Refresh(false);
				};

			mOptionsPanel->mEvtZoomType =
				[this](conwaygol::SnapshotType type)
				{
					mGLDispl->setZoomType(type);

					snapshotAndResetTimer();
					mGLDispl->Refresh(false);
				};

			mOptionsPanel->mEvtSpeedChange =
				[this](std::chrono::milliseconds speed)
				{
					mSimSpeed = speed;
				};

			mOptionsPanel->mEvtFastForward =
				[this](std::size_t ticks)
				{
					if(!mUniEditEnabled)
						return;

					mProgress = 0;
					mProgressTimer->Start(100);

					pushExclusiveTask([ticks, this]{

						mSimulator | sa::mode::excl |
							[&](auto &sim)
							{
								const std::size_t nbPerStep = std::max<std::size_t>(1, ticks / 100);
								const std::size_t nbSteps = ticks / nbPerStep;
								const float stepSize = 100.0f / (float)nbSteps;

								const auto profileStart = atomicClock();

								for(std::size_t i=0; i<nbSteps; ++i)
								{
									sim->advance(nbPerStep, std::bool_constant<true>{});
									//CallAfter([=, pos = stepSize * (i + 1)]{ mOptionsPanel->setFastForwardGauge((int)pos); });
									mProgress = stepSize * (i + 1);
								}

								if(const std::size_t leftOver = ticks - nbPerStep * nbSteps; leftOver)
								{
									sim->advance(leftOver, std::bool_constant<true>{});
									//CallAfter([=]{ mProgressTimer->Stop(); mOptionsPanel->setFastForwardGauge(100); });
								}

								CallAfter([this]{ mProgressTimer->Stop(); mOptionsPanel->setFastForwardGauge(100); });

								CallAfter([duration = atomicClock() - profileStart, ticks, this]{
									mOptionsPanel->setFastForwardTimer(std::chrono::duration_cast<std::chrono::duration<double>>(duration), ticks);
								});
							};

					});
				};

			mOptionsPanel->mEvtRandomFill =
				[this]
				{
					if(!mUniEditEnabled)
						return;

					pauseSimulation();

					mUniverse | sa::mode::excl |
						[&](auto &uni)
						{
							const auto dims = uni.getDimensions();
							const double threshold = 0.8;
							//const auto nbThreads = std::min<std::size_t>(dims[0] * dims[1] / (1024 * 16), omp_get_max_threads());

							//#pragma omp parallel num_threads(nbThreads)
							{
								std::mt19937 rd{std::random_device{}()};
								std::mt19937::result_type rdthreshold = double(rd.max() - rd.min()) * threshold + rd.min();

								//#pragma omp for schedule(static)
								for(std::uint32_t y=0; y<dims[1]; ++y)
									for(std::uint32_t x=0; x<dims[0]; ++x)
										uni.set({x, y}, rd() >= rdthreshold);
							}
						};

					snapshotAndResetTimer();
					mGLDispl->Refresh(false);
				};

			mGLDispl->Bind(wxEVT_LEFT_DOWN,

				[this](wxMouseEvent &evt)
				{
					evt.Skip();

					if(!mUniEditEnabled)
						return;

					mMouseLeftAction = true;
					mGLDispl->CaptureMouse();
					mWasRunning = mSimStatus == SimulationStatus::Active;
					pauseSimulation();

					if(evt.ControlDown())
					{
						mGLDispl->setSelectionPos(getCellPos(evt));
						mGLDispl->setSelectionDim({1, 1});
						mSelectionStarted = true;
					}
					else
					{
						placeCell(evt, !evt.ShiftDown());
						snapshotAndResetTimer();
					}

					mGLDispl->Refresh(false);
				},

				mGLDispl->GetId()

			);

			mGLDispl->Bind(wxEVT_RIGHT_DOWN,

				[this](wxMouseEvent &evt)
				{
					evt.Skip();

					if(!mUniEditEnabled)
						return;

					mMouseRightAction = true;
					mGLDispl->CaptureMouse();
					mFStart = mGLDispl->getViewPos();
					mMousePos = evt.GetPosition();
				},

				mGLDispl->GetId()

			);

			mGLDispl->Bind(wxEVT_MOTION,

				[this](wxMouseEvent &evt)
				{
					evt.Skip();

					if(!mUniEditEnabled)
						return;

					adjustBrush(evt);

					if(mMouseLeftAction && evt.LeftIsDown())
					{
						if(mSelectionStarted)
						{
							const auto pos = getCellPos(evt);
							const auto curPos = mGLDispl->getSelectionPos();

							mGLDispl->setSelectionDim({
								pos[0] < curPos[0] ? 1 : pos[0] - curPos[0] + 1,
								pos[1] < curPos[1] ? 1 : pos[1] - curPos[1] + 1
							});
						}
						else
						{
							placeCell(evt, !evt.ShiftDown());

							CallAfter([&]{
								snapshotAndResetTimer();
								mGLDispl->Refresh(false);
							});
						}
					}

					if(mMouseRightAction && evt.RightIsDown())
					{
						const auto mp = evt.GetPosition();
						const auto uniDims = mConstUniverse.access(sa::mode::shared)->getDimensions();
						const auto cellSize = getCellSize(mGLDispl->getZoomLevel());
						const auto minViewPos = getMinViewPos();

						mFStart[0] -= float(mp.x - mMousePos.x) / cellSize;
						mFStart[1] -= float(mp.y - mMousePos.y) / cellSize;

						mGLDispl->setViewPos({
							std::clamp<float>(mFStart[0], minViewPos[0], uniDims[0] - 1),
							std::clamp<float>(mFStart[1], minViewPos[1], uniDims[1] - 1)
						});

						mMousePos = mp;

						CallAfter([&]{
							snapshotAndResetTimer();
							mGLDispl->Refresh(false);
						});
					}

					mGLDispl->Refresh(false);
				},

				mGLDispl->GetId()

			);

			mGLDispl->Bind(wxEVT_LEFT_UP,

				[this](wxMouseEvent &evt)
				{
					evt.Skip();

					if(!mUniEditEnabled)
						return;

					if(mMouseLeftAction)
					{
						mMouseLeftAction = false;
						mGLDispl->ReleaseMouse();

						if(mSelectionStarted)
						{
							mSelectionStarted = false;

							const auto selDims = mGLDispl->getSelectionDim();
							const auto selPos = mGLDispl->getSelectionPos();

							mStructure.setDimensions(selDims);

							conwaygol::snapshotStructRLE(*mConstUniverse.access(sa::mode::shared), std::back_inserter(mStructure.mData), selPos, selDims);

							mGLDispl->setSelectionDim({});

							if(mStructure.mData.empty())
							{
								mGLDispl->setBrushDim({1, 1});
								mToolBar->EnableTool(ToolIdClearInsert, false);
							}
							else
							{
								mGLDispl->setBrushDim(selDims);
								mToolBar->EnableTool(ToolIdClearInsert, true);
							}
						}

						if(mWasRunning)
							startSimulation();
					}
				},

				mGLDispl->GetId()

			);

			mGLDispl->Bind(wxEVT_RIGHT_UP,

				[this](wxMouseEvent &evt)
				{
					evt.Skip();

					if(!mUniEditEnabled)
						return;

					if(mMouseRightAction)
					{
						mMouseRightAction = false;
						mGLDispl->ReleaseMouse();
					}
				},

				mGLDispl->GetId()

			);

			mGLDispl->Bind(wxEVT_MOUSEWHEEL,

				[this](wxMouseEvent &evt)
				{
					evt.Skip();

					if(!mUniEditEnabled)
						return;

					auto curLevel = mGLDispl->getZoomLevel();
					curLevel = curLevel < 0 ? -1 / curLevel : curLevel + 1;

					const auto amount = (float)evt.GetWheelRotation() / (float)evt.GetWheelDelta();
					const auto mp = evt.GetPosition();

					auto newLevel = (amount > 0 ? 1.1 : 0.9) * curLevel;
					newLevel = std::floor(newLevel < 1 ? -1 / newLevel + 1 : newLevel - 1);

					if((int)newLevel == (int)std::floor(mGLDispl->getZoomLevel()))
						newLevel += amount > 0 ? 1 : -1;

					adjustZoom(newLevel, mp.x, mp.y);
					adjustBrush(evt);

					snapshotAndResetTimer();
					mGLDispl->Refresh(false);

					updateOptionsPanel();

				},

				mGLDispl->GetId()
			);

			mMainWnd->Bind(wxEVT_CLOSE_WINDOW,

				[this](wxCloseEvent &evt)
				{
					evt.Skip();

					if(evt.CanVeto())
						evt.Veto();

					doExit();

					mDrawTimer->Stop();

					mMainWnd->Destroy();

				},

				mMainWnd->GetId()
			);

			mSimulationThread = std::thread(
				[this]
				{
					auto curTime = atomicClock();

					while(mSimStatus != SimulationStatus::Stop)
					{
						if(mSimStatus == SimulationStatus::Paused)
						{
							std::unique_lock waitLock{mSimCVmut};

							mSimCV.wait(waitLock, [&]{ return mSimStatus != SimulationStatus::Paused; });

							if(mSimStatus == SimulationStatus::Stop)
								break;

							curTime = atomicClock();
						}

						(**mSimulator.access(sa::mode::excl)).advance();

						++mSimFrameNo;

						const auto sps = mSimSpeed.load();
						if(sps.count() > 0) // Sleep on the job?
						{
							curTime += sps;
							std::this_thread::sleep_until(curTime);
						}
						else
							curTime = atomicClock();
					}
				}
			);

			updateOptionsPanel();

			mMainWnd->Bind(wxEVT_SIZE,

				[this](wxSizeEvent &evt)
				{
					snapshotAndResetTimer();
					mGLDispl->Refresh(false);
					evt.Skip();
				},

				mMainWnd->GetId()
			);

			mMainWnd->Bind(wxEVT_MAXIMIZE,

				[this](wxMaximizeEvent &evt)
				{
					CallAfter([&]{
						snapshotAndResetTimer();
						mGLDispl->Refresh(false);
					});

					evt.Skip();
				},

				mMainWnd->GetId()
			);

			mMainWnd->Show();

			startSnapshots();
			mGLDispl->Refresh(false);

			return true;
		}

		virtual bool OnExceptionInMainLoop() override
		{
			try { std::rethrow_exception(std::current_exception()); }
			catch(const std::exception &e) { wxMessageBox(std::string("Unhandled exception: ") + e.what(), "Error", wxOK | wxCENTER | wxICON_ERROR); }
			catch(...) { wxMessageBox(std::string("Unhandled exception."), "Error", wxOK | wxCENTER | wxICON_ERROR); }

			return false;
		}

	};

	wxDECLARE_APP(WxApplication);
	wxIMPLEMENT_APP_NO_MAIN(WxApplication);

	int appMain(int argc, char **argv)
	try
	{
		wxDISABLE_DEBUG_SUPPORT();

		std::pmr::set_default_resource(&pl::default_resource());

		return wxEntry(argc, argv);
	}
	catch(...)
	{
		return EXIT_FAILURE;
	}

}

int main(int argc, char **argv)
{
	return ConwayGoL_GUI::appMain(argc, argv);
}
