Conway's game of life simulator
==============================

This is a simple Conway's game of life simulator written in c++ originally made for a contest in [Together C & C++](https://discord.gg/NDcU6B) discord channel. Only a brute force algorithm is supported but it is highly optimized. It support very large universes through my tiled universe simulator in which memory is allocated for active regions in tiles.

File format supported is [RLE](http://www.conwaylife.com/wiki/Run_Length_Encoded) and a simple text format.

### Compiling

##### Dependencies

*  A c++20 compiler. I have only tested with GCC and clang.
*  My [Shared Access](https://gitlab.com/patlefort/sharedaccess) library. Headers only.
*  My [PatLib](https://gitlab.com/patlefort/sharedaccess) library.
*  libnuma if OpenMP is enabled and PatLib is compiled with numa support.
*  OpenMP for multithreading support
*  Boost
*  wxWidget
*  OpenGL

##### Notes

*  NUMA support is currently broken for GCC, something is wrong when I used nested parallel regions on libgomp.
*  You should reduce the refresh rate when zooming out to improve performance. Displaying the grid with high refresh rate takes a lot of CPU.
*  Be sure to select "Classic - Tiled" simulation type if you want to open a very large universe.
*  Huge pages will be used if THP is enabled on Linux and if PatLib is configured to compile with libhugetlbfs.

#### Linux - GCC

```sh
cmake <source dir> -DCMAKE_CXX_FLAGS="-march=native"
cmake --build .
sudo cmake --install .
```

#### Linux - Clang

```sh
cmake <source dir> -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_CXX_FLAGS="-march=native"
cmake --build .
sudo cmake --install .
```

#### Windows - Mingw

Cross compiling from my Arch Linux(btw). NUMA not supported on Windows yet. You will need to install "mingw-w64-wxmsw", "mingw-w64-gcc", "mingw-w64-cmake" and "mingw-w64-boost" packages. Make sure to include all the dlls with the executable when you run it in Windows.

```sh
x86_64-w64-mingw32-cmake <source dir> -DCMAKE_CXX_FLAGS="-march=native" -DCMAKE_INSTALL_PREFIX=<install location> -DCMAKE_CXX_FLAGS_RELEASE="-O3 -DNDEBUG"
cmake --build .
cmake --install .
```

### Running

```sh
conwaygol-gui
```

##### Mouse bindings:
*  Left click to insert structure or a single cell. It will insert a single cell if no structure is loaded.
*  Shift + left click to erase cells.
*  Ctrl + left click and drag to select an area.
*  Right click and drag to move the viewport.
*  Mouse wheel to zoom in and out. It will zoom toward the mouse cursor.

##### Key bindings:
*  F1 to start and pause the simulation.
*  F2 to pause and advance the simulation by one step.
*  Crtl + s to save to a file.

##### Buttons:
*  "New universe": Create new universe with given type and size.
*  "Resize current universe" to resize the universe. The current universe state will be transferred.
*  "Clear" to kill all cells.
*  "Random fill" will fill the universe randomly.
*  "Fast forward" will forward the simulation by the given number of steps and display the time it took.
*  "Save universe" will save the current state of the universe in plain text format.
*  "Load universe" support my plain text format as well as RLE format.
*  "Insert structure from file" will load a structure from a file and set it as the structure for insertion.
*  "Unload structure" will unload the loaded structure.

##### Options:
*  "Type": Algorithm used for the simulation.
*  "Wrap around": Enable or disable wrap around. Not supported yet for tiled simulator.
*  "Speed": The delay between steps in milliseconds. 0 means no delay. The display will cap at 60 FPS but the simulation can run faster.
*  "Zoom type": How to display the universe when zooming out. "Approximate random" is the fastest but create noise. "Accurate averaging" is the slowest and "Approximate" is in between.
*  "Display refresh rate": The maximum number of frames per second the display should draw. This is independent from the speed of the simulation. Setting it too high might slow down the speed of the simulation because it has to compete with it for access to the universe's data.
