/*
	Conway's Game of Life simulator

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "utils.hpp"

#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/array.hpp>
#include <boost/iostreams/device/back_inserter.hpp>

namespace conwaygol
{
	template <typename T_>
	struct StructureWriter
	{
		T_ &struc;

		[[nodiscard]] auto operator() (Point2D dims)
		{
			struc.setDimensions(dims);

			return WriterImpl{struc};
		}

		struct WriterImpl
		{
			T_ &struc;
			pl::av::bounds<2> bnds;
			pl::av::bounds<2>::iterator bndIt{bnds};

			WriterImpl(T_ &struc_) :
				struc{struc_}, bnds{(typename decltype(bnds)::value_type)struc_.getDimensions()[1], (typename decltype(bnds)::value_type)struc_.getDimensions()[0]} {}

			void operator() (std::uint8_t state, std::size_t nb)
			{
				if(!state)
				{
					bndIt += nb;
					return;
				}

				for(std::size_t i=0; i<nb; ++i)
				{
					struc.set({(Point2D::value_type)(*bndIt)[1], (Point2D::value_type)(*bndIt)[0]}, state);
					++bndIt;
				}
			}

			void operator() (std::integral_constant<std::uint8_t, 0>, std::size_t nb)
			{
				bndIt += nb;
				return;
			}
		};
	};

	template <typename T_>
	StructureWriter(T_ &) -> StructureWriter<T_>;

	template <typename T_>
	struct RLEWriter
	{
		T_ &struc;

		[[nodiscard]] auto operator() (Point2D dims)
		{
			struc.setDimensions(dims);

			return WriterImpl{struc};
		}

		struct WriterImpl
		{
			T_ &struc;

			WriterImpl(T_ &struc_) :
				struc{struc_} {}

			void operator() (std::uint8_t state, std::size_t nb)
			{
				while(true)
				{
					struc.mData.push_back({(RleTag::Type)nb, state});
					if(nb <= RleTag::MaxNb)
						break;
					nb -= RleTag::MaxNb;
				}
			}
		};
	};

	template <typename T_>
	RLEWriter(T_ &) -> RLEWriter<T_>;

	// Read Run Length Encoded format. http://www.conwaylife.com/wiki/Run_Length_Encoded
	template <typename FC_>
	void readStructRLE(std::istream &is, FC_&& initFc)
	{
		std::locale loc;
		is.imbue(loc);

		while(is && is.peek() == '#')
		{
			is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
			impl::skipEOL(is);
		}

		Point2D size{};
		is.ignore(4);
		is >> size[0];
		is.ignore(6);
		is >> size[1];
		is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		impl::skipEOL(is);

		auto insertFc = PL_FWD(initFc)(size);

		std::size_t runCount = 1, lineNo = 0, xpos = 0;
		std::string runCountStr;

		const auto resetRunCount =
			[&]
			{
				runCount = 1;
				runCountStr.clear();
			};

		const auto insertCells =
			[&](bool v)
			{
				if(lineNo >= size[1] || xpos >= size[0])
					throw std::out_of_range("Data in the file is exceeding the specified dimensions.");
				insertFc(v, runCount);
			};

		do
		{
			const auto ch = is.get();

			if(std::isspace(ch))
				continue;

			if(ch == '!')
				break;

			if(ch == '$')
			{
				insertFc(std::integral_constant<std::uint8_t, 0>{}, size[0] - xpos + (runCount > 1 ? (runCount - 1) * size[0] : 0));
				lineNo += runCount;
				xpos = 0;
				resetRunCount();
				continue;
			}

			if(ch == 'b')
			{
				insertCells(false);
				xpos += runCount;

				resetRunCount();
				continue;
			}

			if(ch == 'o')
			{
				insertCells(true);
				xpos += runCount;

				resetRunCount();
				continue;
			}

			runCountStr.push_back(ch);
			do
			{
				const auto c = is.peek();

				if(!std::isdigit(c))
					break;
				if(std::isspace(c))
					continue;

				runCountStr.push_back(is.get());
			}
			while(is);

			runCount = std::stoi(runCountStr);
			runCountStr.clear();
		}
		while(is);

	}

	// Write Run Length Encoded format. http://www.conwaylife.com/wiki/Run_Length_Encoded
	template <typename StructType_>
	void writeStructRLE(const StructType_ &data, std::ostream &os)
	{
		const auto size = data.getDimensions();

		std::locale loc;
		os.imbue(loc);

		os << std::dec << "x = " << size[0] << ", y = " << size[1] << '\n';

		std::size_t nbEmptyLines = 0;

		// Create unbuffered streams that write directly to a std::string.
		using dev_type = boost::iostreams::back_insert_device<std::string>;
		using buf_type = boost::iostreams::stream_buffer<dev_type>;
		std::string osLine, tagStr;
		buf_type bufLine{dev_type{osLine}, 0, 0}, bufTag{dev_type{tagStr}, 0, 0};
		std::ostream osliness{&bufLine}, ostag{&bufTag};

		osLine.reserve(70);

		const auto flushLine =
			[&]
			{
				os << osLine << '\n';
				osLine.clear();
			};

		const auto writeTagImpl =
			[&](auto nb, char t)
			{
				tagStr.clear();
				if(nb > 1)
					ostag << std::dec << nb;
				ostag << t;

				if(osLine.size() + tagStr.size() > 70)
					flushLine();

				osliness << tagStr;
			};

		const auto writeTag =
			[&](auto nb, char t)
			{
				if(nbEmptyLines)
				{
					writeTagImpl(nbEmptyLines, '$');
					nbEmptyLines = 0;
				}

				writeTagImpl(nb, t);
			};

		for(Point2D::value_type y=0; y<size[1]; ++y)
		{
			bool curValue = false, lineEmpty = true;
			std::size_t nb = 0;

			for(Point2D::value_type x=0; x<size[0]; ++x)
			{
				const bool v = data[{x, y}];

				if(v) lineEmpty = false;

				if(v == curValue)
					++nb;
				else
				{
					if(x != 0)
						writeTag(nb, curValue ? 'o' : 'b');

					curValue = v;
					nb = 1;
				}
			}

			if(lineEmpty)
				++nbEmptyLines;
			else
			{
				if(curValue)
					writeTag(nb, 'o');

				writeTag(1, '$');
			}
		}

		writeTag(1, '!');
		os << osLine;
	}
}
