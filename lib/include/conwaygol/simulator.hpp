/*
	Conway's Game of Life simulator

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <vector>
#include <cstring>
#include <functional>
#include <span>

#ifdef _OPENMP
	#include <omp.h>
#endif

#include "utils.hpp"
#include "intrins.hpp"

namespace conwaygol
{
	template <typename PackType_>
	struct PackedUniverseProperties
	{
		using PackType = PackType_;

		static constexpr std::size_t nbBitsPack = sizeof(PackType_) * 8;
		inline static PackType_ firstCellMask{ static_cast<PackType_>(~impl::initScalar(impl::type_v<PackType_>, 1)) };

		Point2D dims;
		std::size_t widthAlignedPack, nbPackedCells;
		PackType_ lastCellMask;
	};

	template <typename PackType_>
	[[nodiscard]] constexpr auto calcUniverseNbPacks(const PackedUniverseProperties<PackType_> &props) noexcept
	{
		return props.widthAlignedPack * (props.dims[1] + 2);
	}

	template <typename PackType_, bool CalcMask_ = false>
	[[nodiscard]] constexpr auto calcUniverseProperties(Point2D s) noexcept
	{
		PackedUniverseProperties<PackType_> props{};

		props.dims = s;

		if(s[0] > 0 && s[1] > 0) [[likely]]
		{
			// We make the size of the buffer larger so we don't have to do bound checks and
			// every rows are aligned to the alignment of PackType.
			props.widthAlignedPack = (s[0] + 2) / props.nbBitsPack + ((s[0] + 2) % props.nbBitsPack ? 1 : 0);
			props.nbPackedCells = s[1] * props.widthAlignedPack;

			if constexpr(CalcMask_)
				for(std::size_t i=0, m = (s[0] + 1) % props.nbBitsPack; i<m; ++i)
					props.lastCellMask |= impl::shiftScalarLeftByValue(impl::initScalar(impl::type_v<PackType_>, 1), i);
		}

		return props;
	}

	namespace impl
	{
		void setCellState(auto&& data, auto p, bool v)
		{
			auto &b = data[p.first];
			b = v ? b | p.second : b & ~p.second;
		}

		template <typename Derived_, typename PackType_>
		class PackedUniverseViewFacade
		{
		private:
			using PackType = PackType_;

			[[nodiscard]] constexpr const auto &derived() const& { return static_cast<const Derived_&>(*this); }
			[[nodiscard]] constexpr auto &derived() & { return static_cast<Derived_&>(*this); }

		public:
			static constexpr std::size_t nbBitsPack = PackedUniverseProperties<PackType>::nbBitsPack;

			[[nodiscard]] bool empty() const noexcept { return derived().mData.empty(); }

			[[nodiscard]] auto getDimensions() const noexcept { return derived().mProps.dims; }
			[[nodiscard]] auto getWidthAlignedPack() const noexcept { return derived().mProps.widthAlignedPack; }
			[[nodiscard]] auto getNbPackedCells() const noexcept { return derived().mProps.nbPackedCells; }
			[[nodiscard]] auto getLastCellMask() const noexcept { return derived().mProps.lastCellMask; }
			[[nodiscard]] const auto &getProperties() const noexcept { return derived().mProps; }

			[[nodiscard]] bool hasAliveCellsInBuffer() const noexcept
			{
				return std::any_of(derived().mData.begin(), derived().mData.end(), [](const auto &v){ return impl::testNotZero(v); });
			}

			// Get the state of a single cell.
			[[nodiscard]] bool operator[] (Point2D p) const noexcept
			{
				plassert(p[0] < derived().mProps.dims[0] && p[1] < derived().mProps.dims[1]);

				const auto cell = getCellIndex(p);
				return impl::testNotZero(derived().mData[cell.first] & cell.second);
			}

			// Return the index and the flag to use to get the state of a cell.
			// $p is the real position.
			[[nodiscard]] std::pair<std::size_t, PackType> getCellIndex(Point2D p) const noexcept
			{
				++p[0];

				return {
					(std::size_t)p[0] / nbBitsPack + ((std::size_t)p[1]+1) * derived().mProps.widthAlignedPack,
					impl::shiftScalarLeftByValue(impl::initScalar(impl::type_v<PackType>, 1), p[0] % nbBitsPack)
				};
			}

			// Like method above, but expect a position inside the cells buffer, which is larger than the real size.
			// $p is the buffer position.
			[[nodiscard]] std::pair<std::size_t, PackType> getCellBufferIndex(Point2D p) const noexcept
			{
				return {
					(std::size_t)p[0] / nbBitsPack + (std::size_t)p[1] * derived().mProps.widthAlignedPack,
					impl::shiftScalarLeftByValue(impl::initScalar(impl::type_v<PackType>, 1), p[0] % nbBitsPack)
				};
			}

			// Return true if the specified border has any alive cell.
			template <Border border_>
			[[nodiscard]] bool hasAliveCellsAtBorder(BorderConstant<border_> = {}) const noexcept
			{
				[[maybe_unused]] const auto checkLine = [&](const std::size_t firstIndex)
					{
						const auto lastIndex = firstIndex + derived().mProps.widthAlignedPack - 1;
						const auto first = derived().mData[firstIndex] & derived().mProps.firstCellMask;

						if(lastIndex == firstIndex)
							return impl::testNotZero(first & derived().mProps.lastCellMask);

						const auto last = derived().mData[lastIndex] & derived().mProps.lastCellMask;

						return impl::testNotZero(first) || impl::testNotZero(last) ||
							std::any_of(
								derived().mData.begin() + firstIndex + 1,
								derived().mData.begin() + lastIndex,
								[](const auto &v){ return impl::testNotZero(v); }
							);
					};

				if constexpr(border_ == Border::Down)
				{
					return checkLine(derived().mProps.dims[1] * derived().mProps.widthAlignedPack);
				}
				else if constexpr(border_ == Border::Up)
				{
					return checkLine(derived().mProps.widthAlignedPack);
				}
				else if constexpr(border_ == Border::Left)
				{
					for(Point2D::value_type y=0; y<derived().mProps.dims[1]; ++y)
						if(derived()[{0, y}])
							return true;
				}
				else if constexpr(border_ == Border::Right)
				{
					for(Point2D::value_type y=0; y<derived().mProps.dims[1]; ++y)
						if(derived()[{derived().mProps.dims[0] - 1, y}])
							return true;
				}
				else if constexpr(border_ == Border::UpLeft)
				{
					return derived()[{0, 0}];
				}
				else if constexpr(border_ == Border::UpRight)
				{
					return derived()[{derived().mProps.dims[0] - 1, 0}];
				}
				else if constexpr(border_ == Border::DownLeft)
				{
					return derived()[{0, derived().mProps.dims[1] - 1}];
				}
				else if constexpr(border_ == Border::DownRight)
				{
					return derived()[{derived().mProps.dims[0] - 1, derived().mProps.dims[1] - 1}];
				}

				return false;
			}
		};
	} // namespace impl

	// View on a packed universe that allow manipulating(if PackType_ isn't const) or reading its data.
	template <typename PackType_>
		requires std::is_trivial_v<std::remove_const_t<PackType_>>
	class PackedUniverseView : public impl::PackedUniverseViewFacade<PackedUniverseView<PackType_>, std::remove_const_t<PackType_>>
	{
	public:

		template <typename PT_>
			requires std::is_trivial_v<std::remove_const_t<PT_>>
		friend class PackedUniverseView;

		template <typename, typename>
		friend class impl::PackedUniverseViewFacade;

		static constexpr bool IsConstView = std::is_const_v<PackType_>;

		using PackType = std::remove_const_t<PackType_>;
		using value_type = bool;

		using ConstUniverseViewType = PackedUniverseView<std::add_const_t<PackType_>>;

		static constexpr std::size_t nbBitsPack = impl::PackedUniverseViewFacade<PackedUniverseView<PackType_>, PackType>::nbBitsPack;

	private:

		PackedUniverseProperties<PackType> mProps{};
		std::span<PackType_> mData;

	public:

		PackedUniverseView() = default;

		template <typename OPT_>
		PackedUniverseView(const PackedUniverseView<OPT_> &o) noexcept
			requires ((IsConstView || !PackedUniverseView<OPT_>::IsConstView) && std::same_as<std::remove_const_t<PackType_>, std::remove_const_t<OPT_>>)
			: mProps{o.mProps}, mData{o.mData} {}
		PackedUniverseView(std::span<PackType_> data, Point2D dims) noexcept :
			mProps{calcUniverseProperties<PackType, true>(dims)}, mData(data) {}
		PackedUniverseView(std::span<PackType_> data, const PackedUniverseProperties<PackType> &props) noexcept :
			mProps{props}, mData(data) {}

		using impl::PackedUniverseViewFacade<PackedUniverseView<PackType_>, PackType>::getCellBufferIndex;
		using impl::PackedUniverseViewFacade<PackedUniverseView<PackType_>, PackType>::getCellIndex;
		using impl::PackedUniverseViewFacade<PackedUniverseView<PackType_>, PackType>::empty;

		[[nodiscard]] auto &data() const noexcept { return mData; }
		[[nodiscard]] operator bool() const noexcept { return !mData.empty(); }

		// Heat death of the universe
		void zero() const
			requires (!IsConstView)
		{
			//mData.assign(mData.size(), 0);
			std::fill(mData.begin(), mData.end(), 0);
			//std::memset(std::as_writable_bytes(mData), 0, mData.size_bytes());
		}

		// Get a reference to the state of a pack of cells.
		[[nodiscard]] auto &getPackState(std::size_t index) const noexcept
		{
			plassert(index < mData.size());
			return mData[index];
		}

		// Set the state of a single cell. Thread-safe only if different threads don't modify the same pack.
		void set(Point2D p, bool v) const noexcept
			requires (!IsConstView)
		{
			plassert(p[0] < mProps.dims[0] && p[1] < mProps.dims[1]);

			setState( getCellIndex(p), v );
		}

		// Copy borders to the opposite border in the extra allocated space. The universe must not be empty.
		void wrapBorders() const noexcept
			requires (!IsConstView)
		{
			plassert(!empty());

			std::copy_n(
				mData.begin() + mProps.widthAlignedPack,
				mProps.widthAlignedPack,
				mData.begin() + (mProps.dims[1] + 1) * mProps.widthAlignedPack
			);
			std::copy_n(
				mData.begin() + (mProps.dims[1] * mProps.widthAlignedPack),
				mProps.widthAlignedPack,
				mData.begin()
			);

			setState( getCellBufferIndex({0, 0}), (*this)[{mProps.dims[0] - 1, mProps.dims[1] - 1}] );
			setState( getCellBufferIndex({mProps.dims[0] + 1, 0}), (*this)[{0, mProps.dims[1] - 1}] );
			setState( getCellBufferIndex({mProps.dims[0] + 1, mProps.dims[1] + 1}), (*this)[{0, 0}] );
			setState( getCellBufferIndex({0, mProps.dims[1] + 1}), (*this)[{mProps.dims[0] - 1, 0}] );

			for(Point2D::value_type y=0; y<mProps.dims[1]; ++y)
			{
				setState( getCellBufferIndex({0, y + 1}), (*this)[{mProps.dims[0] - 1, y}] );
				setState( getCellBufferIndex({mProps.dims[0] + 1, y + 1}), (*this)[{0, y}] );
			}
		}

		// Copy borders from an adjacent universe to the opposite border of this universe in the extra allocated space.
		// The universes must not be empty and must be of equal dimensions.
		// $border_ is the border of the target universe.
		template <Border border_>
		void copyBorder(const ConstUniverseViewType uni, BorderConstant<border_> = {}) const noexcept
			requires (!IsConstView)
		{
			plassert(!empty());
			plassert(!uni.empty());

			[[maybe_unused]] const auto &srcProps = uni.getProperties();
			[[maybe_unused]] const auto copyLine = [&](std::size_t sindex, Point2D::value_type tindex)
				{
					const auto indexRight = getCellBufferIndex({mProps.dims[0] + 1, tindex});
					const auto indexLeft = getCellBufferIndex({0, tindex});
					auto &bkStateRight = mData[indexRight.first];
					const auto stateBackupRight = bkStateRight & indexRight.second;
					auto &bkStateLeft = mData[indexLeft.first];
					const auto stateBackupLeft = bkStateLeft & indexLeft.second;

					std::copy_n(
						uni.data().begin() + (sindex * srcProps.widthAlignedPack),
						srcProps.widthAlignedPack,
						mData.begin() + indexLeft.first
					);

					bkStateRight = (bkStateRight & ~indexRight.second) | stateBackupRight;
					bkStateLeft = (bkStateLeft & ~indexLeft.second) | stateBackupLeft;
				};

			if constexpr(border_ == Border::Down)
			{
				plassert(mProps.dims[0] == srcProps.dims[0]);

				copyLine(1, mProps.dims[1] + 1);
			}
			else if constexpr(border_ == Border::Up)
			{
				plassert(mProps.dims[0] == srcProps.dims[0]);

				copyLine(srcProps.dims[1], 0);
			}
			else if constexpr(border_ == Border::Left)
			{
				plassert(mProps.dims[1] == srcProps.dims[1]);
				for(Point2D::value_type y=0; y<mProps.dims[1]; ++y)
					setState( getCellBufferIndex({0, y + 1}), uni[{srcProps.dims[0] - 1, y}] );
			}
			else if constexpr(border_ == Border::Right)
			{
				plassert(mProps.dims[1] == srcProps.dims[1]);
				for(Point2D::value_type y=0; y<mProps.dims[1]; ++y)
					setState( getCellBufferIndex({mProps.dims[0] + 1, y + 1}), uni[{0, y}] );
			}
			else if constexpr(border_ == Border::UpLeft)
			{
				setState( getCellBufferIndex({0, 0}), uni[{srcProps.dims[0] - 1, srcProps.dims[1] - 1}] );
			}
			else if constexpr(border_ == Border::UpRight)
			{
				setState( getCellBufferIndex({mProps.dims[0] + 1, 0}), uni[{0, srcProps.dims[1] - 1}] );
			}
			else if constexpr(border_ == Border::DownLeft)
			{
				setState( getCellBufferIndex({0, mProps.dims[1] + 1}), uni[{srcProps.dims[0] - 1, 0}] );
			}
			else if constexpr(border_ == Border::DownRight)
			{
				setState( getCellBufferIndex({mProps.dims[0] + 1, mProps.dims[1] + 1}), uni[{0, 0}] );
			}
		}

		// Zero out borders in the extra allocated space. The universe must not be empty.
		void zeroBorders() const noexcept
			requires (!IsConstView)
		{
			zeroBorder(BorderConstant_v<Border::Up>);
			zeroBorder(BorderConstant_v<Border::UpLeft>);
			zeroBorder(BorderConstant_v<Border::UpRight>);
			zeroBorder(BorderConstant_v<Border::Left>);
			zeroBorder(BorderConstant_v<Border::Right>);
			zeroBorder(BorderConstant_v<Border::Down>);
			zeroBorder(BorderConstant_v<Border::DownLeft>);
			zeroBorder(BorderConstant_v<Border::DownRight>);
		}

		template <Border border_>
		void zeroBorder(BorderConstant<border_> = {}) const noexcept
			requires (!IsConstView)
		{
			plassert(!empty());

			[[maybe_unused]] const auto zeroLine = [&](Point2D::value_type tindex)
				{
					const auto indexRight = getCellBufferIndex({mProps.dims[0] + 1, tindex});
					const auto indexLeft = getCellBufferIndex({0, tindex});
					auto &bkStateRight = mData[indexRight.first];
					const auto stateBackupRight = bkStateRight & indexRight.second;
					auto &bkStateLeft = mData[indexLeft.first];
					const auto stateBackupLeft = bkStateLeft & indexLeft.second;

					std::fill_n(
						mData.begin() + indexLeft.first,
						mProps.widthAlignedPack,
						PackType{}
					);

					bkStateRight = (bkStateRight & ~indexRight.second) | stateBackupRight;
					bkStateLeft = (bkStateLeft & ~indexLeft.second) | stateBackupLeft;
				};

			if constexpr(border_ == Border::Down)
			{
				zeroLine(mProps.dims[1] + 1);
			}
			else if constexpr(border_ == Border::Up)
			{
				zeroLine(0);
			}
			else if constexpr(border_ == Border::Left)
			{
				for(Point2D::value_type y=0; y<mProps.dims[1]; ++y)
					setState( getCellBufferIndex({0, y + 1}), false );
			}
			else if constexpr(border_ == Border::Right)
			{
				for(Point2D::value_type y=0; y<mProps.dims[1]; ++y)
					setState( getCellBufferIndex({mProps.dims[0] + 1, y + 1}), false );
			}
			else if constexpr(border_ == Border::UpLeft)
			{
				setState( getCellBufferIndex({0, 0}), false );
			}
			else if constexpr(border_ == Border::UpRight)
			{
				setState( getCellBufferIndex({mProps.dims[0] + 1, 0}), false );
			}
			else if constexpr(border_ == Border::DownLeft)
			{
				setState( getCellBufferIndex({0, mProps.dims[1] + 1}), false );
			}
			else if constexpr(border_ == Border::DownRight)
			{
				setState( getCellBufferIndex({mProps.dims[0] + 1, mProps.dims[1] + 1}), false );
			}
		}

		// Set the state of a single cell.
		// $p is a pair returned by getCellBufferIndex.
		void setState(std::pair<std::size_t, PackType> p, bool v) const
			requires (!IsConstView)
		{
			plassert(p.first < mData.size());
			impl::setCellState(mData, p, v);
		}

		// Return the state of a cell.
		// $p is the buffer position.
		[[nodiscard]] bool getCellStateBuffer(Point2D p) const noexcept
		{
			const auto cell = getCellBufferIndex(p);
			plassert(cell.first < mData.size());
			return bool(mData[cell.first] & cell.second);
		}

		// Return the number of neighbors of a cell at position $p.
		[[nodiscard]] std::uint8_t getNbNeighbors(Point2D p) const noexcept
		{
			std::uint8_t nbNeighbors{};

			++p[0]; ++p[1];

			visitNeighbors(p, [&](const auto np){ nbNeighbors += (std::uint8_t)getCellStateBuffer(np); });

			return nbNeighbors;
		}

		// Return the state of a pack of cells that isn't aligned with the storage and require some bit shifting.
		// $index is the buffer index.
		// $shift is the the amount of shifting to get the actual position of the pack of cells.
		template <int shift, bool CheckZero_ = false, bool CheckEnd_ = false>
		[[nodiscard]] PackType getCellsUnaligned(
			std::size_t index,
			std::integral_constant<int, shift> = {},
			std::bool_constant<CheckZero_> = {},
			std::bool_constant<CheckEnd_> = {}) const noexcept
		{
			plassert(index < mData.size());

			if constexpr(CheckZero_ && shift < 0)
			{
				if(!index) [[unlikely]]
					return mData[index] << std::integral_constant<int, -shift>{};
			}

			const auto startIndex = shift < 0 ? index - 1 : index;
			constexpr int startBit = shift < 0 ? nbBitsPack + shift : shift;

			plassert(startIndex < mData.size());

			PackType res = mData[startIndex] >> std::integral_constant<int, startBit>{};

			if constexpr(CheckEnd_ && shift > 0)
			{
				if(index + 1 == mData.size()) [[unlikely]]
					return res;
			}

			if constexpr(startBit != 0)
			{
				plassert(startIndex + 1 < mData.size());
				res |= mData[startIndex + 1] << std::integral_constant<int, nbBitsPack - startBit>{};
			}

			return res;
		}

		void flip(PackedUniverseView &other) noexcept
			requires (!IsConstView)
		{
			using std::swap;
			swap(mData, other.mData);
		}

		void swap(PackedUniverseView &other) noexcept
			requires (!IsConstView)
		{
			using std::swap;
			swap(mProps, other.mProps);
			swap(mData, other.mData);
		}

		friend void swap(PackedUniverseView &u1, std::swappable_with<PackedUniverseView> auto&& u2) noexcept
		{
			u1.swap(PL_FWD(u2));
		}
	};

	// Packed universe data container suitable for the simulation.
	template <typename PackType_ = std::uint_fast8_t, typename Allocator_ = std::allocator<PackType_>>
		requires std::is_trivial_v<PackType_>
	class PackedUniverse : public impl::PackedUniverseViewFacade<PackedUniverse<PackType_, Allocator_>, PackType_>
	{
	public:

		template <typename, typename>
		friend class impl::PackedUniverseViewFacade;

		// Type that will store cell data.
		// std::uint32_t seem to be the sweet spot on my system when not using intrinsics.
		// std::uint8_t and impl::PackedInt8_128 is the best with intrinsics.
		using PackType = PackType_;
		using StorageType = std::vector<PackType, Allocator_>;

		using value_type = bool;

		static constexpr std::size_t nbBitsPack = impl::PackedUniverseViewFacade<PackedUniverse<PackType_, Allocator_>, PackType>::nbBitsPack;

		using impl::PackedUniverseViewFacade<PackedUniverse<PackType_, Allocator_>, PackType>::getCellBufferIndex;
		using impl::PackedUniverseViewFacade<PackedUniverse<PackType_, Allocator_>, PackType>::getCellIndex;
		using impl::PackedUniverseViewFacade<PackedUniverse<PackType_, Allocator_>, PackType>::empty;

		PackedUniverse() = default;
		PackedUniverse(Point2D dims, Allocator_ a = {}) :
			mData(std::move(a))
		{
			setDimensions(dims);
		}

		PackedUniverse(Allocator_ a) :
			mData(std::move(a)) {}

		#ifdef PL_LIB_NUMA
			void setCpuMask(pl::numa::policy) noexcept {}
		#endif

		[[nodiscard]] auto createConstView() const noexcept
			{ return PackedUniverseView<std::add_const_t<PackType>>{ std::span{std::as_const(mData)}, mProps }; }
		[[nodiscard]] auto createView() noexcept
			{ return PackedUniverseView<PackType>{ std::span{mData}, mProps }; }

		[[nodiscard]] operator PackedUniverseView<PackType>() noexcept { return createView(); }
		[[nodiscard]] operator PackedUniverseView<std::add_const_t<PackType>>() const noexcept { return createConstView(); }

		// Set universe dimension
		void setDimensions(Point2D s)
		{
			if(!mData.empty() && s[0] == mProps.dims[0] && s[1] == mProps.dims[1])
			{
				zero();
				return;
			}

			mData.clear();
			mData.shrink_to_fit();

			mProps = calcUniverseProperties<PackType, true>(s);
			mData.resize(calcUniverseNbPacks(mProps), 0);
		}

		// Set the state of a single cell. Thread-safe only if different threads don't modify the same pack.
		void set(Point2D p, bool v) noexcept
		{
			plassert(p[0] < mProps.dims[0] && p[1] < mProps.dims[1]);

			setState( getCellIndex(p), v );
		}

		// Set the state of a single cell.
		// $p is a pair returned by getCellBufferIndex.
		void setState(std::pair<std::size_t, PackType> p, bool v)
		{
			plassert(p.first < mData.size());
			impl::setCellState(mData, p, v);
		}

		// Heat death of the universe
		void zero()
		{
			mData.assign(mData.size(), 0);
		}

		void flip(PackedUniverse &other) noexcept
		{
			mData.swap(other.mData);
		}

		void swap(PackedUniverse &other) noexcept
		{
			using std::swap;
			swap(mProps, other.mProps);
			mData.swap(other.mData);
		}

		void swap(StorageType &other) noexcept
		{
			mData.swap(other);
		}

		template <typename OT_>
		friend void swap(PackedUniverse &u1, std::swappable_with<PackedUniverse> auto&& u2) noexcept
		{
			u1.swap(PL_FWD(u2));
		}

	private:
		PackedUniverseProperties<PackType> mProps{};
		StorageType mData;

		// Binary serialization
		friend auto &operator <=(std::ostream &os, const PackedUniverse &uni)
		{
			os << std::dec << uni.mProps.dims[0] << ' ' << uni.mProps.dims[1] << '\n';

			for(Point2D::value_type y=0; y<uni.mProps.dims[1]; ++y)
			{
				for(Point2D::value_type x=0; x<uni.mProps.dims[0]; ++x)
					os <= uni.mData[(y + 1) * uni.mProps.widthAlignedPack + x + 1];
			}

			return os;
		}

		friend auto &operator >=(std::istream &is, PackedUniverse &uni)
		{
			std::locale loc;
			is.imbue(loc);

			Point2D dims{};

			is >> dims[0] >> dims[1];
			is.ignore();

			uni.setDimensions(dims);

			for(Point2D::value_type y=0; y<dims[1]; ++y)
			{
				for(Point2D::value_type x=0; x<dims[0]; ++x)
					is >= uni.mData[(y + 1) * uni.mProps.widthAlignedPack + x + 1];
			}

			return is;
		}

		// Text serialization
		friend auto &operator <<(std::ostream &os, const PackedUniverse &uni)
		{
			std::locale loc;
			os.imbue(loc);

			os << std::dec << uni.mProps.dims[0] << ' ' << uni.mProps.dims[1] << '\n';

			for(Point2D::value_type y=0; y<uni.mProps.dims[1]; ++y)
			{
				for(Point2D::value_type x=0; x<uni.mProps.dims[0]; ++x)
					os << (uni[{x, y}] ? '*' : '.');

				os << '\n';
			}

			return os;
		}

		friend auto &operator >>(std::istream &is, PackedUniverse &uni)
		{
			Point2D dims{};

			is >> dims[0] >> dims[1];
			impl::skipEOL(is);

			uni.setDimensions(dims);
			const auto view = uni.createView();

			for(Point2D::value_type y=0; y<dims[1]; ++y)
			{
				for(Point2D::value_type x=0; x<dims[0]; ++x)
					if(impl::getNextNonSpaceChar(is) == '*')
						view.set({x, y}, true);

				impl::skipEOL(is);
			}

			return is;
		}
	};


	// Game rules
	namespace rules
	{
		struct Classic
		{
			// Enable early termination if no neighbors are detected.
			static constexpr bool NoNeighborsDeath = true;

			// Apply the rules of the game and return new cell state.
			template <typename ST_, typename T_>
			[[nodiscard]] ST_ operator() (ST_ curState, T_ nbNeighbors) const noexcept
			{
				return (curState && nbNeighbors == 2) || nbNeighbors == 3;
			}
		};

		struct Labyrinth1
		{
			static constexpr bool NoNeighborsDeath = true;

			template <typename ST_, typename T_>
			[[nodiscard]] ST_ operator() (ST_ curState, T_ nbNeighbors) const noexcept
			{
				return (curState && nbNeighbors > 1 && nbNeighbors < 5) || nbNeighbors == 3;
			}
		};

		struct Labyrinth2
		{
			static constexpr bool NoNeighborsDeath = true;

			template <typename ST_, typename T_>
			[[nodiscard]] ST_ operator() (ST_ curState, T_ nbNeighbors) const noexcept
			{
				return (curState && nbNeighbors > 1 && nbNeighbors < 5) || nbNeighbors == 2;
			}
		};
	}


	namespace impl
	{

		// Provides useful functions for a simulator.
		template <typename GameRule_, typename PackType_>
			requires (!std::is_const_v<PackType_> && std::is_trivial_v<PackType_>)
		class Simulator
		{
		public:
			using GameRule = GameRule_;
			using PackType = typename PackedUniverseView<PackType_>::PackType;

			PackedUniverseView<std::add_const_t<PackType_>> mUniverseFront;
			PackedUniverseView<PackType_> mUniverseBack;
			const GameRule &mGameRule;

			Simulator(PackedUniverseView<std::add_const_t<PackType_>> unif, PackedUniverseView<PackType_> unib, const GameRule &gr) :
				mUniverseFront{unif}, mUniverseBack{unib}, mGameRule{gr} {}

			void advance() noexcept
			{
				advance([](auto&& /*oldState*/, auto&& newState, bool /*hasNeighbors*/){ return newState; });
			}

			// Advance simulation by one step.
			// $fc will be passed to updateCells.
			template <typename FC_>
			void advance(FC_&& fc) const noexcept
			{
				const auto &props = mUniverseFront.getProperties();

				for(std::size_t y=0; y<props.dims[1]; ++y)
				{
					const auto yindex = (y+1) * props.widthAlignedPack;
					for(std::size_t i = yindex, m = i + props.widthAlignedPack; i<m; ++i)
						updateCells(i, PL_FWD(fc));

					mUniverseBack.getPackState(yindex) &= props.firstCellMask;
					mUniverseBack.getPackState(yindex + props.widthAlignedPack - 1) &= props.lastCellMask;
				}
			}


			static constexpr std::size_t nb4BitsPacks = sizeof(std::uint_fast8_t) * 8 / 4;

			// Neighbors count mask
			[[nodiscard]] static constexpr auto buildMask()
			{
				std::uint_fast8_t res{};

				for(std::size_t i=0; i<nb4BitsPacks; ++i)
					res |= std::uint_fast8_t{0b1} << (i * 4);

				return res;
			}

			// Neighbors count shift mask
			[[nodiscard]] static constexpr auto shiftMask()
			{
				std::uint_fast8_t res{};

				for(std::size_t i=0; i<nb4BitsPacks; ++i)
					res |= std::uint_fast8_t{0b111} << (i * 4);

				return res;
			}

			void updateCells(std::size_t index) const noexcept
			{
				updateCells(index, [](auto&& /*oldState*/, auto&& newState, bool /*hasNeighbors*/){ return newState; });
			}

			// Update a pack of cells at $index.
			// $index is the buffer index.
			// $fc will be called with the old cell state, the new state and does it has any neighbors and must return the new cell state.
			template <typename FC_>
			void updateCells(std::size_t index, FC_&& fc) const noexcept
			{
				std::array<PackType, 4> nbNeighbors{};
				[[maybe_unused]] bool hasNeighbors = false;

				// Count neighbors for each cells in the pack. Counts are packed into 4 bits.
				const auto countNeighbor = [&](auto n)
					{
						static constexpr auto mask = buildMask();
						static constexpr auto smask = shiftMask();

						auto st = nbNeighbors.begin();
						while(impl::testNotZero(n))
						{
							*st++ += n & PackType{mask};
							n = (n >> 1) & PackType{smask};

							if constexpr(GameRule::NoNeighborsDeath)
								hasNeighbors = true;
						}
					};

				const auto widthAlign = mUniverseFront.getWidthAlignedPack();

				// Up
				countNeighbor( mUniverseFront.getPackState(index - widthAlign) );

				// Down
				countNeighbor( mUniverseFront.getPackState(index + widthAlign) );

				// Left
				countNeighbor( mUniverseFront.getCellsUnaligned(index, std::integral_constant<int, -1>{}) );

				// Right
				countNeighbor( mUniverseFront.getCellsUnaligned(index, std::integral_constant<int, 1>{}) );

				// Up-Left
				countNeighbor( mUniverseFront.getCellsUnaligned(index - widthAlign, std::integral_constant<int, -1>{}, std::bool_constant<true>{}) );

				// Up-Right
				countNeighbor( mUniverseFront.getCellsUnaligned(index - widthAlign, std::integral_constant<int, 1>{}) );

				// Down-Left
				countNeighbor( mUniverseFront.getCellsUnaligned(index + widthAlign, std::integral_constant<int, -1>{}) );

				// Down-Right
				countNeighbor( mUniverseFront.getCellsUnaligned(index + widthAlign, std::integral_constant<int, 1>{}, std::bool_constant<false>{}, std::bool_constant<true>{}) );


				// Compute new state for pack of cells
				const auto cellState = mUniverseFront.getPackState(index);

				auto &b = mUniverseBack.getPackState(index);

				if constexpr(GameRule::NoNeighborsDeath)
					if(!hasNeighbors)
					{
						b = PL_FWD(fc)(cellState, PackType{}, hasNeighbors);
						return;
					}

				PackType newState{};

				for(int i=0; i<(int)nbNeighbors.size(); ++i)
				{
					if constexpr(std::is_integral_v<PackType>) // Compiler bug? I get error even for a codepath that fail this test.
					{
						for(int c=0; c<(int)nb4BitsPacks; ++c)
							newState += (PackType)mGameRule(
									bool(cellState & (PackType{1} << (i+c*4))),
									(nbNeighbors[i] >> (c*4)) & PackType{0b1111}
								) << (i+c*4);
					}
					else
					{
						for(int c=0; c<(int)nb4BitsPacks; ++c)
							newState += mGameRule(cellState, (nbNeighbors[i] >> (c*4)) & PackType{0b1111}) & (PackType{1} << (i+c*4));
					}
				}

				b = PL_FWD(fc)(cellState, newState, hasNeighbors);
			}

			// Update a single cell at position $p.
			void updateCell(Point2D p) const noexcept
			{
				const auto cell = mUniverseFront.getCellIndex(p);
				const bool cellState = bool(mUniverseFront.getPackState(cell.first) & cell.second);
				bool newState;

				const auto nbNeighbors = mUniverseFront.getNbNeighbors(p);
				newState = mGameRule(cellState, nbNeighbors);

				auto &b = mUniverseBack.getPackState(cell.first);
				b = newState ? b | cell.second : b & ~cell.second;
			}
		};
		template <typename GameRule_, typename PackType_>
		Simulator(PackedUniverseView<std::add_const_t<PackType_>>, PackedUniverseView<PackType_>, const GameRule_ &)
			-> Simulator<GameRule_, PackType_>;

	} // Namespace impl


	// Run the simulation on a universe.
	template <typename GameRule_ = rules::Classic, bool SharedAccessUniverse_ = true>
	class Simulator
	{
	public:

		using GameRule = GameRule_;

		#ifdef __SSE2__
			using PackType = impl::PackedInt8_128;
		#elif defined(__MMX__)
			using PackType = impl::PackedInt8_64;
		#else
			using PackType = std::uint_fast8_t;
		#endif

		#ifdef PL_LIB_HUGEPAGES
			using UniverseAllocator = std::pmr::polymorphic_allocator<PackType>;
		#else
			using UniverseAllocator = std::allocator<PackType>;
		#endif

		using UniverseType = PackedUniverse<PackType, UniverseAllocator>;

	private:

		using MutexType = std::conditional_t<SharedAccessUniverse_, sa::shared_upgrade<UniverseType>::mutex_type, boost::null_mutex>;
		UniverseType mUniverseBack;
		GameRule mGameRule;
		bool mWrapAround = false;

		void allocateBack(const UniverseType &uni)
		{
			const auto backDims = mUniverseBack.getDimensions();
			const auto frontDims = uni.getDimensions();

			if(frontDims[0] != backDims[0] || frontDims[1] != backDims[1])
				mUniverseBack.setDimensions(frontDims);
		}

	public:
		sa::shared<UniverseType, MutexType> mUniverseAccess{
			#ifdef PL_LIB_HUGEPAGES
				sa::in_place_list, UniverseAllocator{&pl::hugepages_resource()}
			#endif
		};

		Simulator() = default;
		Simulator(const GameRule &gr) :
			mGameRule{gr} {}

		void setRule(GameRule r) noexcept { mGameRule = std::move(r); }
		[[nodiscard]] const GameRule &getRule() const noexcept { return mGameRule; }

		#ifdef PL_LIB_NUMA
			void setCpuMask(pl::numa::policy) noexcept {}
		#endif

		// PackedUniverse can be set to wrap around.
		void setWrap(bool v) noexcept
		{
			mWrapAround = v;

			if(!mWrapAround)
				mUniverseAccess | sa::mode::excl | [](auto &uni){ if(!uni.empty()) uni.createView().zeroBorders(); };
		}
		[[nodiscard]] auto getWrap() const noexcept { return mWrapAround; }

		// Advance the simulation by $steps steps. If $LockWrite_ is true, access to universe will be locked in exclusive mode for the entire duration.
		template <bool LockWrite_ = false>
		void advance(std::size_t steps = 1, std::bool_constant<LockWrite_> = {}) noexcept
		{
			const auto fc =
				[&](const auto &uni, auto&& upgLock)
				{
					if(uni.empty()) [[unlikely]]
						return;

					// Make sure back buffer is allocated.
					allocateBack(uni);

					#ifdef _OPENMP
						// Too many threads kill performance. This seems like a good number after some testing.
						static constexpr std::size_t minSize = 512;
						const auto nbThreads = std::min<std::size_t>(uni.getNbPackedCells() / minSize + (uni.getNbPackedCells() % minSize ? 1 : 0), omp_get_max_threads());

						if(nbThreads > 1)
						{
							// Multi-threads version
							#pragma omp parallel num_threads(nbThreads)
							{
								for(std::size_t s=0; s<steps; ++s)
								{
									const auto uniFrontView = uni.createConstView();
									const auto uniBackView = mUniverseBack.createView();
									const auto &props = uniFrontView.getProperties();
									impl::Simulator sim{uniFrontView, uniBackView, getRule()};

									if(getWrap())
									{
										#pragma omp master
											upgLock([&](auto &uni){ uni.createView().wrapBorders(); });
										#pragma omp barrier
									}

									#pragma omp for schedule(static)
									for(std::size_t y=0; y<props.dims[1]; ++y)
									{
										const auto yindex = (y+1) * props.widthAlignedPack;

										for(std::size_t x=0; x<props.widthAlignedPack; ++x)
											sim.updateCells(yindex + x);

										uniBackView.getPackState(yindex) &= props.firstCellMask;
										uniBackView.getPackState(yindex + props.widthAlignedPack - 1) &= props.lastCellMask;
									}

									#pragma omp master
										upgLock([&](auto &uni){ uni.flip(mUniverseBack); });
									#pragma omp barrier
								}
							}
						}
						else
					#endif
						{
							// Single thread version
							for(std::size_t s=0; s<steps; ++s)
							{
								impl::Simulator sim{uni.createConstView(), mUniverseBack.createView(), getRule()};

								if(getWrap())
									upgLock([&](auto &uni){ uni.createView().wrapBorders(); });

								sim.advance();

								upgLock([&](auto &uni){ uni.flip(mUniverseBack); });
							}
						}
				};

			// Lock in shared-access mode if $LockWrite_ is false, otherwise, lock in exclusive mode.
			if constexpr(LockWrite_)
			{
				mUniverseAccess | sa::mode::excl |
					[&](auto &uni)
					{
						fc(uni, EmptyUpgradeLock{uni});
					};
			}
			else
			{
				mUniverseAccess | sa::mode::shared_upgrade |
					pl::applier(std::ref(fc));
			}
		}
	};

}
