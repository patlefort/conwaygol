/*
	Conway's Game of Life simulator

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#if (__cplusplus <= 201703L)
	#error "At least a C++20 compiler is required."
#endif

#include <mutex>
#include <atomic>
#include <shared_mutex>
#include <iostream>
#include <cassert>
#include <random>
#include <tuple>
#include <experimental/type_traits>
#include <concepts>
#include <memory_resource>

#include <boost/predef.h>

#include <shared_access/shared_upgrade.hpp>

#include <boost/thread/null_mutex.hpp>

#include <patlib/array_view.hpp>
#include <patlib/pcg32.hpp>
#include <patlib/memory.hpp>

#ifdef PL_NUMA
	#include <patlib/numa.hpp>
#endif

#ifdef PL_LIB_NUMA
	#define CGOL_IFNUMA(...) __VA_ARGS__
#else
	#define CGOL_IFNUMA(...)
#endif

#ifdef _OPENMP
	#define CGOL_IFOPENMP(...) __VA_ARGS__
#else
	#define CGOL_IFOPENMP(...)
#endif

namespace conwaygol
{
	namespace sa = shared_access;
	namespace pl = patlib;

	using Point2D = std::array<std::uint32_t, 2>;
	using Point2Ds = std::array<int, 2>;
	using Point2Dr = std::array<float, 2>;

	template <typename Object_>
	struct EmptyUpgradeLock
	{
		Object_ &mObject;

		struct nothing {};

		template <typename FC_>
		void operator() (FC_&& f) { upgrade(PL_FWD(f)); }

		auto operator() () noexcept { return upgrade(); }
		auto upgrade() noexcept { return std::make_pair(std::ref(mObject), nothing{}); }

		template <typename FC_>
		void upgrade(FC_&& f) noexcept { PL_FWD(f)(mObject); }

		template <typename FC_>
		bool tryUpgrade(FC_&& f) { PL_FWD(f)(mObject); return true; }
		template <typename FC_, typename Time_>
		bool tryUpgrade(FC_&& f, Time_) { PL_FWD(f)(mObject); return true; }
	};
	template <typename Object_>
	EmptyUpgradeLock(Object_ &) -> EmptyUpgradeLock<Object_>;


	enum class Border
	{
		Left = 0,
		Right,
		Up,
		Down,
		UpLeft,
		UpRight,
		DownLeft,
		DownRight
	};

	template <Border b_>
	using BorderConstant = std::integral_constant<Border, b_>;

	template <Border b_>
	constexpr auto BorderConstant_v = BorderConstant<b_>{};

	[[nodiscard]] constexpr Border oppositeBorder(Border b) noexcept
	{
		if(b == Border::Down)
		{
			return Border::Up;
		}
		else if(b == Border::Up)
		{
			return Border::Down;
		}
		else if(b == Border::Left)
		{
			return Border::Right;
		}
		else if(b == Border::Right)
		{
			return Border::Left;
		}
		else if(b == Border::UpLeft)
		{
			return Border::DownRight;
		}
		else if(b == Border::UpRight)
		{
			return Border::DownLeft;
		}
		else if(b == Border::DownLeft)
		{
			return Border::UpRight;
		}
		else// if(b == Border::DownRight)
		{
			return Border::UpLeft;
		}
	}

	enum class SnapshotType
	{
		AccurateAverage = 0,
		Approximate,
		ApproximateRandom
	};

	// Implementation details
	namespace impl
	{
		// Type container. Useful for doing overloaded calls.
		template <typename T_>
		struct type_c { using type = T_; };

		template <typename T_>
		constexpr type_c<T_> type_v{};

		// Skip End-of-Line characters.
		inline std::istream &skipEOL(std::istream &is)
		{
			if(is.peek() == '\n')
				is.ignore();

			if(is.peek() == '\r')
				is.ignore();

			return is;
		}

		// Skip space characters and return first non-space character found.
		inline int getNextNonSpaceChar(std::istream &is)
		{
			int ch;
			while( std::isspace(ch = is.get()) );

			return ch;
		}


		// Shift the scalar $value to left by $shift. Overloaded by PackedInts.
		[[nodiscard]] constexpr auto shiftScalarLeftByValue(auto v, int shift) noexcept
			{ return v << shift; }

		// Init a scalar value. Overloaded by PackedInts. Necessary because the ctor {v} doesn't produce a scalar for PackedInts.
		template <typename PT_>
		[[nodiscard]] constexpr PT_ initScalar(type_c<PT_>, auto v) noexcept
			{ return static_cast<PT_>(v); }

		// Flip all bits to one.
		template <typename PT_>
		[[nodiscard]] constexpr PT_ initAllOne(type_c<PT_>) noexcept
		{
			if constexpr(std::numeric_limits<PT_>::is_signed)
				return static_cast<PT_>(-1);
			else
				return std::numeric_limits<PT_>::max();
		}

		// Test if $v is not all zero.
		[[nodiscard]] constexpr bool testNotZero(auto v) noexcept
			{ return v; }

		namespace tag
		{
			PL_MAKE_TAG(zero)
			PL_MAKE_TAG(scalar)
		}

	} // namespace impl


	// Structure contain cells data for a structure. Simple storage with no special optimization.
	template <typename T_ = std::uint8_t, typename Allocator_ = pl::allocator_noinit<T_>>
	class Structure
	{
	public:

		using StorageType = std::vector<T_, Allocator_>;

		using value_type = typename StorageType::value_type;
		using iterator = typename StorageType::iterator;
		using const_iterator = typename StorageType::const_iterator;
		using reverse_iterator = typename StorageType::reverse_iterator;
		using const_reverse_iterator = typename StorageType::const_reverse_iterator;

		void setDimensions(Point2D s)
		{
			mDimensions = s;

			mData.clear();
			mData.resize(s[0] * s[1]);
		}

		[[nodiscard]] auto getDimensions() const noexcept { return mDimensions; }

		void zero()
			{ mData.assign(mData.size(), {}); }

		void flip()
			{ mData.flip(); }

		[[nodiscard]] const auto &operator[] (Point2D p) const
			{ return mData[(std::size_t)p[1] * mDimensions[0] + (std::size_t)p[0]]; }

		[[nodiscard]] auto &operator[] (Point2D p)
			{ return mData[(std::size_t)p[1] * mDimensions[0] + (std::size_t)p[0]]; }

		[[nodiscard]] const auto &operator[] (std::size_t index) const
			{ return mData[index]; }

		[[nodiscard]] auto &operator[] (std::size_t index)
			{ return mData[index]; }

		[[nodiscard]] const auto &at(std::size_t index) const
			{ return mData.at(index); }

		[[nodiscard]] auto &at(std::size_t index)
			{ return mData.at(index); }

		// Not thread-safe if using bool type.
		void set(Point2D p, value_type v) noexcept
		{
			(*this)[p] = v;
		}

		[[nodiscard]] auto begin() noexcept { return mData.begin(); }
		[[nodiscard]] auto begin() const noexcept { return mData.begin(); }
		[[nodiscard]] auto cbegin() const noexcept { return mData.cbegin(); }

		[[nodiscard]] auto rbegin() noexcept { return mData.rbegin(); }
		[[nodiscard]] auto rbegin() const noexcept { return mData.rbegin(); }
		[[nodiscard]] auto crbegin() const noexcept { return mData.crbegin(); }

		[[nodiscard]] auto end() noexcept { return mData.end(); }
		[[nodiscard]] auto end() const noexcept { return mData.end(); }
		[[nodiscard]] auto cend() const noexcept { return mData.cend(); }

		[[nodiscard]] auto rend() noexcept { return mData.rend(); }
		[[nodiscard]] auto rend() const noexcept { return mData.rend(); }
		[[nodiscard]] auto crend() const noexcept { return mData.crend(); }

		[[nodiscard]] bool empty() const noexcept { return mData.empty(); }
		[[nodiscard]] auto size() const noexcept { return mData.size(); }
		[[nodiscard]] auto max_size() const noexcept { return mData.max_size(); }

		void clear() noexcept
		{
			mData.clear();
			mDimensions = {};
		}
		void shrink_to_fit() { mData.shrink_to_fit(); }

		void swap(Structure &o) noexcept
		{
			using std::swap;
			mData.swap(o.mData);
			swap(mDimensions, o.mDimensions);
		}

		friend void swap(Structure &m1, Structure &m2) noexcept
			{ m1.swap(m2); }

		// Text serialization
		friend auto &operator <<(std::ostream &os, const Structure &struc)
		{
			os << std::dec << struc.mDimensions[0] << ' ' << struc.mDimensions[1] << '\n';

			for(Point2D::value_type y=0; y<struc.mDimensions[1]; ++y)
			{
				for(Point2D::value_type x=0; x<struc.mDimensions[0]; ++x)
					os << (struc[{x, y}] ? '*' : '.');

				os << '\n';
			}

			return os;
		}

		friend auto &operator >>(std::istream &is, Structure &struc)
		{
			Point2D dims{};

			is >> dims[0] >> dims[1];
			impl::skipEOL(is);

			struc.setDimensions(dims);

			for(Point2D::value_type y=0; y<dims[1]; ++y)
			{
				for(Point2D::value_type x=0; x<dims[0]; ++x)
					struc[{x, y}] = (impl::getNextNonSpaceChar(is) == '*');

				impl::skipEOL(is);
			}

			return is;
		}

	private:
		StorageType mData;
		conwaygol::Point2D mDimensions{};

	};

	struct RleTag
	{
		using Type = std::uint32_t;
		static constexpr std::size_t MaxNb = pl::fill_bits((std::size_t)sizeof(Type)*8-8);

		Type
			nb : sizeof(Type)*8-8,
			state : 8;
	};

	class RleStructure
	{
	public:

		std::vector<RleTag> mData;

		void setDimensions(Point2D s)
		{
			mDimensions = s;

			mData.clear();
		}

		[[nodiscard]] auto getDimensions() const noexcept { return mDimensions; }

		void zero()
		{
			mData.clear();
		}

		// Text serialization
		friend auto &operator <<(std::ostream &os, const RleStructure &struc)
		{
			os << std::dec << struc.mDimensions[0] << ' ' << struc.mDimensions[1] << '\n';

			std::size_t y = 0, x = 0, runCount = 0;
			auto it = struc.mData.cbegin(), endit = struc.mData.cend();

			while(y < struc.mDimensions[1])
			{
				if(it == endit)
				{
					os << '.';
				}
				else
				{
					os << (it != endit && it->state ? '*' : '.');

					if(++runCount >= it->nb)
					{
						++it;
						runCount = 0;
					}
				}

				if(++x >= struc.mDimensions[0])
				{
					x = 0;
					++y;
					os << '\n';
				}
			}

			return os;
		}

		friend auto &operator >>(std::istream &is, RleStructure &struc)
		{
			Point2D dims{};

			is >> dims[0] >> dims[1];
			impl::skipEOL(is);

			struc.setDimensions(dims);

			std::uint8_t curState = 0;
			RleTag::Type nb = 0;
			for(std::size_t y=0; y<struc.mDimensions[1]; ++y)
				for(std::size_t x=0; x<struc.mDimensions[0]; ++x)
				{
					const auto state = impl::getNextNonSpaceChar(is) == '*' ? 1 : 0;

					if(state == curState)
						++nb;
					else
					{
						if(nb)
							struc.mData.push_back({nb, curState});
						curState = state;
						nb = 1;
					}
				}

			if(nb && curState)
				struc.mData.push_back({nb, curState});

			return is;
		}

	private:
		Point2D mDimensions{};

	};


	// Generic function to take a snapshot of a universe or structure into another structure or universe.
	template <typename InsertFC_, typename SourceStruct_>
	void snapshotStruct(const SourceStruct_ &sStruct, Point2D pos, Point2D dim, InsertFC_&& insertFc, std::uint32_t zoomFactor = 1, SnapshotType type = SnapshotType::AccurateAverage)
	{
		//using VT = typename SourceStruct_::value_type;

		//const auto dim = dStruct.getDimensions();
		plassert(pos[0] < sStruct.getDimensions()[0] && pos[1] < sStruct.getDimensions()[1] && pos[0] + dim[0] * zoomFactor <= sStruct.getDimensions()[0] && pos[1] + dim[1] * zoomFactor <= sStruct.getDimensions()[1]);

		if(zoomFactor > 1)
		{
			const auto tileSize = zoomFactor * zoomFactor;

			switch(type)
			{
			case SnapshotType::AccurateAverage:

				{
					const float w = 1 / float(zoomFactor * zoomFactor);

					#ifdef _OPENMP
						const std::size_t nbThreads = std::max<std::size_t>(1, std::min<std::size_t>( (dim[0] * zoomFactor * dim[1] * zoomFactor) / (1024 * 32), omp_get_max_threads()));

						#pragma omp parallel for num_threads(nbThreads) schedule(static) collapse(2)
					#endif
					for(Point2D::value_type y=0; y<dim[1]; ++y)
						for(Point2D::value_type x=0; x<dim[0]; ++x)
						{
							const auto checkTile =
								[&]() -> float
								{
									float res{};

									for(Point2D::value_type ty=0; ty<zoomFactor; ++ty)
										for(Point2D::value_type tx=0; tx<zoomFactor; ++tx)
											res += (float)sStruct[{pos[0] + x * zoomFactor + tx, pos[1] + y * zoomFactor + ty}] * w;

									return res;
								};

							PL_FWD(insertFc) ({x, y}, checkTile());
						}
				}

				break;
			case SnapshotType::Approximate:

				{
					#ifdef _OPENMP
						const std::size_t nbThreads = std::max<std::size_t>(1, std::min<std::size_t>( (dim[0] * zoomFactor * dim[1] * zoomFactor) / (1024 * 256), omp_get_max_threads()));

						#pragma omp parallel for num_threads(nbThreads) schedule(static) collapse(2)
					#endif
					for(Point2D::value_type y=0; y<dim[1]; ++y)
						for(Point2D::value_type x=0; x<dim[0]; ++x)
						{
							const auto checkTile =
								[&]() -> float
								{
									for(Point2D::value_type ty=0; ty<zoomFactor; ++ty)
										for(Point2D::value_type tx=0; tx<zoomFactor; ++tx)
											if( sStruct[{pos[0] + x * zoomFactor + tx, pos[1] + y * zoomFactor + ty}] )
												return 1;

									return 0;
								};

							PL_FWD(insertFc) ({x, y}, checkTile());
						}
				}

				break;
			case SnapshotType::ApproximateRandom:

				{
					const std::size_t nbSamples = 1;//std::max<std::size_t>(4, zoomFactor - 1);

					#ifdef _OPENMP
						const std::size_t nbThreads = std::max<std::size_t>(1, std::min<std::size_t>( (dim[0] * dim[1] * nbSamples) / (1024 * 256), omp_get_max_threads() ));

						#pragma omp parallel num_threads(nbThreads)
					#endif
					{
						//boost::random::mt19937 rng{boost::random::random_device{}()}; // boost::random::mt19937 is faster than the std one.
						pl::pcg32_8 rng;
						alignas(32) std::array<std::uint32_t, 8> rngValues;
						std::size_t rngIndex = 0;

						{
							std::random_device rs;
							alignas(32) std::array<std::uint64_t, 8> initStates{rs(), rs(), rs(), rs(), rs(), rs(), rs(), rs()};
							alignas(32) std::array<std::uint64_t, 8> initSequences{rs(), rs(), rs(), rs(), rs(), rs(), rs(), rs()};

							rng.seed(initStates.data(), initSequences.data());
						}

						const auto fetchRng = [&]{
							if(!rngIndex)
								rng.next_uint(rngValues.data());

							const auto res = rngValues[rngIndex];
							rngIndex = (rngIndex + 1) % 8;

							return res;
						};

						#ifdef _OPENMP
							#pragma omp for schedule(static) collapse(2)
						#endif
						for(Point2D::value_type y=0; y<dim[1]; ++y)
							for(Point2D::value_type x=0; x<dim[0]; ++x)
							{
								const auto checkTile =
									[&]() -> float
									{
										// Randomly sample points. This is order of magnitude faster than checking all cells with the tradeoff of a noisy result.
										for(std::size_t i=0; i<nbSamples; ++i)
										{
											const auto rn = fetchRng() % tileSize;
											//const auto rn = rngIndex++ % tileSize;
											const auto tx = rn / zoomFactor;
											const auto ty = rn % zoomFactor;

											if( sStruct[{pos[0] + x * zoomFactor + tx, pos[1] + y * zoomFactor + ty}] )
												return 1;
										}

										return 0;
									};

								PL_FWD(insertFc) ({x, y}, checkTile());
							}
					}
				}

				break;
			};

		}
		else
		{
			#ifdef _OPENMP
				const std::size_t nbThreads = std::max<std::size_t>(1, std::min<std::size_t>( (dim[0] * dim[1]) / (1024 * 128), omp_get_max_threads()));

				if(nbThreads > 1)
				{
					#pragma omp parallel for num_threads(nbThreads) schedule(static) collapse(2)
					for(Point2D::value_type y=0; y<dim[1]; ++y)
						for(Point2D::value_type x=0; x<dim[0]; ++x)
							PL_FWD(insertFc) ({x, y}, sStruct[{pos[0] + x, pos[1] + y}]);
				}
				else
			#endif
				{
					for(Point2D::value_type y=0; y<dim[1]; ++y)
						for(Point2D::value_type x=0; x<dim[0]; ++x)
							PL_FWD(insertFc) ({x, y}, sStruct[{pos[0] + x, pos[1] + y}]);
				}
		}
	}

	// Insert a structure into another structure.
	template <typename SourceStruct_, typename DestStruct_>
	void insertStruct(const SourceStruct_ &sStruct, DestStruct_ &dStruct, Point2D pos = {})
	{
		const auto dDims = dStruct.getDimensions();
		auto dims = sStruct.getDimensions();

		dims = {
			std::min(dims[0], dDims[0] - pos[0]),
			std::min(dims[1], dDims[1] - pos[1])
		};

		//const auto nbThreads = std::min<std::size_t>(curDim[0] * curDim[1] / (1024 * 16), omp_get_max_threads());

		//#pragma omp parallel for num_threads(nbThreads) schedule(static)
		for(Point2D::value_type y=0; y<dims[1]; ++y)
			for(Point2D::value_type x=0; x<dims[0]; ++x)
				dStruct.set({pos[0] + x, pos[1] + y}, sStruct[{x, y}]);
	}

	template <typename SourceStruct_, typename DestIt_>
	void snapshotStructRLE(const SourceStruct_ &sStruct, DestIt_ dIt, Point2D pos, Point2D size)
	{
		plassert(pos[0] + size[0] <= sStruct.getDimensions()[0] && pos[1] + size[1] <= sStruct.getDimensions()[1]);

		std::uint8_t curState = 0;
		std::uint32_t nb = 0;
		for(Point2D::value_type y=pos[1], my=pos[1] + size[1]; y<my; ++y)
			for(Point2D::value_type x=pos[0], mx=pos[0] + size[0]; x<mx; ++x)
			{
				const auto state = sStruct[{x, y}];

				if(state == curState)
					++nb;
				else
				{
					if(nb)
						*dIt++ = {nb, curState};
					curState = state;
					nb = 1;
				}
			}

		if(nb && curState)
			*dIt = {nb, curState};
	}

	template <typename SourceIt_, typename DestStruct_>
	void insertStructRLE(SourceIt_ sIt, SourceIt_ eIt, DestStruct_ &dStruct, Point2D pos, Point2D dims)
	{
		const auto dDims = dStruct.getDimensions();

		pl::av::bounds<2> bnds{(pl::av::bounds<2>::value_type)dims[1], (pl::av::bounds<2>::value_type)dims[0]};

		auto bndIt = bnds.begin();
		for(; sIt!=eIt; ++sIt)
		{
			std::uint8_t state = sIt->state;
			if(!state)
				bndIt += sIt->nb;
			else
			{
				for(std::size_t i=0, m=sIt->nb; i<m; ++i, ++bndIt)
					if(const Point2D p{(Point2D::value_type)(*bndIt)[1] + pos[0], (Point2D::value_type)(*bndIt)[0] + pos[1]};
							p[0] < dDims[0] && p[1] < dDims[1])
						dStruct.set(p, state);
			}
		}
	}

	// Visit neighbors of a cell and execute $fc for each with the cell position as argument.
	void visitNeighbors(Point2D p, std::invocable<Point2D> auto && fc) noexcept
	{
		PL_FWD(fc)( Point2D{p[0] - 1, p[1]} );
		PL_FWD(fc)( Point2D{p[0] - 1, p[1] - 1} );
		PL_FWD(fc)( Point2D{p[0], p[1] - 1} );
		PL_FWD(fc)( Point2D{p[0] + 1, p[1] - 1} );

		PL_FWD(fc)( Point2D{p[0] + 1, p[1]} );
		PL_FWD(fc)( Point2D{p[0] + 1, p[1] + 1} );
		PL_FWD(fc)( Point2D{p[0], p[1] + 1} );
		PL_FWD(fc)( Point2D{p[0] - 1, p[1] + 1} );
	}

	void visitNeighborsBounded(const Point2D p, const Point2D size, bool wrapAround, auto&& checkFc)
	{
		if(wrapAround)
		{
			const auto ifNotSamePos =
				[&](const Point2D tp, auto&& fc)
				{
					if(tp[0] != p[0] || tp[1] != p[1])
						PL_FWD(fc)(tp);
				};

			if(size[1] > 1)
			{
				PL_FWD(checkFc)({p[0], !p[1] ? size[1] - 1 : p[1] - 1}, BorderConstant_v<Border::Up>);
				PL_FWD(checkFc)({p[0], p[1] + 1 == size[1] ? 0 : p[1] + 1}, BorderConstant_v<Border::Down>);
			}

			ifNotSamePos({!p[0] ? size[0] - 1 : p[0] - 1, !p[1] ? size[1] - 1 : p[1] - 1}, [&](auto tp){ PL_FWD(checkFc)(tp, BorderConstant_v<Border::UpLeft>); });
			ifNotSamePos({p[0] + 1 == size[0] ? 0 : p[0] + 1, !p[1] ? size[1] - 1 : p[1] - 1}, [&](auto tp){ PL_FWD(checkFc)(tp, BorderConstant_v<Border::UpRight>); });

			if(size[0] > 1)
			{
				PL_FWD(checkFc)({!p[0] ? size[0] - 1 : p[0] - 1, p[1]}, BorderConstant_v<Border::Left>);
				PL_FWD(checkFc)({p[0] + 1 == size[0] ? 0 : p[0] + 1, p[1]}, BorderConstant_v<Border::Right>);
			}

			ifNotSamePos({!p[0] ? size[0] - 1 : p[0] - 1, p[1] + 1 == size[1] ? 0 : p[1] + 1}, [&](auto tp){ PL_FWD(checkFc)(tp, BorderConstant_v<Border::UpLeft>); });
			ifNotSamePos({p[0] + 1 == size[0] ? 0 : p[0] + 1, p[1] + 1 == size[1] ? 0 : p[1] + 1}, [&](auto tp){ PL_FWD(checkFc)(tp, BorderConstant_v<Border::UpLeft>); });
		}
		else
		{
			if(p[1] > 0)
			{
				PL_FWD(checkFc)({p[0], p[1] - 1}, BorderConstant_v<Border::Up>);
				if(p[0] > 0)
					PL_FWD(checkFc)({p[0] - 1, p[1] - 1}, BorderConstant_v<Border::UpLeft>);
				if(p[0] < size[0] - 1)
					PL_FWD(checkFc)({p[0] + 1, p[1] - 1}, BorderConstant_v<Border::UpRight>);
			}

			if(p[0] > 0)
				PL_FWD(checkFc)({p[0] - 1, p[1]}, BorderConstant_v<Border::Left>);
			if(p[0] < size[0] - 1)
				PL_FWD(checkFc)({p[0] + 1, p[1]}, BorderConstant_v<Border::Right>);

			if(p[1] < size[1] - 1)
			{
				PL_FWD(checkFc)({p[0], p[1] + 1}, BorderConstant_v<Border::Down>);
				if(p[0] > 0)
					PL_FWD(checkFc)({p[0] - 1, p[1] + 1}, BorderConstant_v<Border::DownLeft>);
				if(p[0] < size[0] - 1)
					PL_FWD(checkFc)({p[0] + 1, p[1] + 1}, BorderConstant_v<Border::DownRight>);
			}
		}
	}

	void visitAdjacentBorders(const Point2D p, const Point2D size, auto&& checkFc)
	{
		if(!p[0])
		{
			PL_FWD(checkFc)(BorderConstant_v<Border::Left>);

			if(!p[1])
				PL_FWD(checkFc)(BorderConstant_v<Border::UpLeft>);
			else if(p[1] + 1 == size[1])
				PL_FWD(checkFc)(BorderConstant_v<Border::DownLeft>);
		}
		else if(p[0] + 1 == size[0])
		{
			PL_FWD(checkFc)(BorderConstant_v<Border::Right>);

			if(!p[1])
				PL_FWD(checkFc)(BorderConstant_v<Border::UpRight>);
			else if(p[1] + 1 == size[1])
				PL_FWD(checkFc)(BorderConstant_v<Border::DownRight>);
		}

		if(!p[1])
		{
			PL_FWD(checkFc)(BorderConstant_v<Border::Up>);
		}
		else if(p[1] + 1 == size[1])
		{
			PL_FWD(checkFc)(BorderConstant_v<Border::Down>);
		}
	}

	template <Border B>
	Point2D getAdjacentPosition(const Point2D p, const Point2D size, bool wrapAround, const BorderConstant<B>)
	{
		if constexpr(B == Border::Down)
		{
			if(wrapAround && p[1] + 1 == size[1])
				return {p[0], 0};

			return {p[0], p[1] + 1};
		}
		else if constexpr(B == Border::Up)
		{
			if(wrapAround && !p[1])
				return {p[0], size[1] - 1};

			return {p[0], p[1] - 1};
		}
		else if constexpr(B == Border::Left)
		{
			if(wrapAround && !p[0])
				return {size[0] - 1, p[1]};

			return {p[0] - 1, p[1]};
		}
		else if constexpr(B == Border::Right)
		{
			if(wrapAround && p[0] + 1 == size[0])
				return {0, p[1]};

			return {p[0] + 1, p[1]};
		}
		else if constexpr(B == Border::UpLeft)
		{
			if(wrapAround && !p[0] && !p[1])
				return {size[0] - 1, size[1] - 1};

			return {p[0] - 1, p[1] - 1};
		}
		else if constexpr(B == Border::UpRight)
		{
			if(wrapAround && p[0] + 1 == size[0] && !p[1])
				return {0, size[1] - 1};

			return {p[0] + 1, p[1] - 1};
		}
		else if constexpr(B == Border::DownLeft)
		{
			if(wrapAround && !p[0] && p[1] + 1 == size[1])
				return {size[0] - 1, 0};

			return {p[0] - 1, p[1] + 1};
		}
		else// if(B == Border::DownRight)
		{
			if(wrapAround && p[0] + 1 == size[0] && p[1] + 1 == size[1])
				return {};

			return {p[0] + 1, p[1] + 1};
		}
	}
}
