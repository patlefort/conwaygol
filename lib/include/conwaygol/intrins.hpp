/*
	Conway's Game of Life simulator

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#if defined(__MMX__) || defined(__SSE2__)
	#include <immintrin.h>
#endif

namespace conwaygol::impl
{
	// Minimal implementation of integer simd vectors using intrinsics. Only contains what I need.
	#ifdef __MMX__

		// 8x 8 bits values.
		struct PackedInt8_64
		{
			using ThisType = PackedInt8_64;
			using SimdType = __m64;

			SimdType pv;

			PackedInt8_64() = default;
			PackedInt8_64(std::initializer_list<tag::zero_t>) noexcept
				: pv{} {}

			PackedInt8_64(SimdType o) noexcept
				: pv{o} {}

			PackedInt8_64(char v) noexcept :
				pv{_mm_set1_pi8(v)} {}

			PackedInt8_64(tag::scalar_t, char v) noexcept :
				pv{_mm_set_pi8(0, 0, 0, 0, 0, 0, 0, v)} {}

			PackedInt8_64(
				char v1, char v2, char v3, char v4,
				char v5, char v6, char v7, char v8) noexcept :
					pv{_mm_set_pi8(v8, v7, v6, v5, v4, v3, v2, v1)} {}

			[[nodiscard]] ThisType operator- (ThisType v) const noexcept
				{ return _mm_sub_pi8(pv, v.pv); }

			ThisType &operator-= (ThisType v) noexcept
				{ *this = *this - v; return *this; }

			[[nodiscard]] ThisType operator+ (ThisType v) const noexcept
				{ return _mm_add_pi8(pv, v.pv); }

			ThisType &operator+= (ThisType v) noexcept
				{ *this = *this + v; return *this; }

			// Shift right the entire vector horizontally.
			template <int shift>
			[[nodiscard]] ThisType operator>> (std::integral_constant<int, shift>) const noexcept
				{ return _mm_srl_si64(pv, _mm_set_pi32(0, shift)); }

			// Shift left the entire vector horizontally.
			template <int shift>
			[[nodiscard]] ThisType operator<< (std::integral_constant<int, shift>) const noexcept
				{ return _mm_sll_si64(pv, _mm_set_pi32(0, shift)); }

			// Shift right individual elements with leading zeros.
			[[nodiscard]] ThisType operator>> (const int shift) const noexcept
			{
				const short shiftMask = (short)0xFF00 | (short(0xFF) >> shift);
				return _mm_and_si64(_mm_srli_pi16(pv, shift), _mm_set1_pi16(shiftMask));
			}

			// Shift left individual elements with leading zeros.
			[[nodiscard]] ThisType operator<< (const int shift) const noexcept
			{
				const short shiftMask = (short)0xFF | (short(0xFF) << (shift+8));
				return _mm_and_si64(_mm_slli_pi16(pv, shift), _mm_set1_pi16(shiftMask));
			}

			[[nodiscard]] ThisType operator~ () const noexcept
				{ return _mm_andnot_si64(pv, _mm_cmpeq_pi32(pv, pv)); }

			[[nodiscard]] ThisType operator& (ThisType v) const noexcept
				{ return _mm_and_si64(pv, v.pv); }

			[[nodiscard]] ThisType operator| (ThisType v) const noexcept
				{ return _mm_or_si64(pv, v.pv); }

			[[nodiscard]] ThisType operator&& (ThisType v) const noexcept
				{ return *this & v; }

			[[nodiscard]] ThisType operator|| (ThisType v) const noexcept
				{ return *this | v; }

			template <int shift>
			ThisType &operator>>= (std::integral_constant<int, shift> v) noexcept
				{ *this = *this >> v; return *this; }

			template <int shift>
			ThisType &operator<<= (std::integral_constant<int, shift> v) noexcept
				{ *this = *this << v; return *this; }

			ThisType &operator>>= (const int shift) noexcept
				{ *this = *this >> shift; return *this; }

			ThisType &operator<<= (const int shift) noexcept
				{ *this = *this << shift; return *this; }

			ThisType &operator&= (ThisType v) noexcept
				{ *this = *this & v; return *this; }

			ThisType &operator|= (ThisType v) noexcept
				{ *this = *this | v; return *this; }

			[[nodiscard]] ThisType operator== (ThisType v) const noexcept
				{ return _mm_cmpeq_pi8(pv, v.pv); }

			[[nodiscard]] ThisType operator!= (ThisType v) const noexcept
				{ return ~ThisType{_mm_cmpeq_pi8(pv, v.pv)}; }

			[[nodiscard]] ThisType operator> (ThisType v) const noexcept
				{ return _mm_cmpgt_pi8(pv, v.pv); }

			[[nodiscard]] ThisType operator< (ThisType v) const noexcept
				{ return _mm_cmpgt_pi8(v.pv, pv); }

			[[nodiscard]] explicit operator bool() const noexcept
			{
				union
				{
					SimdType pv;
					std::int64_t sv;
				} uv;

				uv.pv = _mm_cmpeq_pi32(pv, _mm_setzero_si64());

				return uv.sv != (std::int64_t)-1;
			}
		};

		[[nodiscard]] inline PackedInt8_64 shiftScalarLeftByValue(PackedInt8_64 v, int shift) noexcept
			{ return _mm_sll_si64(v.pv, _mm_set_pi32(0, shift)); }

		template <typename T_>
		[[nodiscard]] PackedInt8_64 initScalar(type_c<PackedInt8_64>, T_ v) noexcept
			{ return {tag::scalar, (char)v}; }

		[[nodiscard]] inline PackedInt8_64 initAllOne(type_c<PackedInt8_64>) noexcept
			{ return -1; }


		// 4x 16 bits values.
		struct PackedInt16_64
		{
			using ThisType = PackedInt16_64;
			using SimdType = __m64;

			SimdType pv;

			PackedInt16_64() = default;
			PackedInt16_64(std::initializer_list<tag::zero_t>) noexcept
				: pv{} {}

			PackedInt16_64(SimdType o) noexcept
				: pv{o} {}

			PackedInt16_64(short v) noexcept :
				pv{_mm_set1_pi16(v)} {}

			PackedInt16_64(tag::scalar_t, short v) noexcept :
				pv{_mm_set_pi16(0, 0, 0, v)} {}

			PackedInt16_64(short v1, short v2, short v3, short v4) noexcept :
					pv{_mm_set_pi16(v4, v3, v2, v1)} {}

			[[nodiscard]] ThisType operator- (ThisType v) const noexcept
				{ return _mm_sub_pi16(pv, v.pv); }

			ThisType &operator-= (ThisType v) noexcept
				{ *this = *this - v; return *this; }

			[[nodiscard]] ThisType operator+ (ThisType v) const noexcept
				{ return _mm_add_pi16(pv, v.pv); }

			ThisType &operator+= (ThisType v) noexcept
				{ *this = *this + v; return *this; }

			template <int shift>
			[[nodiscard]] ThisType operator>> (std::integral_constant<int, shift>) const noexcept
				{ return _mm_srl_si64(pv, _mm_set_pi32(0, shift)); }

			template <int shift>
			[[nodiscard]] ThisType operator<< (std::integral_constant<int, shift>) const noexcept
				{ return _mm_sll_si64(pv, _mm_set_pi32(0, shift)); }

			[[nodiscard]] ThisType operator>> (const int shift) const noexcept
				{ return _mm_srli_pi16(pv, shift); }

			[[nodiscard]] ThisType operator<< (const int shift) const noexcept
				{ return _mm_slli_pi16(pv, shift); }

			[[nodiscard]] ThisType operator~ () const noexcept
				{ return _mm_andnot_si64(pv, _mm_cmpeq_pi32(pv, pv)); }

			[[nodiscard]] ThisType operator& (ThisType v) const noexcept
				{ return _mm_and_si64(pv, v.pv); }

			[[nodiscard]] ThisType operator| (ThisType v) const noexcept
				{ return _mm_or_si64(pv, v.pv); }

			[[nodiscard]] ThisType operator&& (ThisType v) const noexcept
				{ return *this & v; }

			[[nodiscard]] ThisType operator|| (ThisType v) const noexcept
				{ return *this | v; }

			template <int shift>
			ThisType &operator>>= (std::integral_constant<int, shift> v) noexcept
				{ *this = *this >> v; return *this; }

			template <int shift>
			ThisType &operator<<= (std::integral_constant<int, shift> v) noexcept
				{ *this = *this << v; return *this; }

			ThisType &operator>>= (const int shift) noexcept
				{ *this = *this >> shift; return *this; }

			ThisType &operator<<= (const int shift) noexcept
				{ *this = *this << shift; return *this; }

			ThisType &operator&= (ThisType v) noexcept
				{ *this = *this & v; return *this; }

			ThisType &operator|= (ThisType v) noexcept
				{ *this = *this | v; return *this; }

			[[nodiscard]] ThisType operator== (ThisType v) const noexcept
				{ return _mm_cmpeq_pi16(pv, v.pv); }

			[[nodiscard]] ThisType operator!= (ThisType v) const noexcept
				{ return ~ThisType{_mm_cmpeq_pi16(pv, v.pv)}; }

			[[nodiscard]] ThisType operator> (ThisType v) const noexcept
				{ return _mm_cmpgt_pi16(pv, v.pv); }

			[[nodiscard]] ThisType operator< (ThisType v) const noexcept
				{ return _mm_cmpgt_pi16(v.pv, pv); }

			// Implicit convertion causes problems
			[[nodiscard]] explicit operator bool() const noexcept
			{
				union
				{
					SimdType pv;
					std::uint64_t sv;
				} uv;

				return (bool)uv.sv;
			}
		};

		[[nodiscard]] inline PackedInt16_64 shiftScalarLeftByValue(PackedInt16_64 v, int shift) noexcept
			{ return _mm_sll_si64(v.pv, _mm_set_pi32(0, shift)); }

		template <typename T_>
		[[nodiscard]] PackedInt16_64 initScalar(type_c<PackedInt16_64>, T_ v) noexcept
			{ return {tag::scalar, (short)v}; }

		[[nodiscard]] inline PackedInt16_64 initAllOne(type_c<PackedInt16_64>) noexcept
			{ return -1;  }

		[[nodiscard]] inline bool testNotZero(PackedInt16_64 v) noexcept
		{
			union
			{
				PackedInt16_64::SimdType pv;
				std::uint64_t sv;
			} uv;

			uv.pv = v.pv;

			return (bool)uv.sv;
		}

	#endif

	#ifdef __SSE2__

		// 16x 8 bits values.
		struct PackedInt8_128
		{
			using ThisType = PackedInt8_128;
			using SimdType = __m128i;

			SimdType pv;

			PackedInt8_128() = default;
			PackedInt8_128(std::initializer_list<tag::zero_t>) noexcept
				: pv{} {}

			PackedInt8_128(SimdType o) noexcept
				: pv{o} {}

			PackedInt8_128(char v) noexcept :
				pv{_mm_set1_epi8(v)} {}

			PackedInt8_128(tag::scalar_t, char v) noexcept :
				pv{_mm_set_epi8(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, v)} {}

			PackedInt8_128(
				char v1, char v2, char v3, char v4,
				char v5, char v6, char v7, char v8,
				char v9, char v10, char v11, char v12,
				char v13, char v14, char v15, char v16) noexcept :
					pv{_mm_set_epi8(v16, v15, v14, v13, v12, v11, v10, v9, v8, v7, v6, v5, v4, v3, v2, v1)} {}

			[[nodiscard]] ThisType operator- (ThisType v) const noexcept
				{ return _mm_sub_epi8(pv, v.pv); }

			ThisType &operator-= (ThisType v) noexcept
				{ *this = *this - v; return *this; }

			[[nodiscard]] ThisType operator+ (ThisType v) const noexcept
				{ return _mm_add_epi8(pv, v.pv); }

			ThisType &operator+= (ThisType v) noexcept
				{ *this = *this + v; return *this; }

			template <int shift>
			[[nodiscard]] ThisType operator>> (std::integral_constant<int, shift>) const noexcept
			{
				auto res = pv;

				if constexpr(shift > 8)
					res = _mm_srli_si128(res, shift / 8);

				static constexpr int shiftRest = shift % 8;

				return _mm_add_epi64(_mm_srli_epi64(res, shiftRest), _mm_srli_si128(_mm_slli_epi64(res, 64 - shiftRest), 8));
			}

			template <int shift>
			[[nodiscard]] ThisType operator<< (std::integral_constant<int, shift>) const noexcept
			{
				auto res = pv;

				if constexpr(shift > 8)
					res = _mm_slli_si128(res, shift / 8);

				static constexpr int shiftRest = shift % 8;

				return _mm_add_epi64(_mm_slli_epi64(res, shiftRest), _mm_slli_si128(_mm_srli_epi64(res, 64 - shiftRest), 8));
			}

			[[nodiscard]] ThisType operator>> (const int shift) const noexcept
			{
				const short shiftMask = (short)0xFF00 | (short(0xFF) >> shift);
				return _mm_and_si128(_mm_srli_epi16(pv, shift), _mm_set1_epi16(shiftMask));
			}

			[[nodiscard]] ThisType operator<< (const int shift) const noexcept
			{
				const short shiftMask = (short)0xFF | (short(0xFF) << (shift+8));
				return _mm_and_si128(_mm_slli_epi16(pv, shift), _mm_set1_epi16(shiftMask));
			}

			[[nodiscard]] ThisType operator~ () const noexcept
				{ return _mm_andnot_si128(pv, _mm_cmpeq_epi32(pv, pv)); }

			[[nodiscard]] ThisType operator& (ThisType v) const noexcept
				{ return _mm_and_si128(pv, v.pv); }

			[[nodiscard]] ThisType operator| (ThisType v) const noexcept
				{ return _mm_or_si128(pv, v.pv); }

			[[nodiscard]] ThisType operator&& (ThisType v) const noexcept
				{ return *this & v; }

			[[nodiscard]] ThisType operator|| (ThisType v) const noexcept
				{ return *this | v; }

			template <int shift>
			ThisType &operator>>= (std::integral_constant<int, shift> v) noexcept
				{ *this = *this >> v; return *this; }

			template <int shift>
			ThisType &operator<<= (std::integral_constant<int, shift> v) noexcept
				{ *this = *this << v; return *this; }

			ThisType &operator>>= (const int shift) noexcept
				{ *this = *this >> shift; return *this; }

			ThisType &operator<<= (const int shift) noexcept
				{ *this = *this << shift; return *this; }

			ThisType &operator&= (ThisType v) noexcept
				{ *this = *this & v; return *this; }

			ThisType &operator|= (ThisType v) noexcept
				{ *this = *this | v; return *this; }

			[[nodiscard]] ThisType operator== (ThisType v) const noexcept
				{ return _mm_cmpeq_epi8(pv, v.pv); }

			[[nodiscard]] ThisType operator!= (ThisType v) const noexcept
				{ return ~ThisType{_mm_cmpeq_epi8(pv, v.pv)}; }

			[[nodiscard]] ThisType operator> (ThisType v) const noexcept
				{ return _mm_cmpgt_epi8(pv, v.pv); }

			[[nodiscard]] ThisType operator< (ThisType v) const noexcept
				{ return _mm_cmplt_epi8(pv, v.pv); }

			[[nodiscard]] explicit operator int() const noexcept
				{ return _mm_movemask_epi8(pv); }

			[[nodiscard]] explicit operator bool() const noexcept
				{ return int(*this); }
		};

		// 8x 16 bits values.
		struct PackedInt16_128
		{
			using ThisType = PackedInt16_128;
			using SimdType = __m128i;

			SimdType pv;

			PackedInt16_128() = default;
			PackedInt16_128(std::initializer_list<tag::zero_t>) noexcept
				: pv{} {}

			PackedInt16_128(SimdType o) noexcept
				: pv{o} {}

			PackedInt16_128(short v) noexcept :
				pv{_mm_set1_epi16(v)} {}

			PackedInt16_128(tag::scalar_t, short v) noexcept :
				pv{_mm_set_epi16(0, 0, 0, 0, 0, 0, 0, v)} {}

			PackedInt16_128(short v1, short v2, short v3, short v4, short v5, short v6, short v7, short v8) noexcept :
					pv{_mm_set_epi16(v8, v7, v6, v5, v4, v3, v2, v1)} {}

			[[nodiscard]] ThisType operator- (ThisType v) const noexcept
				{ return _mm_sub_epi16(pv, v.pv); }

			ThisType &operator-= (ThisType v) noexcept
				{ *this = *this - v; return *this; }

			[[nodiscard]] ThisType operator+ (ThisType v) const noexcept
				{ return _mm_add_epi16(pv, v.pv); }

			ThisType &operator+= (ThisType v) noexcept
				{ *this = *this + v; return *this; }

			template <int shift>
			[[nodiscard]] ThisType operator>> (std::integral_constant<int, shift>) const noexcept
			{
				auto res = pv;

				if constexpr(shift > 8)
					res = _mm_srli_si128(res, shift / 8);

				static constexpr int shiftRest = shift % 8;

				return _mm_or_si128(_mm_srli_epi64(res, shiftRest), _mm_srli_si128(_mm_slli_epi64(res, 64 - shiftRest), 8));
			}

			template <int shift>
			[[nodiscard]] ThisType operator<< (std::integral_constant<int, shift>) const noexcept
			{
				auto res = pv;

				if constexpr(shift > 8)
					res = _mm_slli_si128(res, shift / 8);

				static constexpr int shiftRest = shift % 8;

				return _mm_or_si128(_mm_slli_epi64(res, shiftRest), _mm_slli_si128(_mm_srli_epi64(res, 64 - shiftRest), 8));
			}

			[[nodiscard]] ThisType operator>> (const int shift) const noexcept
				{ return _mm_srli_epi16(pv, shift); }

			[[nodiscard]] ThisType operator<< (const int shift) const noexcept
				{ return _mm_slli_epi16(pv, shift); }

			[[nodiscard]] ThisType operator~ () const noexcept
				{ return _mm_andnot_si128(pv, _mm_cmpeq_epi32(pv, pv)); }

			[[nodiscard]] ThisType operator& (ThisType v) const noexcept
				{ return _mm_and_si128(pv, v.pv); }

			[[nodiscard]] ThisType operator| (ThisType v) const noexcept
				{ return _mm_or_si128(pv, v.pv); }

			[[nodiscard]] ThisType operator&& (ThisType v) const noexcept
				{ return *this & v; }

			[[nodiscard]] ThisType operator|| (ThisType v) const noexcept
				{ return *this | v; }

			template <int shift>
			ThisType &operator>>= (std::integral_constant<int, shift> v) noexcept
				{ *this = *this >> v; return *this; }

			template <int shift>
			ThisType &operator<<= (std::integral_constant<int, shift> v) noexcept
				{ *this = *this << v; return *this; }

			ThisType &operator>>= (const int shift) noexcept
				{ *this = *this >> shift; return *this; }

			ThisType &operator<<= (const int shift) noexcept
				{ *this = *this << shift; return *this; }

			ThisType &operator&= (ThisType v) noexcept
				{ *this = *this & v; return *this; }

			ThisType &operator|= (ThisType v) noexcept
				{ *this = *this | v; return *this; }

			[[nodiscard]] ThisType operator== (ThisType v) const noexcept
				{ return _mm_cmpeq_epi16(pv, v.pv); }

			[[nodiscard]] ThisType operator!= (ThisType v) const noexcept
				{ return ~ThisType{_mm_cmpeq_epi16(pv, v.pv)}; }

			[[nodiscard]] ThisType operator> (ThisType v) const noexcept
				{ return _mm_cmpgt_epi16(pv, v.pv); }

			[[nodiscard]] ThisType operator< (ThisType v) const noexcept
				{ return _mm_cmpgt_epi16(v.pv, pv); }

			[[nodiscard]] explicit operator int() const noexcept
				{ return _mm_movemask_epi8(pv); }

			[[nodiscard]] explicit operator bool() const noexcept
				{ return int(*this); }
		};

		// 4x 32 bits values.
		struct PackedInt32_128
		{
			using ThisType = PackedInt32_128;
			using SimdType = __m128i;

			SimdType pv;

			PackedInt32_128() = default;
			PackedInt32_128(std::initializer_list<tag::zero_t>) noexcept
				: pv{} {}

			PackedInt32_128(SimdType o) noexcept
				: pv{o} {}

			PackedInt32_128(int v) noexcept :
				pv{_mm_set1_epi32(v)} {}

			PackedInt32_128(tag::scalar_t, int v) noexcept :
				pv{_mm_set_epi32(0, 0, 0, v)} {}

			PackedInt32_128(int v1, int v2, int v3, int v4) noexcept :
				pv{_mm_set_epi32(v4, v3, v2, v1)} {}

			[[nodiscard]] ThisType operator- (ThisType v) const noexcept
				{ return _mm_sub_epi32(pv, v.pv); }

			ThisType &operator-= (ThisType v) noexcept
				{ *this = *this - v; return *this; }

			[[nodiscard]] ThisType operator+ (ThisType v) const noexcept
				{ return _mm_add_epi32(pv, v.pv); }

			ThisType &operator+= (ThisType v) noexcept
				{ *this = *this + v; return *this; }

			[[nodiscard]] ThisType operator* (ThisType v) const noexcept
				{ return _mm_mul_epi32(pv, v.pv); }

			ThisType &operator*= (ThisType v) noexcept
				{ *this = *this * v; return *this; }

			template <int shift>
			[[nodiscard]] ThisType operator>> (std::integral_constant<int, shift>) const noexcept
			{
				auto res = pv;

				if constexpr(shift > 8)
					res = _mm_srli_si128(res, shift / 8);

				static constexpr int shiftRest = shift % 8;

				return _mm_or_si128(_mm_srli_epi64(res, shiftRest), _mm_srli_si128(_mm_slli_epi64(res, 64 - shiftRest), 8));
			}

			template <int shift>
			[[nodiscard]] ThisType operator<< (std::integral_constant<int, shift>) const noexcept
			{
				auto res = pv;

				if constexpr(shift > 8)
					res = _mm_slli_si128(res, shift / 8);

				static constexpr int shiftRest = shift % 8;

				return _mm_or_si128(_mm_slli_epi64(res, shiftRest), _mm_slli_si128(_mm_srli_epi64(res, 64 - shiftRest), 8));
			}

			[[nodiscard]] ThisType operator>> (const int shift) const noexcept
				{ return _mm_srli_epi32(pv, shift); }

			[[nodiscard]] ThisType operator<< (const int shift) const noexcept
				{ return _mm_slli_epi32(pv, shift); }

			[[nodiscard]] ThisType operator~ () const noexcept
				{ return _mm_andnot_si128(pv, _mm_cmpeq_epi32(pv, pv)); }

			[[nodiscard]] ThisType operator& (ThisType v) const noexcept
				{ return _mm_and_si128(pv, v.pv); }

			[[nodiscard]] ThisType operator| (ThisType v) const noexcept
				{ return _mm_or_si128(pv, v.pv); }

			[[nodiscard]] ThisType operator&& (ThisType v) const noexcept
				{ return *this & v; }

			[[nodiscard]] ThisType operator|| (ThisType v) const noexcept
				{ return *this | v; }

			template <int shift>
			ThisType &operator>>= (std::integral_constant<int, shift> v) noexcept
				{ *this = *this >> v; return *this; }

			template <int shift>
			ThisType &operator<<= (std::integral_constant<int, shift> v) noexcept
				{ *this = *this << v; return *this; }

			ThisType &operator>>= (const int shift) noexcept
				{ *this = *this >> shift; return *this; }

			ThisType &operator<<= (const int shift) noexcept
				{ *this = *this << shift; return *this; }

			ThisType &operator&= (ThisType v) noexcept
				{ *this = *this & v; return *this; }

			ThisType &operator|= (ThisType v) noexcept
				{ *this = *this | v; return *this; }

			[[nodiscard]] ThisType operator== (ThisType v) const noexcept
				{ return _mm_cmpeq_epi32(pv, v.pv); }

			[[nodiscard]] ThisType operator!= (ThisType v) const noexcept
				{ return ~ThisType{_mm_cmpeq_epi32(pv, v.pv)}; }

			[[nodiscard]] ThisType operator> (ThisType v) const noexcept
				{ return _mm_cmpgt_epi32(pv, v.pv); }

			[[nodiscard]] ThisType operator< (ThisType v) const noexcept
				{ return _mm_cmplt_epi32(pv, v.pv); }

			[[nodiscard]] explicit operator int() const noexcept
				{ return _mm_movemask_epi8(pv); }

			[[nodiscard]] explicit operator bool() const noexcept
				{ return int(*this); }

		};

		// Shift left entire vector horizontally by a runtime value.
		[[nodiscard]] inline PackedInt32_128 shiftScalarLeftByValue(PackedInt32_128 v, int shift) noexcept
		{
			union
			{
				__m128i pv;
				__uint128_t sv;
			} uv;

			uv.pv = v.pv;
			uv.sv <<= shift;

			return uv.pv;
		}

		[[nodiscard]] inline PackedInt8_128 shiftScalarLeftByValue(PackedInt8_128 v, int shift) noexcept
		{
			union
			{
				__m128i pv;
				__uint128_t sv;
			} uv;

			uv.pv = v.pv;
			uv.sv <<= shift;

			return uv.pv;
		}

		[[nodiscard]] inline PackedInt16_128 shiftScalarLeftByValue(PackedInt16_128 v, int shift) noexcept
		{
			union
			{
				__m128i pv;
				__uint128_t sv;
			} uv;

			uv.pv = v.pv;
			uv.sv <<= shift;

			return uv.pv;
		}

		template <typename T_>
		[[nodiscard]] PackedInt32_128 initScalar(type_c<PackedInt32_128>, T_ v) noexcept
			{ return {tag::scalar, (int)v}; }

		template <typename T_>
		[[nodiscard]] PackedInt8_128 initScalar(type_c<PackedInt8_128>, T_ v) noexcept
			{ return {tag::scalar, (char)v}; }

		template <typename T_>
		[[nodiscard]] PackedInt16_128 initScalar(type_c<PackedInt16_128>, T_ v) noexcept
			{ return {tag::scalar, (short)v}; }

		[[nodiscard]] inline PackedInt32_128 initAllOne(type_c<PackedInt32_128>) noexcept
			{ return -1; }

		[[nodiscard]] inline PackedInt8_128 initAllOne(type_c<PackedInt8_128>) noexcept
			{ return -1; }

		[[nodiscard]] inline PackedInt16_128 initAllOne(type_c<PackedInt16_128>) noexcept
			{ return -1; }

		#ifdef __SSE4_1__
			[[nodiscard]] inline bool testNotZero(PackedInt8_128 v) noexcept
				{ return !_mm_test_all_zeros(v.pv, v.pv); }

			[[nodiscard]] inline bool testNotZero(PackedInt32_128 v) noexcept
				{ return !_mm_test_all_zeros(v.pv, v.pv); }

			[[nodiscard]] inline bool testNotZero(PackedInt16_128 v) noexcept
				{ return !_mm_test_all_zeros(v.pv, v.pv); }
		#else
			[[nodiscard]] inline bool testNotZero(PackedInt8_128 v) noexcept
				{ return _mm_movemask_epi8(_mm_cmpeq_epi32(v.pv, _mm_setzero_si128())) != 0xFFFF; }

			[[nodiscard]] inline bool testNotZero(PackedInt32_128 v) noexcept
				{ return _mm_movemask_epi8(_mm_cmpeq_epi32(v.pv, _mm_setzero_si128())) != 0xFFFF; }

			[[nodiscard]] inline bool testNotZero(PackedInt16_128 v) noexcept
				{ return _mm_movemask_epi8(_mm_cmpeq_epi32(v.pv, _mm_setzero_si128())) != 0xFFFF; }
		#endif

	#endif


	#ifdef __AVX2__

		// 32x 8 bits values. Not quite fonctional yet and slower than PackedInt8_128 so far.
		struct PackedInt8_256
		{
			using ThisType = PackedInt8_256;
			using SimdType = __m256i;

			SimdType pv;

			PackedInt8_256() = default;
			PackedInt8_256(std::initializer_list<tag::zero_t>) noexcept
				: pv{} {}

			PackedInt8_256(SimdType o) noexcept
				: pv{o} {}

			PackedInt8_256(char v) noexcept :
				pv{_mm256_set1_epi8(v)} {}

			PackedInt8_256(tag::scalar_t, char v) noexcept :
				pv{_mm256_set_epi8(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, v)} {}

			PackedInt8_256(
				char v1, char v2, char v3, char v4,
				char v5, char v6, char v7, char v8,
				char v9, char v10, char v11, char v12,
				char v13, char v14, char v15, char v16,
				char v17, char v18, char v19, char v20,
				char v21, char v22, char v23, char v24,
				char v25, char v26, char v27, char v28,
				char v29, char v30, char v31, char v32) noexcept :
					pv{_mm256_set_epi8(v32, v31, v30, v29, v28, v27, v26, v25, v24, v23, v22, v21, v20, v19, v18, v17, v16, v15, v14, v13, v12, v11, v10, v9, v8, v7, v6, v5, v4, v3, v2, v1)} {}

			[[nodiscard]] ThisType operator- (ThisType v) const noexcept
				{ return _mm256_sub_epi8(pv, v.pv); }

			ThisType &operator-= (ThisType v) noexcept
				{ *this = *this - v; return *this; }

			[[nodiscard]] ThisType operator+ (ThisType v) const noexcept
				{ return _mm256_add_epi8(pv, v.pv); }

			ThisType &operator+= (ThisType v) noexcept
				{ *this = *this + v; return *this; }

			template <int shift>
			[[nodiscard]] ThisType operator>> (std::integral_constant<int, shift>) const noexcept
			{
				auto res = pv;

				if constexpr(shift > 8)
				{
					const auto t1 = _mm256_slli_si256(res, 32 - shift / 8);
					res =  _mm256_or_si256( _mm256_srli_si256(res, shift / 8), _mm256_permute2x128_si256(_mm256_setzero_si256(), t1, 0b11) );
				}

				static constexpr int shiftRest = shift % 8;

				const auto t1 = _mm256_or_si256(_mm256_srli_epi64(res, shiftRest), _mm256_srli_si256(_mm256_slli_epi64(res, 64 - shiftRest), 8));
				const auto t2 = _mm256_permute4x64_epi64(_mm256_slli_epi64(res, 64 - shiftRest), 0b1000);
				return _mm256_or_si256(t1, _mm256_and_si256(_mm256_set_epi64x(0, 0, std::int64_t(-1), 0), t2) );
			}

			template <int shift>
			[[nodiscard]] ThisType operator<< (std::integral_constant<int, shift>) const noexcept
			{
				auto res = pv;

				if constexpr(shift > 8)
				{
					const auto t1 = _mm256_srli_si256(res, 32 - shift / 8);
					res = _mm256_or_si256( _mm256_slli_si256(res, shift / 8), _mm256_permute2x128_si256(_mm256_setzero_si256(), t1, 0b1000) );
				}

				static constexpr int shiftRest = shift % 8;

				const auto t1 = _mm256_or_si256(_mm256_slli_epi64(res, shiftRest), _mm256_slli_si256(_mm256_srli_epi64(res, 64 - shiftRest), 8));
				const auto t2 = _mm256_permute4x64_epi64(_mm256_srli_epi64(res, 64 - shiftRest), 0b10000);
				return _mm256_or_si256(t1, _mm256_and_si256(_mm256_set_epi64x(0, std::int64_t(-1), 0, 0), t2) );
			}

			[[nodiscard]] ThisType operator>> (const int shift) const noexcept
			{
				const short shiftMask = (short)0xFF00 | (short(0xFF) >> shift);
				return _mm256_and_si256(_mm256_srli_epi16(pv, shift), _mm256_set1_epi16(shiftMask));
			}

			[[nodiscard]] ThisType operator<< (const int shift) const noexcept
			{
				const short shiftMask = (short)0xFF | (short(0xFF) << (shift+8));
				return _mm256_and_si256(_mm256_slli_epi16(pv, shift), _mm256_set1_epi16(shiftMask));
			}

			[[nodiscard]] ThisType operator~ () const noexcept
				{ return _mm256_andnot_si256(pv, _mm256_cmpeq_epi32(pv, pv)); }

			[[nodiscard]] ThisType operator& (ThisType v) const noexcept
				{ return _mm256_and_si256(pv, v.pv); }

			[[nodiscard]] ThisType operator| (ThisType v) const noexcept
				{ return _mm256_or_si256(pv, v.pv); }

			[[nodiscard]] ThisType operator&& (ThisType v) const noexcept
				{ return *this & v; }

			[[nodiscard]] ThisType operator|| (ThisType v) const noexcept
				{ return *this | v; }

			template <int shift>
			ThisType &operator>>= (std::integral_constant<int, shift> v) noexcept
				{ *this = *this >> v; return *this; }

			template <int shift>
			ThisType &operator<<= (std::integral_constant<int, shift> v) noexcept
				{ *this = *this << v; return *this; }

			ThisType &operator>>= (const int shift) noexcept
				{ *this = *this >> shift; return *this; }

			ThisType &operator<<= (const int shift) noexcept
				{ *this = *this << shift; return *this; }

			ThisType &operator&= (ThisType v) noexcept
				{ *this = *this & v; return *this; }

			ThisType &operator|= (ThisType v) noexcept
				{ *this = *this | v; return *this; }

			[[nodiscard]] ThisType operator== (ThisType v) const noexcept
				{ return _mm256_cmpeq_epi8(pv, v.pv); }

			[[nodiscard]] ThisType operator!= (ThisType v) const noexcept
				{ return ~ThisType{_mm256_cmpeq_epi8(pv, v.pv)}; }

			[[nodiscard]] ThisType operator> (ThisType v) const noexcept
				{ return _mm256_cmpgt_epi8(pv, v.pv); }

			[[nodiscard]] ThisType operator< (ThisType v) const noexcept
				{ return _mm256_cmpgt_epi8(v.pv, pv); }

			[[nodiscard]] explicit operator int() const noexcept
				{ return _mm256_movemask_epi8(pv); }

			[[nodiscard]] explicit operator bool() const noexcept
				{ return int(*this); }
		};

		[[nodiscard]] inline PackedInt8_256 shiftScalarLeftByValue(PackedInt8_256 v, int shift) noexcept
		{
			union
			{
				__m256i ppv;
				__uint128_t sv[2];
			} uv;

			uv.ppv = v.pv;

			if(shift >= 128)
			{
				uv.sv[1] = uv.sv[0] << (shift - 128);
				uv.sv[0] = 0;
			}
			else
			{
				uv.sv[1] = (uv.sv[1] << shift) | (uv.sv[0] >> (sizeof(__uint128_t) - shift));
				uv.sv[0] <<= shift;
			}

			return uv.ppv;
		}

		template <typename T_>
		[[nodiscard]] PackedInt8_256 initScalar(type_c<PackedInt8_256>, T_ v) noexcept
			{ return {tag::scalar_t{}, (char)v}; }

		[[nodiscard]] inline PackedInt8_256 initAllOne(type_c<PackedInt8_256>) noexcept
			{ return -1; }

		[[nodiscard]] inline bool testNotZero(PackedInt8_256 v) noexcept
			{ return _mm256_movemask_epi8(_mm256_cmpeq_epi64(v.pv, _mm256_setzero_si256())) != (int)-1; }

	#endif
}
