/*
	Conway's Game of Life simulator

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include "utils.hpp"

namespace conwaygol
{
	// Serializable objects
	class ISerializable
	{
	public:
		virtual ~ISerializable() = default;

		virtual void serialize(std::ostream &outputStream) const = 0;
		virtual void unserialize(std::istream &inputStream) = 0;

		friend auto &operator<<(std::ostream &os, const ISerializable &s)
			{ s.serialize(os); return os; }

		friend auto &operator>>(std::istream &is, ISerializable &s)
			{ s.unserialize(is); return is; }
	};

	// Virtual interfaces to universes and simulators
	class IUniverse : public ISerializable
	{
	public:
		virtual ~IUniverse() = default;

		virtual IUniverse &operator =(const IUniverse &) = 0;
		virtual IUniverse &operator =(IUniverse &&) = 0;

		virtual void setDimensions(Point2D s) = 0;

		[[nodiscard]] virtual Point2D getDimensions() const = 0;

		virtual void zero() = 0;
		[[nodiscard]] virtual bool empty() const = 0;

		[[nodiscard]] virtual std::uint8_t operator[] (Point2D p) const noexcept = 0;

		virtual void set(Point2D p, std::uint8_t v) noexcept = 0;

		virtual void snapshot(Point2D pos, Structure<std::uint8_t> &struc, std::uint32_t zoomFactor = 1, SnapshotType type = SnapshotType::AccurateAverage) const = 0;
		virtual void insert(const Structure<std::uint8_t> &struc, Point2D pos = {}) = 0;
		virtual void insert(const IUniverse &struc, Point2D pos = {}) = 0;

		#ifdef PL_LIB_NUMA
			virtual void setCpuMask(pl::numa::policy mask) = 0;
		#endif
	};

	// Virtualize a universe that follow the universe concept.
	// If $Ref_ is true, VirtualUniverse will hold a reference to a UniverseType_.
	template <typename UniverseType_, bool Ref_ = true>
	class VirtualUniverse : public IUniverse
	{
	private:

		template <typename T_, typename FC_>
		void tryDynCast(T_ &o, const FC_ &fc)
		{
			if(auto ptr = dynamic_cast< std::conditional_t<std::is_const_v<T_>, const VirtualUniverse<UniverseType_, Ref_>, VirtualUniverse<UniverseType_, Ref_>> *>(&o); ptr)
				fc(*ptr, std::bool_constant<true>{});
			else if(auto ptr = dynamic_cast< std::conditional_t<std::is_const_v<T_>, const VirtualUniverse<UniverseType_, !Ref_>, VirtualUniverse<UniverseType_, !Ref_>> *>(&o); ptr)
				fc(*ptr, std::bool_constant<true>{});
			else
				fc(PL_FWD(o), std::bool_constant<false>{});
		}

	public:

		using UniverseType = UniverseType_;

		template <typename... Args_>
		VirtualUniverse(Args_&&... args) :
			mUniverse{PL_FWD(args)...} {}

		virtual IUniverse &operator =(const IUniverse &o) override
		{
			tryDynCast(o,
				[&](auto &casted, auto success)
				{
					if constexpr(success)
						mUniverse = casted.mUniverse;
					else
					{
						mUniverse.setDimensions(casted.getDimensions());
						insert(casted);
					}
				}
			);

			return *this;
		}

		virtual IUniverse &operator =(IUniverse &&o) override
		{
			tryDynCast(o,
				[&](auto &casted, auto success)
				{
					if constexpr(success)
						mUniverse = std::move(casted.mUniverse);
					else
					{
						mUniverse.setDimensions(casted.getDimensions());
						insert(casted);
						o.setDimensions({});
					}
				}
			);

			return *this;
		}

		std::conditional_t<Ref_, UniverseType &, UniverseType> mUniverse;

		virtual void setDimensions(Point2D s) override
			{ mUniverse.setDimensions(s); }

		[[nodiscard]] virtual Point2D getDimensions() const override
			{ return mUniverse.getDimensions(); }

		virtual void zero() override { mUniverse.zero(); }
		[[nodiscard]] virtual bool empty() const override { return mUniverse.empty(); }

		[[nodiscard]] virtual std::uint8_t operator[] (Point2D p) const noexcept override
			{ return mUniverse[p]; }

		virtual void set(Point2D p, std::uint8_t v) noexcept override
			{ return mUniverse.set(p, v); }

		virtual void snapshot(Point2D pos, Structure<std::uint8_t> &struc, std::uint32_t zoomFactor = 1, SnapshotType type = SnapshotType::AccurateAverage) const override
		{
			constexpr float cellMax = std::numeric_limits<std::uint8_t>::max();
			constexpr float cellSt = 1.0f / std::numeric_limits<std::uint8_t>::max() / 2.0f;

			snapshotStruct(mUniverse, pos, struc.getDimensions(), [&](Point2D p, float v){ struc.set(p, v * cellMax + cellSt); }, zoomFactor, type);
		}

		virtual void insert(const Structure<std::uint8_t> &struc, Point2D pos = {}) override
		{
			insertStruct(struc, mUniverse, pos);
		}

		virtual void insert(const IUniverse &struc, Point2D pos = {}) override
		{
			tryDynCast(struc,
				[&](auto &casted, auto success)
				{
					if constexpr(success)
						insertStruct(casted.mUniverse, mUniverse, pos);
					else
						insertStruct(casted, mUniverse, pos);
				}
			);
		}

		#ifdef PL_LIB_NUMA
			virtual void setCpuMask(pl::numa::policy mask) override { mUniverse.setCpuMask(std::move(mask)); }
		#endif

		virtual void serialize(std::ostream &outputStream) const override
			{ outputStream << mUniverse; }
		virtual void unserialize(std::istream &inputStream) override
			{ inputStream >> mUniverse; }
	};


	using SharedUniverseType = sa::shared<IUniverse &, sa::shared_upgrade_mutex_type &>;
	using SharedUniverseViewType = sa::shared_view<SharedUniverseType>;
	using SharedUniverseConstViewType = sa::shared_view<const SharedUniverseType>;

	class ISimulator
	{
	public:
		virtual ~ISimulator() = default;

		virtual void advance(std::size_t steps = 1, bool lockWrite = false) = 0;

		virtual void setWrap(bool v) = 0;
		[[nodiscard]] virtual bool getWrap() const = 0;

		[[nodiscard]] virtual SharedUniverseConstViewType getUniverse() const = 0;
		[[nodiscard]] virtual SharedUniverseViewType getUniverse() = 0;

		// Create a new empty universe of the proper type for the simulator.
		[[nodiscard]] virtual std::unique_ptr<IUniverse> createUniverse() const = 0;

		#ifdef PL_LIB_NUMA
			virtual void setCpuMask(pl::numa::policy mask) = 0;
		#endif
	};

	// Virtualize a simulator that follow the simulator concept.
	template <typename SimulatorType_>
	class VirtualSimulator : public ISimulator
	{
	public:

		using SimulatorType = SimulatorType_;

		SimulatorType mSimulator;

		// Access to the universe will share the same mutex through different objects.
		sa::shared<
			VirtualUniverse<typename SimulatorType::UniverseType>,
			sa::shared_upgrade_mutex_type &
		> mSharedVirtualUniverse {
			mSimulator.mUniverseAccess,
			sa::in_place_list,
			*mSimulator.mUniverseAccess.access(sa::mode::excl)
		};
		SharedUniverseType mSharedVirtualUniverseInterface {
			sa::reference<IUniverse>,
			mSharedVirtualUniverse
		};

		[[nodiscard]] virtual SharedUniverseConstViewType getUniverse() const override
			{ return mSharedVirtualUniverseInterface; }

		[[nodiscard]] virtual SharedUniverseViewType getUniverse() override
			{ return mSharedVirtualUniverseInterface; }

		virtual void setWrap(bool v) override { mSimulator.setWrap(v); }
		[[nodiscard]] virtual bool getWrap() const override { return mSimulator.getWrap(); }

		virtual void advance(std::size_t steps = 1, bool lockWrite = false) override
		{
			if(lockWrite)
				mSimulator.advance(steps, std::bool_constant<true>{});
			else
				mSimulator.advance(steps, std::bool_constant<false>{});
		}

		[[nodiscard]] virtual std::unique_ptr<IUniverse> createUniverse() const override
		{
			return std::make_unique<VirtualUniverse<typename SimulatorType::UniverseType, false>>();
		}

		#ifdef PL_LIB_NUMA
			virtual void setCpuMask(pl::numa::policy mask) override { mSimulator.setCpuMask(std::move(mask)); }
		#endif
	};
}
