/*
	Conway's Game of Life simulator

	Copyright (C) 2018 Patrick Northon

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#ifdef _OPENMP
	#include <omp.h>
#endif

#include <boost/container/stable_vector.hpp>
#include <boost/pool/pool_alloc.hpp>

#include <memory>
#include <atomic>

#include "simulator.hpp"

namespace conwaygol
{
	template <typename PointerType_>
	struct TileData
	{
		using PointerType = PointerType_;

		Point2D p;
		PointerType_ data, dataBack;
	};

	template <typename T_>
	using DefaultActiveTilesContainer = std::vector<T_, std::allocator<T_>>;
	template <typename T_>
	using StableActiveTilesContainer = boost::container::stable_vector<T_, std::allocator<T_>>;

	struct PackAllocator
	{
		using size_type = std::size_t;
		using difference_type = std::ptrdiff_t;

		[[nodiscard]] static char *malloc(const size_type size)
		{
			#ifdef PL_LIB_HUGEPAGES
				return static_cast<char *>(pl::hugepages_resource().allocate(size, alignof(std::max_align_t)));
			#else
				return static_cast<char *>(operator new(size));
			#endif
		}

		static void free(char * const block)
		{
			#ifdef PL_LIB_HUGEPAGES
				return pl::hugepages_resource().deallocate(block, 0, alignof(std::max_align_t));
			#else
				operator delete(block);
			#endif
		}
	};

	using DefaultMemorySource = boost::pool<PackAllocator>;

	template <typename TileType_, template<typename> class ContainerType_ = DefaultActiveTilesContainer, typename MemorySource_ = DefaultMemorySource>
	class ActiveTilesContainer
	{
	public:

		using PointerType = typename TileType_::PointerType;
		using TileType = TileType_;

		ContainerType_<TileType> tiles;
		MemorySource_ *memorySource;

		ActiveTilesContainer(MemorySource_ *m = nullptr) : memorySource{m} {}
		ActiveTilesContainer(const ActiveTilesContainer &) = default;
		ActiveTilesContainer(ActiveTilesContainer &&) = default;

		ActiveTilesContainer &operator=(const ActiveTilesContainer &) = default;
		ActiveTilesContainer &operator=(ActiveTilesContainer &&) = default;

		auto &addTile(const Point2D p)
		{
			tiles.push_back({p, allocate(), nullptr});
			return tiles.back();
		}

		void allocateTile(TileType &tile)
		{
			tile.data = allocate();
		}

		void allocateTileBack(TileType &tile)
		{
			tile.dataBack = allocate();
		}

		void eraseTilesIf(auto&& fc)
		{
			// Meant to be used with a stable vector. Elements must not move in memory.
			for(auto it=tiles.begin(), end = tiles.end(); it != end; )
			{
				if(PL_FWD(fc)(*it))
				{
					deallocateData(*it);
					it = tiles.erase(it);
					end = tiles.end();
				}
				else
					++it;
			}
		}

		void flip() noexcept
		{
			using std::swap;
			for(auto &tile : tiles)
			{
				plassert(tile.data);
				plassert(tile.dataBack);
				swap(tile.data, tile.dataBack);
			}
		}

		void safeResize(const std::size_t size)
		{
			if(size < tiles.size())
				for(std::size_t i=size; i<tiles.size(); ++i)
					deallocateData(tiles[i]);

			tiles.resize(size, {});
		}

	private:

		[[nodiscard]] PointerType allocate()
			{ return static_cast<PointerType>(memorySource->malloc()); }

		void deallocateData(TileType &tile)
		{
			if(tile.data)
				memorySource->free(tile.data);
			if(tile.dataBack)
				memorySource->free(tile.dataBack);
		}
	};

	template <typename PackType_ = std::uint_fast8_t, typename MemorySource_ = DefaultMemorySource>
	class UniverseTiled
	{
	public:

		using UniverseTileViewType = PackedUniverseView<PackType_>;
		using UniverseTileConstViewType = PackedUniverseView<std::add_const_t<PackType_>>;
		using PackType = typename UniverseTileViewType::PackType;
		using PointerType = PackType *; //typename std::allocator_traits<Allocator_>::pointer;
		using ActiveTile = TileData<PointerType>;
		using MemorySourceType = MemorySource_;

		bool mDirty{false};

		UniverseTiled() = default;
		UniverseTiled(UniverseTiled &&) = default;

		UniverseTiled(const UniverseTiled &o)
		{
			*this = o;
		}

		UniverseTiled &operator=(UniverseTiled &&) = default;
		UniverseTiled &operator=(const UniverseTiled &o)
		{
			setDimensions(o.getDimensions());

			for(const auto &node : o.mActiveTiles)
				for(const auto &tile : node.tiles)
				{
					auto&& [newTile, props] = addActiveTile(tile.p);
					std::copy_n(tile.data, getTileNbPacks(), newTile.data);
					getTile(tile.p) = &newTile;
				}

			return *this;
		}

		#ifdef PL_LIB_NUMA
			const auto &getCpuMask() const noexcept { return cpuMask; }
			void setCpuMask(pl::numa::policy mask) { cpuMask = std::move(mask); }
		#endif

		void setDimensions(Point2D s)
		{
			if(!mTiles.empty() && s[0] == mDimensions[0] && s[1] == mDimensions[1])
			{
				zero();
				return;
			}

			mDimensions = s;
			mNbTiles = {};
			mLastTilesDims = {};
			mDirty = false;

			mTiles = {};
			mActiveTiles = {};
			mTilesToAdd = {};
			mMemorySources.clear();

			#if defined(PL_LIB_NUMA) && defined(_OPENMP)
				const auto nbNodes = (std::size_t)pl::numa::node_count();
			#else
				constexpr std::size_t nbNodes = 1;
			#endif

			constexpr auto chunkSize = getTileNbPacks() * sizeof(PackType);
			constexpr auto blockSize = std::size_t(1024 * 1024 * 2) / chunkSize;
			constexpr auto maxSize = blockSize * 5;

			mMemorySources.reserve(nbNodes);
			for(std::size_t i=0; i<nbNodes; ++i)
				mMemorySources.push_back(std::make_unique<MemorySourceType>(chunkSize, blockSize, maxSize));

			mActiveTiles.reserve(mMemorySources.size());
			mTilesToAdd.reserve(mMemorySources.size());

			for(std::size_t i=0; i<mMemorySources.size(); ++i)
			{
				mActiveTiles.emplace_back(std::to_address(mMemorySources[i]));
				mTilesToAdd.emplace_back(std::to_address(mMemorySources[i]));
			}

			mTileProps = {};
			mTilePropsBottom = {};
			mTilePropsRight = {};
			mTilePropsLast = {};

			if(s[0] > 0 && s[1] > 0)
			{
				mNbTiles = {
					(s[0] - 1) / mTileSize + 1,
					(s[1] - 1) / mTileSize + 1
				};

				mLastTilesDims = {
					(s[0] - 1) % mTileSize + 1,
					(s[1] - 1) % mTileSize + 1
				};

				mTiles.resize(mNbTiles[0] * mNbTiles[1]);

				mTileProps = calcUniverseProperties<PackType, true>({mTileSize, mTileSize});
				mTilePropsBottom = calcUniverseProperties<PackType, true>({mNbTiles[0] > 1 ? mTileSize : mLastTilesDims[0], mLastTilesDims[1]});
				mTilePropsRight = calcUniverseProperties<PackType, true>({mLastTilesDims[0], mNbTiles[1] > 1 ? mTileSize : mLastTilesDims[1]});
				mTilePropsLast = calcUniverseProperties<PackType, true>(mLastTilesDims);
			}
		}

		[[nodiscard]] bool empty() const noexcept { return mTiles.empty(); }
		[[nodiscard]] auto getDimensions() const noexcept { return mDimensions; }
		[[nodiscard]] static constexpr auto getTileSize() noexcept { return mTileSize; }
		[[nodiscard]] static constexpr auto getTileNbPacks() noexcept { return mTileNbPacks; }

		[[nodiscard]] auto getNbTiles() const noexcept { return mNbTiles; }
		[[nodiscard]] auto getNbNodes() const noexcept { return mActiveTiles.size(); }
		[[nodiscard]] auto getNbActiveTiles(std::size_t node) const noexcept { return mActiveTiles[node].tiles.size(); }

		[[nodiscard]] auto createActiveTileView(ActiveTile &t) const noexcept
		{
			const auto &props = selectTileProperties(t.p);
			return createActiveTileView(t.data, props); // calcUniverseNbPacks(props)
		}

		[[nodiscard]] auto createActiveTileView(const ActiveTile &t) const noexcept
		{
			const auto &props = selectTileProperties(t.p);
			return createActiveTileView(t.data, props);
		}

		[[nodiscard]] auto createActiveTileView(PackType * const data, const Point2D p) const noexcept
		{
			const auto &props = selectTileProperties(p);
			return createActiveTileView(data, props);
		}

		[[nodiscard]] auto createActiveTileView(const PackType * const data, const Point2D p) const noexcept
		{
			const auto &props = selectTileProperties(p);
			return createActiveTileView(data, props);
		}

		[[nodiscard]] auto createActiveTileView(PackType * const data, const PackedUniverseProperties<PackType> &props) const noexcept
			{ return UniverseTileViewType{std::span{data, getTileNbPacks()}, props}; }

		[[nodiscard]] auto createActiveTileView(const PackType * const data, const PackedUniverseProperties<PackType> &props) const noexcept
			{ return UniverseTileConstViewType{std::span{data, getTileNbPacks()}, props}; }

		void forEachActiveTiles(auto&& fc)
		{
			for(auto &node : mActiveTiles)
				for(auto &tp : node.tiles)
					PL_FWD(fc)(tp, createActiveTileView(tp));
		}

		void forEachActiveTiles(auto&& fc) const
		{
			for(const auto &node : mActiveTiles)
				for(const auto &tp : node.tiles)
					PL_FWD(fc)(tp, createActiveTileView(tp));
		}

		[[nodiscard]] auto &getActiveTile(std::size_t index, std::size_t node) noexcept
			{ return mActiveTiles[node].tiles[index]; }
		[[nodiscard]] const auto &getActiveTile(std::size_t index, std::size_t node) const noexcept
			{ return mActiveTiles[node].tiles[index]; }

		[[nodiscard]] auto &getActiveTiles(std::size_t node) noexcept
			{ return mActiveTiles[node]; }
		[[nodiscard]] const auto &getActiveTiles(std::size_t node) const noexcept
			{ return mActiveTiles[node]; }

		void zero()
		{
			mTiles.assign(mTiles.size(), nullptr);

			for(auto &node : mActiveTiles)
				node = {node.memorySource};

			for(auto &ms : mMemorySources)
				ms->purge_memory();
		}

		void zeroBorders()
		{
			forEachActiveTiles([&](auto&& tp, auto&& t){
				if(tp.p[1] == 0)
				{
					t.zeroBorder(BorderConstant_v<Border::Up>);

					if(tp.p[0] == 0)
						t.zeroBorder(BorderConstant_v<Border::UpLeft>);
					if(tp.p[0] == mNbTiles[0] - 1)
						t.zeroBorder(BorderConstant_v<Border::UpRight>);
				}

				if(tp.p[0] == 0)
					t.zeroBorder(BorderConstant_v<Border::Left>);
				if(tp.p[0] == mNbTiles[0] - 1)
					t.zeroBorder(BorderConstant_v<Border::Right>);

				if(tp.p[1] == mNbTiles[1] - 1)
				{
					t.zeroBorder(BorderConstant_v<Border::Down>);
					if(tp.p[0] == 0)
						t.zeroBorder(BorderConstant_v<Border::DownLeft>);
					if(tp.p[0] == mNbTiles[0] - 1)
						t.zeroBorder(BorderConstant_v<Border::DownRight>);
				}
			});
		}

		[[nodiscard]] bool operator[] (Point2D p) const noexcept
		{
			const auto tp = getTilePosition(p);
			const auto tile = getTile(tp);

			plassert(!tile || tile->p == tp);
			return !tile ? false : createActiveTileView(*tile)[{p[0] % mTileSize, p[1] % mTileSize}];
		}

		// Not thread-safe
		void set(Point2D p, bool v)
		{
			const auto tp = getTilePosition(p);
			auto &tile = getTile(tp);
			UniverseTileViewType view;

			if(!tile)
			{
				if(!v)
					return;

				auto&& [newTile, props] = addActiveTile(tp);
				tile = &newTile;
				view = createActiveTileView(newTile.data, props);
				view.zero();
			}
			else
			{
				view = createActiveTileView(*tile);
			}

			Point2D tilePos{p[0] % mTileSize, p[1] % mTileSize};
			view.set(tilePos, v);

			std::atomic_ref dirty{mDirty};
			dirty.store(true, std::memory_order::release);
		}

		void addTilesToAdd(std::size_t node, auto&& fc, auto&& addFc) const
		{
			if(!mTilesToAdd[node].tiles.empty())
			{
				PL_FWD(fc)([&](auto &uni){
					auto &atiles = uni.mActiveTiles[node].tiles;
					const auto oldSize = atiles.size();

					auto &tta = uni.mTilesToAdd[node].tiles;

					for(auto &t : tta)
						PL_FWD(addFc)(t);

					atiles.insert(atiles.end(), std::make_move_iterator(tta.begin()), std::make_move_iterator(tta.end()));

					for(std::size_t i=oldSize, m=atiles.size(); i<m; ++i)
						uni.getTile(atiles[i].p) = &atiles[i];

					tta.clear();
				});
			}
		}

		void copyBorders(ActiveTile &activeTile, bool wrapAround) const noexcept
		{
			const auto p = activeTile.p;
			const auto view = createActiveTileView(activeTile.dataBack, p);

			const auto doCopy =
				[&](Point2D pos, const auto border)
				{
					if(const auto t = getTile(pos); !t)
						view.zeroBorder(border);
					else
						view.copyBorder(createActiveTileView(t->dataBack, pos), border);
				};

			if(wrapAround)
			{
				doCopy({p[0], !p[1] ? mNbTiles[1] - 1 : p[1] - 1}, BorderConstant_v<Border::Up>);
				doCopy({!p[0] ? mNbTiles[0] - 1 : p[0] - 1, !p[1] ? mNbTiles[1] - 1 : p[1] - 1}, BorderConstant_v<Border::UpLeft>);
				doCopy({p[0] + 1 == mNbTiles[0] ? 0 : p[0] + 1, !p[1] ? mNbTiles[1] - 1 : p[1] - 1}, BorderConstant_v<Border::UpRight>);

				doCopy({!p[0] ? mNbTiles[0] - 1 : p[0] - 1, p[1]}, BorderConstant_v<Border::Left>);
				doCopy({p[0] + 1 == mNbTiles[0] ? 0 : p[0] + 1, p[1]}, BorderConstant_v<Border::Right>);

				doCopy({p[0], p[1] + 1 == mNbTiles[1] ? 0 : p[1] + 1}, BorderConstant_v<Border::Down>);
				doCopy({!p[0] ? mNbTiles[0] - 1 : p[0] - 1, p[1] + 1 == mNbTiles[1] ? 0 : p[1] + 1}, BorderConstant_v<Border::DownLeft>);
				doCopy({p[0] + 1 == mNbTiles[0] ? 0 : p[0] + 1, p[1] + 1 == mNbTiles[1] ? 0 : p[1] + 1}, BorderConstant_v<Border::DownRight>);
			}
			else
			{
				if(p[1] > 0)
				{
					doCopy({p[0], p[1] - 1}, BorderConstant_v<Border::Up>);
					if(p[0] > 0)
						doCopy({p[0] - 1, p[1] - 1}, BorderConstant_v<Border::UpLeft>);
					if(p[0] < mNbTiles[0] - 1)
						doCopy({p[0] + 1, p[1] - 1}, BorderConstant_v<Border::UpRight>);
				}

				if(p[0] > 0)
					doCopy({p[0] - 1, p[1]}, BorderConstant_v<Border::Left>);
				if(p[0] < mNbTiles[0] - 1)
					doCopy({p[0] + 1, p[1]}, BorderConstant_v<Border::Right>);

				if(p[1] < mNbTiles[1] - 1)
				{
					doCopy({p[0], p[1] + 1}, BorderConstant_v<Border::Down>);
					if(p[0] > 0)
						doCopy({p[0] - 1, p[1] + 1}, BorderConstant_v<Border::DownLeft>);
					if(p[0] < mNbTiles[0] - 1)
						doCopy({p[0] + 1, p[1] + 1}, BorderConstant_v<Border::DownRight>);
				}
			}
		}

		void removeEmptyTiles() noexcept
		{
			for(auto &node : mActiveTiles)
			{
				node.eraseTilesIf([&](const auto &t){
					const auto view = createActiveTileView(t);

					if(!view.hasAliveCellsInBuffer())
					{
						auto &tile = getTile(t.p);
						tile = nullptr;
						return true;
					}

					return false;
				});
			}
		}

		[[nodiscard]] Point2D getTilePosition(Point2D p) const noexcept
			{ return {p[0] / mTileSize, p[1] / mTileSize}; }

		[[nodiscard]] auto getTileIndex(Point2D p) const noexcept
			{ return p[1] / mTileSize * mNbTiles[0] + p[0] / mTileSize; }

		[[nodiscard]] auto &getTile(Point2D p) noexcept
			{ return mTiles[(std::size_t)p[1] * mNbTiles[0] + (std::size_t)p[0]]; }
		[[nodiscard]] const auto &getTile(Point2D p) const noexcept
			{ return mTiles[(std::size_t)p[1] * mNbTiles[0] + (std::size_t)p[0]]; }

		[[nodiscard]] auto &getTile(std::size_t i) noexcept
			{ return mTiles[i]; }
		[[nodiscard]] const auto &getTile(std::size_t i) const noexcept
			{ return mTiles[i]; }

		[[nodiscard]] auto getTileDimensions(const Point2D p) const noexcept
		{
			return Point2D{
				p[0] == mNbTiles[0] - 1 ? mLastTilesDims[0] : mTileSize,
				p[1] == mNbTiles[1] - 1 ? mLastTilesDims[1] : mTileSize
			};
		}

		[[nodiscard]] const auto &selectTileProperties(const Point2D p) const noexcept
		{
			const bool isLastX = p[0] + 1 == mNbTiles[0];
			const bool isLastY = p[1] + 1 == mNbTiles[1];

			return (isLastX ? (isLastY ? mTilePropsLast : mTilePropsRight) : (isLastY ? mTilePropsBottom : mTileProps));
		}

		auto addActiveTile(auto &container, const Point2D p)
		{
			const int node = getNextNumaNode();

			CGOL_IFNUMA( pl::numa::scoped_alloc_on_node ag{node}; )

			const auto props = selectTileProperties(p);
			return std::make_pair(std::ref(container[node].addTile(p)), props);
		}

		auto addActiveTile(const Point2D p)
		{
			return addActiveTile(mActiveTiles, p);
		}

		auto addTileToAdd(const Point2D p)
		{
			return addActiveTile(mTilesToAdd, p);
		}

	private:
		static constexpr std::uint32_t mTileSize = std::max<std::uint32_t>(1, 256 / UniverseTileViewType::nbBitsPack) * UniverseTileViewType::nbBitsPack - 2;
		static constexpr std::size_t mTileNbPacks = calcUniverseNbPacks(calcUniverseProperties<PackType>({mTileSize, mTileSize}));
		Point2D mDimensions{}, mNbTiles{}, mLastTilesDims{};

		#ifdef PL_LIB_NUMA
			pl::numa::policy cpuMask;
		#endif

		#ifdef PL_LIB_HUGEPAGES
			using TileTableAllocator = std::pmr::polymorphic_allocator<ActiveTile *>;
		#else
			using TileTableAllocator = std::allocator<ActiveTile *>;
		#endif

		std::vector<ActiveTile *, TileTableAllocator> mTiles{
			#ifdef PL_LIB_HUGEPAGES
				TileTableAllocator{&pl::hugepages_resource()}
			#endif
		};
		std::vector<ActiveTilesContainer<TileData<PackType *>, StableActiveTilesContainer, MemorySourceType>> mActiveTiles;
		std::vector<ActiveTilesContainer<TileData<PackType *>, DefaultActiveTilesContainer, MemorySourceType>> mTilesToAdd;
		PackedUniverseProperties<PackType> mTileProps, mTilePropsBottom, mTilePropsRight, mTilePropsLast;
		std::vector<std::unique_ptr<MemorySourceType>> mMemorySources;

		#if defined(PL_LIB_NUMA) && defined(_OPENMP)
			int mCurrentNode = 0;
			unsigned int mCurrentCpu = 0;

			int getNextNumaNode() noexcept
			{
				int res;

				if(numa_available() > -1)
				{
					const auto mask = cpuMask.cpu_mask();
					const auto nbBits = numa_bitmask_nbytes(mask) * 8;

					do
					{
						while(!numa_bitmask_isbitset(mask, mCurrentCpu))
						{
							if(++mCurrentCpu >= nbBits)
								mCurrentCpu = 0;
						}
					}
					while( (mCurrentNode = numa_node_of_cpu(mCurrentCpu)) == -1 );

					res = pl::numa::node_index(mCurrentNode);

					if(++mCurrentCpu >= numa_bitmask_nbytes(mask) * 8)
						mCurrentCpu = 0;
				}
				else
				{
					mCurrentNode = 0;
					res = 0;
				}

				plassert(res >= 0 && res < (int)mActiveTiles.size());

				return res;
			}
		#else

			constexpr int getNextNumaNode() noexcept
			{
				return 0;
			}

		#endif


		// Text serialization
		friend auto &operator <<(std::ostream &os, const UniverseTiled &uni)
		{
			os << std::dec << uni.mDimensions[0] << ' ' << uni.mDimensions[1] << '\n';

			for(Point2D::value_type y=0; y<uni.mDimensions[1]; ++y)
			{
				for(Point2D::value_type x=0; x<uni.mDimensions[0]; ++x)
					os << (uni[{x, y}] ? '*' : '.');

				os << '\n';
			}

			return os;
		}

		friend auto &operator >>(std::istream &is, UniverseTiled &uni)
		{
			Point2D dims{};

			is >> dims[0] >> dims[1];
			impl::skipEOL(is);

			uni.setDimensions(dims);

			for(Point2D::value_type y=0; y<dims[1]; ++y)
			{
				for(Point2D::value_type x=0; x<dims[0]; ++x)
					if(impl::getNextNonSpaceChar(is) == '*')
						uni.set({x, y}, true);

				impl::skipEOL(is);
			}

			return is;
		}

		friend void insertStruct(const UniverseTiled &sStruct, auto &dStruct, Point2D pos = {})
		{
			const auto dDims = dStruct.getDimensions();
			const auto globTileSize = sStruct.getTileSize();

			sStruct.forEachActiveTiles(
				[&](const auto &t, const auto &view)
				{
					const auto tileDims = view.getDimensions();
					const auto dPos = Point2D{ t.p[0] * globTileSize + pos[0], t.p[1] * globTileSize + pos[1] };

					if(dPos[0] < dDims[0] && dPos[1] < dDims[1] && dPos[0] + tileDims[0] >= pos[0] && dPos[1] + tileDims[1] >= pos[1])
					{
						const Point2D clippedDims{
							dPos[0] + tileDims[0] > dDims[0] ? dDims[0] - dPos[0] : tileDims[0],
							dPos[1] + tileDims[1] > dDims[1] ? dDims[1] - dPos[1] : tileDims[1]
						};

						for(Point2D::value_type y=0; y<clippedDims[1]; ++y)
							for(Point2D::value_type x=0; x<clippedDims[0]; ++x)
								dStruct.set({dPos[0] + x, dPos[1] + y}, view[{x, y}]);
					}
				}
			);
		}
	};

	template <typename GameRule_ = rules::Classic, bool SharedAccessUniverse_ = true>
	class SimulatorTiled
	{
	public:
		using GameRule = GameRule_;

		#ifdef __SSE2__
			using PackType = impl::PackedInt8_128;
		#elif defined(__MMX__)
			using PackType = impl::PackedInt8_64;
		#else
			using PackType = std::uint_fast8_t;
		#endif

		using MemorySourceType = DefaultMemorySource;
		using UniverseType = UniverseTiled<PackType, MemorySourceType>;

	private:
		using MutexType = std::conditional_t<SharedAccessUniverse_, sa::shared_upgrade<UniverseType>::mutex_type, boost::null_mutex>;
		using PointerType = PackType *;

		GameRule mGameRule;
		bool mWrapAround = false;
		std::size_t mStepCounter = 0;
		Point2D oldSize{};
		UniverseType mUniverse;

		#ifdef PL_LIB_NUMA
			pl::numa::policy cpuMask;
		#endif

		static constexpr std::size_t mCleanupFrequency = 1000;

	public:
		sa::shared<UniverseType &, MutexType> mUniverseAccess{sa::in_place_list, mUniverse};

		SimulatorTiled(GameRule gr = {})
			: mGameRule{std::move(gr)} {}

		void setRule(GameRule r) noexcept { mGameRule = std::move(r); }
		[[nodiscard]] const GameRule &getRule() const noexcept { return mGameRule; }

		#ifdef PL_LIB_NUMA
			void setCpuMask(pl::numa::policy m) { cpuMask = std::move(m); }
		#endif

		// PackedUniverse can be set to wrap around.
		void setWrap(bool v) noexcept
		{
			mWrapAround = v;

			if(!mWrapAround)
				mUniverseAccess | sa::mode::excl | [](auto &uni){ uni.zeroBorders(); uni.mDirty = true; };
		}
		[[nodiscard]] auto getWrap() const noexcept { return mWrapAround; }

		template <bool LockWrite_ = false>
		void advance(std::size_t steps = 1, std::bool_constant<LockWrite_> = {}) noexcept
		{
			#ifdef _OPENMP
				#if !BOOST_COMP_CLANG
					omp_set_nested(true);
				#endif

				omp_set_max_active_levels(2);
			#endif

			const auto fc =
				[&](const auto &uni, auto&& upgLock)
				{
					if(uni.empty())
						return;

					const auto nbNodes = uni.getNbNodes();
					std::atomic_ref dirty{mUniverse.mDirty};
					const bool isDirty = dirty.load(std::memory_order::acquire);

					const auto advanceImpl = [&](const auto node)
						{
							const auto &activeTiles = uni.getActiveTiles(node);
							auto &activeTilesWrite = mUniverse.getActiveTiles(node);

							const auto checkTileBorders = [&](const Point2D p, const PointerType data, auto&& upgLockFc)
								{
									const auto view = uni.createActiveTileView(data, p);

									const auto checkAtBorder =
										[&](const Point2D tp, const auto border)
										{
											if(std::atomic_ref aref{ mUniverse.getTile((std::size_t)tp[1] * uni.getNbTiles()[0] + (std::size_t)tp[0]) };
												!aref.load(std::memory_order::acquire))
											{
												if(!view.hasAliveCellsAtBorder(border))
													return;

												PL_FWD(upgLockFc)([&](auto &uni){
													if(aref.load(std::memory_order::acquire)) // Check if already created by another thread.
														return;

													auto&& [newTile, props] = uni.addTileToAdd(tp);
													std::fill_n(newTile.data, uni.getTileNbPacks(), 0);
													aref.store(&newTile, std::memory_order::release);
												});
											}
										};

									visitNeighborsBounded(p, uni.getNbTiles(), mWrapAround, checkAtBorder);
								};

							const auto checkBorders = [&]<bool CheckBack, bool GenBackData>(std::bool_constant<CheckBack>, std::bool_constant<GenBackData>)
								{
									const auto upgLockCritical =
										[&](auto&& f)
										{
											#pragma omp critical
											PL_FWD(upgLock)([&](auto &uni){ f(uni); });
										};

									#pragma omp parallel for schedule(static) proc_bind(close)
									for(std::size_t i=0; i<activeTiles.tiles.size(); ++i)
									{
										const auto &t = activeTiles.tiles[i];
										if constexpr(CheckBack)
											checkTileBorders(t.p, t.dataBack, upgLockCritical);
										else
											checkTileBorders(t.p, t.data, upgLockCritical);
									}
									#pragma omp barrier

									uni.addTilesToAdd(node, upgLockCritical, [&]([[maybe_unused]] auto &t){
										if constexpr(GenBackData)
										{
											activeTilesWrite.allocateTileBack(t);
											std::fill_n(t.dataBack, uni.getTileNbPacks(), 0);
										}
									});
								};

							if(isDirty)
							{
								checkBorders(std::bool_constant<false>{}, std::bool_constant<false>{});
								#pragma omp barrier

								//#pragma omp parallel for schedule(static) proc_bind(close)
								for(std::size_t i=0; i<activeTilesWrite.tiles.size(); ++i)
								{
									auto &t = activeTilesWrite.tiles[i];

									if(!t.dataBack)
										activeTilesWrite.allocateTileBack(t);

									std::copy_n(t.data, uni.getTileNbPacks(), t.dataBack);
								}
								#pragma omp barrier

								#pragma omp parallel for schedule(static) proc_bind(close)
								for(std::size_t i=0; i<activeTilesWrite.tiles.size(); ++i)
									uni.copyBorders(activeTilesWrite.tiles[i], mWrapAround);

								#pragma omp barrier

								#pragma omp master
								upgLock([&, nbNodes](auto &uni){
									for(std::size_t n=0; n<nbNodes; ++n)
										uni.getActiveTiles(n).flip();
								});
								#pragma omp barrier
							}

							for(std::size_t s=0; s<steps; ++s)
							{
								// Advance simulation.
								#pragma omp parallel for schedule(static) proc_bind(close)
								for(std::size_t i=0; i<activeTiles.tiles.size(); ++i)
								{
									auto &t = activeTilesWrite.tiles[i];
									const auto &props = uni.selectTileProperties(t.p);

									plassert(t.data);
									plassert(t.dataBack);

									impl::Simulator sim{uni.createActiveTileView(t.data, props), uni.createActiveTileView(t.dataBack, props), mGameRule};
									sim.advance();
								}

								#pragma omp barrier

								// Check borders to see if we need to add new active tiles.
								checkBorders(std::bool_constant<true>{}, std::bool_constant<true>{});

								#pragma omp barrier

								// Copy borders.
								#pragma omp parallel for schedule(static) proc_bind(close)
								for(std::size_t i=0; i<activeTilesWrite.tiles.size(); ++i)
									uni.copyBorders(activeTilesWrite.tiles[i], mWrapAround);

								#pragma omp barrier

								// Clean-up empty tiles and flip.
								#pragma omp master
								upgLock([&, nbNodes](auto &uni){
									for(std::size_t n=0; n<nbNodes; ++n)
										uni.getActiveTiles(n).flip();

									if(++mStepCounter == mCleanupFrequency)
									{
										uni.removeEmptyTiles();
										mStepCounter = 0;
									}
								});

								#pragma omp barrier
							}
						};

					#ifdef PL_LIB_NUMA
						// Run a thread for each NUMA nodes.
						#pragma omp parallel num_threads(nbNodes) proc_bind(spread)
					#endif
					{
						#if defined(PL_LIB_NUMA) && defined(_OPENMP)
							const auto node = (std::size_t)omp_get_thread_num();
							pl::numa::scoped_run_on_node numaRunOnNode{pl::numa::node_num((int)node)};
						#else
							constexpr std::size_t node = 0;
						#endif

						#ifdef _OPENMP
						{
							const auto totalCells = uni.getActiveTiles(node).tiles.size() * uni.getTileNbPacks();
							static constexpr std::size_t minSize = 512;
							#ifdef PL_LIB_NUMA
								const auto maxThreads = (std::size_t)cpuMask.node_cpu_count(node);
							#else
								const auto maxThreads = (std::size_t)omp_get_max_threads();
							#endif
							const auto nbThreads = std::min<std::size_t>(totalCells / minSize + (totalCells % minSize ? 1 : 0), maxThreads);

							omp_set_num_threads(nbThreads);
						}
						#endif

						advanceImpl(node);
					}

					if(isDirty)
						dirty.store(false, std::memory_order::release);

				};

			// Lock in shared-access mode if $LockWrite_ is false, otherwise, lock in exclusive mode.
			if constexpr(LockWrite_)
			{
				mUniverseAccess | sa::mode::excl |
					[&](auto &uni)
					{
						fc(uni, EmptyUpgradeLock{uni});
					};
			}
			else
			{
				mUniverseAccess | sa::mode::shared_upgrade |
					pl::applier(std::ref(fc));
			}
		}
	};
}
